#!/usr/bin/env bash

# Author:       Xabier Vázquez-Campos
# Email:        xvazquezc@gmail.com
# Date:         Check the repo for latest commits: https://bitbucket.org/xvazquezc/otureporter
# License:      GNU General Public License
# Usage:        otureporter.sh -i BS_PROJECTS -o OUTDIR -c CTLPROJECT -p POSITIVES -n NEGATIVES [OPTION...]
# Description:  Automated workflow for the analysis and reporting of amplicon data.

set -o errexit
set -o nounset
set -o pipefail

# Load common variables and functions
OTUREPORTER_DIR="$(cd "$(dirname "$(readlink -e "${BASH_SOURCE[0]}")")" >/dev/null 2>&1 && pwd)"
# shellcheck source=scripts/common.sh
source "$OTUREPORTER_DIR/scripts/common.sh"
# shellcheck source=scripts/local.sh
source "$OTUREPORTER_DIR/scripts/local.sh"

# Save the launch arguments so we can log them later
if [[ "$#" -eq 0 ]]; then
  OTUREPORTER_LAUNCH_ARGS=" "
else
  OTUREPORTER_LAUNCH_ARGS=$(printf " %q" "$@")
fi

### Globals
##############################################################################

declare -a SAMPLES=() PROJECTS=() NEG_CONTROLS=() POS_CONTROLS=() LOADED_NEG_CONTROLS=() LOADED_POS_CONTROLS=()

# The BaseSpace project to look for controls which don't have a explicit
# project.
declare PROJECT_CTL=""

# The base output directory, will contain 'psotprocessing' and 'report'.
# May contain 'mothur_run' if a separate TEMP_DIR wasn't specified.
declare MY_DIR

# The temporary director, defaults to $MY_DIR if one wasn't explicitly
# specified.
declare TEMP_DIR

# The clustering cutoff to be used.
declare CLUSTER_CUTOFF="0.03"

# The 'name' of the run to be used in the report. Usually the name of the
# folder in $MY_DIR.
declare RUN_ID

# The method by which samples will be retrieved. Either "bs" - BaseSpace
# or "local" - Local file system.
declare RUN_MODE

# The primer set selected. e.g. "v4b", "its1", etc..
declare PRIMERS

# Number of threads to use.
declare NUMPROC_GLOBAL="4"

# Minimum number of sequences that a sample must have after chimera.vsearch and
# remove.lineages so it's not discarded when doing the subsampling
# (default: 10000).
declare MINSUBSAMPLE="10000"

# The non-target lineages for use when $MOTHUR_MODE = "align"
declare REM_LIN_OVERRIDE=""

# The target lineages for use when $MOTHUR_MODE = "noalign"
declare TARGET_OVERRIDE=""

# Path to database info file to explictly use
declare DATABASE_OVERRIDE=""

# Whether to remove any existing output directory.
declare REMOVE_EXISTING_OUTPUT="n"

# Whether to retain all intermediary data.
declare KEEP_INTERMEDIARY="n"

# Whether to generate phylogenetic tree with the representative sequences with
# FastTree.
declare BUILD_TREE="n"

# Whether the user has elected to not use controls.
declare NO_CONTROLS="n"

# Whether the user asked us to just reknit an existing run.
declare FLAG_REKNIT="n"

# Whether the user asked us to run the self tests.
declare FLAG_PERFORM_SELF_TEST="n"

# Whether the user asked us to continue an existing run.
declare FLAG_RESUME="n"

# Whether the user asked us to checkpoint after each stage.
declare FLAG_SAVE_ALL_STAGES="n"

# Whether the user asked us to diff the global variables following each stage.
declare FLAG_DIFF_STAGES="n"

# Whether the user asked us to only use the R1 reads for ITS/no-align.
declare FLAG_ONLY_R1="n"

# Whether the user asked to use cluster.split
declare CLUSTER_SPLIT="n"

# Whether the user specified a taxlevel to use during cluster.split
declare TAXLEVEL="4"

# Wether to use a different number of cpus for the second part of cluster.split
declare NUMPROC_SPLIT=${NUMPROC_GLOBAL}

# Stage (if any) the user has asked us to forcibly start at.
declare START_FROM_OVERRIDE=""

# Stage (if any) the user has asked us to forcibly run.
declare RUN_STAGE_OVERRIDE=""

### Functions
##############################################################################

usage() {
  echo "Usage: $(cmd) -m MODE --mock MOCK -s PRIMERS -i BS_PROJECTS -o OUTDIR -c CTLPROJECT -p POSITIVES -n NEGATIVES [OPTION...]"
  echo ";
REQUIRED
  -i, --input;Basespace project ID(s) / Local folders.
  -o, --output;Output directory. Directory should not exist. It will be created.
  -c, --control;Basespace project containing the control samples / Local folder.
  --mock;Mock community reference to use (--list_mock to list available mock communities).
  -p, --positive;Positive control samples within the control sample folder.
                ;Several controls can be provided as a list in a file.
  -n, --negative;Negative control samples within the control sample folder.
                ;Several controls can be provided as a list in a file.
  -s, --primers;Primer set to be used.
  -m, --mode;local: FastQ.gz files are in your computer. Full paths required.
            ;basespace: Fetch data directly from BaseSpace. No need to manually download.
;
OPTIONAL
  --sample;Explicitly specify one or more samples to use.
  --tmp;Use a designated temporary folder to mount Basespace and run mothur.
  --cutoff;OTU clustering similarity cutoff.
          ;(Note: Mothur's cutoff is the complement of this) (Default: 0.97).
  -f, --force;If a folder with the same run ID exists, overwrite it.
  -t, --threads;Use that many threads for processing (default: 4)
  --only_r1;(ITS/no-align only) Only use R1 reads.
  --min_subsample;Minimum number of sequences that a sample must have
                 ;after chimera.vsearch and remove.lineages so it's not discarded
                 ;when doing the subsampling (default: 10000).
  -r, --remove_lineage;Override the default non-target lineages. Check the
                      ;--list option for the defaults associated with each primer set.
  --target_lineage;Override the default target lineages. For use with non-aligned
                  ;mode.
  --database;Overrides the automatic selection of the database with the given
            ;database info file.
  -d, --ref_dir;Path to the reference folder containing accessory scripts,
               ;databases and templates (default: same as $(cmd) location).
  --hoarder_mode;Keep all the intermediate mothur files and the downloaded
                ;fastq files, and compress them. Be aware that this may occupy
                ;A LOT of space even compressed.
  --tree;Generates a phylogenetic tree with the representative sequences.
        ;Uses FastTree GTR+GAMMA model and '-fastest' option.
  --no_controls;Allow running without specifying control samples.
  --cluster_split;(align only) Use the cluster.split() command from mothur instead
                 ;of the 'trad is rad' dist.seqs() + cluster() approach.
                 ;Recommended for large and/or very diverse datasets.
  --taxlevel;(with --cluster_split only) Specify the taxonomy level you want to use
            ;to split the distance file (default: 4, i.e. 4th taxon level).
  --threads_split;(with --cluster_split only) Use a lower number of threads for the
                 ;clustering step in cluster.split() (default: same as --threads).
  --download_db;Download an appropriate database if none are found.
;
MISCELLANEOUS
  --resume;Attempt to resume an interrupted run. YMMV.
  --reknit;Was the report not generated properly? Worry not! Use this option
          ;with '--output' to generate it without rerunning the whole thing.
  -l, --list;Print the available primer sets.
  --list_mock;Print the available mock references.
  -v, --verbose;Enable verbose output, can be specified twice.
  -h, --help;Show this help information and exits.
  --nocolor;Disables all colorized output.
  --checkdeps;Just check general dependencies.
  --selftest;Check everything is setup and working correctly.
            ;Performs a test run with sample data.
  --wiki;Opens the repository Wiki in your web browser.
  --version;Output OTUreporter version.
;
DEBUG
  --save;Create an archive of the output directory after each stage.
  --diff_stages;Track variables leaked after stage execution.
  --start_from;Forcibly start from a specific stage.
  --run_stage;Forcibly run a single specific stage.
  " | column_format
}

function parse_arguments() {
  local OPTIONS=i:o:c:p:n:s:d:ft:m:lvhr:
  local LONGOPTIONS=input:,output:,control:,positive:,negative:,primers:,ref_dir:,force,threads:,min_subsample:,list,list_mock,verbose,help,wiki,tmp:,hoarder_mode,reknit,resume,tree,mode:,remove_lineage:,target_lineage:,database:,no_controls,mock:,sample:,nocolor,download_db,checkdeps,selftest,save,diff_stages,start_from:,run_stage:,version,cutoff:,only_r1,cluster_split,taxlevel:,threads_split:

  check_getopt

  # -temporarily store output to be able to check for errors
  # -e.g. use “--options” parameter by name to activate quoting/enhanced mode
  # -pass arguments only via   -- "$@"   to separate them correctly
  local PARSED=$(getopt --options=${OPTIONS} --longoptions=${LONGOPTIONS} --name "$0" -- "$@")

  if [[ $? -ne 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
  fi
  # read getopt’s output this way to handle the quoting right:
  eval set -- "${PARSED}"

  # handle non-option arguments
  if [[ $# -eq 1 ]]; then
    usage
    exit 4
  fi

  # now enjoy the options in order and nicely split until we see --
  while true; do
    case "$1" in
    -o | --output)
      MY_DIR=$(readlink -m "$2")   # Output directory
      RUN_ID=$(basename "$MY_DIR") # ID of the analysis - used in the report

      # simple check if the script directory is a (nested) subdirectory of the output location
      # not solely trusting $PWD as it reflects what the user cd'd to, not necessarily the current location
      # (e.g. the $PWD folder was moved)
      if [[ $PWD/ == $MY_DIR/* ]] || [[ $(pwd)/ == $MY_DIR/* ]] || [[ $(pwd -P)/ == $MY_DIR/* ]]; then
        error "Output directory appears to be an ancestor of the current/script directory"
        error "Output: $MY_DIR"
        error "PWD: $(pwd -P)"
        exit 1
      fi
      shift 2
      ;;
    -i | --input)
      # if input is a file, create an array, otherwise just assign the value given
      if [[ -f "$2" ]] && [[ ! "$2" =~ .(fastq|gz) ]]; then
        PROJECTS+=("$(cat "$2")")
      elif [[ $2 =~ "," ]]; then
        PROJECTS+=(${2//,/ }) # FIXME handles spaces ungracefully
      else
        PROJECTS+=("$2")
      fi
      shift 2
      ;;
    -c | --control)
      if [[ -f "$2" ]] && [[ ! "$2" =~ .(fastq|gz) ]]; then
        PROJECT_CTL="$(cat "$2")"
      else
        PROJECT_CTL="$2"
      fi
      shift 2
      ;;
    -p | --positive)
      if [[ -f "$2" ]] && [[ ! "$2" =~ .(fastq|gz) ]]; then
        POS_CONTROLS+=("$(cat "$2")")
      elif [[ $2 =~ "," ]]; then
        POS_CONTROLS+=(${2//,/ }) # FIXME handles spaces ungracefully
      else
        POS_CONTROLS+=("$2")
      fi
      shift 2
      ;;
    -n | --negative)
      if [[ -f "$2" ]] && [[ ! "$2" =~ .(fastq|gz) ]]; then
        NEG_CONTROLS+=("$(cat "$2")")
      elif [[ $2 =~ "," ]]; then
        NEG_CONTROLS+=(${2//,/ }) # FIXME handles spaces ungracefully
      else
        NEG_CONTROLS+=("$2")
      fi
      shift 2
      ;;
    --mock)
      MOCK_REF_ID=$2
      shift 2
      ;;
    --sample)
      if [[ -f "$2" ]] && [[ ! "$2" =~ .(fastq|gz) ]]; then
        SAMPLES+=("$(cat "$2")")
      elif [[ $2 =~ "," ]]; then
        SAMPLES+=(${2//,/ }) # FIXME handles spaces ungracefully
      else
        SAMPLES+=("$2")
      fi
      shift 2
      ;;
    -m | --mode)
      case $2 in
      local | Local)
        RUN_MODE=local
        shift 2
        ;;
      bs | basespace | BaseSpace | Basespace)
        RUN_MODE=bs
        shift 2
        ;;
      *)
        error "Mode ${2} is not a valid parameter" >&2
        exit 1
        ;;
      esac
      ;;
    -s | --primers)
      PRIMERS=$2
      shift 2
      ;;
    -d | --ref_dir)
      REF_DIR=$2
      shift 2
      ;;
    -t | --threads)
      NUMPROC_GLOBAL=$2
      shift 2
      ;;
    --min_subsample)
      MINSUBSAMPLE=$2
      shift 2
      ;;
    -l | --list)
      info "Listing available primer profiles..."
      list_primers
      exit
      ;;
    --list_mock)
      info "Listing available mock profiles..."
      list_mocks
      exit
      ;;
    -r | --remove_lineage)
      REM_LIN_OVERRIDE=$2
      shift 2
      ;;
    --target_lineage)
      TARGET_OVERRIDE=$2
      shift 2
      ;;
    --database)
      DATABASE_OVERRIDE=$2
      shift 2
      ;;
    -f | --force)
      REMOVE_EXISTING_OUTPUT="y"
      shift
      ;;
    --hoarder_mode)
      KEEP_INTERMEDIARY="y"
      shift
      ;;
    --cluster_split)
      CLUSTER_SPLIT="y"
      shift
      ;;
    --taxlevel)
      TAXLEVEL=$2
      shift 2
      ;;
    --threads_split)
      NUMPROC_SPLIT=$2
      shift 2
      ;;
    --tree)
      DEPENDENCIES+=(FastTree) # add FastTree for the dependency check
      BUILD_TREE="y"
      shift
      ;;
    --reknit)
      FLAG_REKNIT="y"
      shift
      ;;
    --resume)
      FLAG_RESUME="y"
      shift
      ;;
    --save)
      FLAG_SAVE_ALL_STAGES="y"
      shift
      ;;
    --diff_stages)
      FLAG_DIFF_STAGES="y"
      shift
      ;;
    --only_r1)
      FLAG_ONLY_R1="y"
      shift
      ;;
    --start_from)
      START_FROM_OVERRIDE=$2
      shift 2
      ;;
    --run_stage)
      RUN_STAGE_OVERRIDE=$2
      shift 2
      ;;
    -v | --verbose)
      LOG_LEVEL=$((LOG_LEVEL + 1))

      if [[ $LOG_LEVEL -ge $LEVEL_TRACE ]]; then
        set -o xtrace
      fi
      shift
      ;;
    -h | --help)
      usage
      exit
      ;;
    --tmp)
      TEMP_DIR=$2
      shift 2
      ;;
    --cutoff)
      CLUSTER_CUTOFF=$(float_subtract "1" "$2")
      shift 2
      ;;
    --download_db)
      DEPENDENCIES+=(jq)
      DOWNLOAD_MISSING_DB="y"
      shift
      ;;
    --checkdeps)
      ensure_all_dependencies --optional
      exit
      ;;
    --selftest)
      FLAG_PERFORM_SELF_TEST="y"
      shift
      ;;
    --wiki)
      local wiki="https://bitbucket.org/xvazquezc/otureporter/wiki/Home"
      local opsys=$(uname)
      if [[ $opsys == Linux ]]; then
        if hash browse 2>/dev/null; then
          browse $wiki
        elif hash firefox 2>/dev/null; then
          firefox $wiki
        elif hash chromium-browser 2>/dev/null; then
          chromium-browser $wiki
        elif hash firefox-esr 2>/dev/null; then
          firefox-esr $wiki
        else
          info "Wiki located at $wiki"
        fi
      else
        open $wiki
      fi
      exit
      ;;
    --no_controls)
      NO_CONTROLS="y"
      shift
      ;;
    --nocolor)
      COLOR_OUTPUT=n
      color_disable
      shift
      ;;
    --version)
      otureporter_version
      exit 0
      ;;
    --)
      shift
      break
      ;;
    # -*|--*)
    #     echo "\"$1\" is not a valid flag.\
    #     Try with '`cmd` --help' for more information."
    #     exit 1;;
    *)
      if [ -z "$1" ]; then break; else
        error "'$1' is not a valid option"
        exit 3
      fi
      ;;
    esac
  done

  if [[ $# -ne 0 ]]; then
    error "$@ invalid option"
    exit 4
  fi
}

### Script runtime
##############################################################################

# Outputs:
#   * MOTHUR
#   * PROJECTS
#   * TEMP_DIR
#   * PROJECT_CTL
#   * RUN_MODE
#   * MOTHUR_PREFIX
#   * REF_ALN
#   * SAMPLES
#   * MY_DIR
#   * NO_CONTROLS
#   * NEG_CONTROLS
#   * REF_DIR
#   * RUN_ID
#   * POS_CONTROLS
#   * MINSUBSAMPLE
#   * MOCK_REF_ID
#   * SCRIPTS
#   * TAX_REF
#   * BUILD_TREE
#   * PRIMERS
#   * REM_LIN_OVERRIDE
#   * TARGET_OVERRIDE
#   * DATABASE_OVERRIDE
#   * MOTHUR_VERSION
function otureporter_run_init() {
  # Check main input parameters
  # TODO recheck required parameter logic wrt BaseSpace refactoring
  ensure_param "PRIMERS" "-s / --primers"
  ensure_param "RUN_ID" "-o / --output"
  ensure_param "RUN_MODE" "-m / --mode"

  if [[ "${#SAMPLES[@]}" -eq 0 ]] && [[ "${#PROJECTS[@]}" -eq 0 ]]; then
    error "No samples or projects specified?"
    exit 1
  fi

  if [[ $RUN_MODE == local ]]; then
    if [[ "${#PROJECTS[@]}" -eq 0 ]] || [[ "${#SAMPLES[@]}" -gt 0 ]]; then
      error "Samples should be specified with --input / -i in local mode."
      exit 1
    fi
  fi

  local PRIMER_INFO_FILE PRIMER_RMD_FILE DB_INFO_FILE MOTHUR_MODE TAX_REF REF_ALN
  eval_vars_from_func PRIMER_INFO_FILE PRIMER_RMD_FILE DB_INFO_FILE MOTHUR_MODE TAX_REF REF_ALN -- \
    get_primer_info "$PRIMERS" "$REM_LIN_OVERRIDE" "$TARGET_OVERRIDE" "n" "" "$DATABASE_OVERRIDE"

  if [[ "${NO_CONTROLS}" != "y" ]]; then
    ensure_param "MOCK_REF_ID" "--mock" "You must specify the ID of a mock community, --list_mock to see available communities."
    ensure_param "POS_CONTROLS" "-p / --positive"
    # ensure_param "NEG_CONTROLS" "-n / --negative"  # Not required?
  else
    warn "Not using controls!"

    if [[ -n ${POS_CONTROLS-} ]] || [[ -n ${NEG_CONTROLS-} ]]; then
      error "But some were provided anyway? Aborting."
      exit 1
    fi

    warn "Using controls is good practice!" # TODO expand

    declare -a POS_CONTROLS NEG_CONTROLS
  fi

  # Load specific mock info
  local MOCK_REF MOCK_REF_NAME MOCK_REF_DESC
  if [[ "${NO_CONTROLS}" != "y" ]]; then
    eval_vars_from_func MOCK_REF MOCK_REF_NAME MOCK_REF_DESC -- \
      get_mock_info "${MOCK_REF_ID}"

    # Check the requirements for using the mock community analysis
    if [[ ! -f $MOCK_REF ]]; then
      error "Mock reference file missing" >&2
      exit 1
    else
      info "Mock community analysis parameters seem ok. Keep going..."
    fi
  fi

  # Check if the reference alignment and taxonomy exist
  if [[ "$MOTHUR_MODE" == "align" ]]; then
    if [[ ! -f $TAX_REF ]] || [[ ! -f $REF_ALN ]]; then
      error "The reference taxonomy file ${TAX_REF} and/or the reference alignment file ${REF_ALN} do not exist." >&2
      error "Check your Reference directory at ${REF_DIR}/db" >&2
      exit 1
    fi
  fi

  if [[ -e "$MY_DIR" ]] && [[ "${REMOVE_EXISTING_OUTPUT}" != "y" ]]; then
    error "\n$MY_DIR already exists.\nChoose a different folder or use \"$(cmd) --force\" to overwrite it."
    exit 1
  fi

  local MOTHUR_VERSION
  MOTHUR_VERSION=$(find_version mothur | grep version= | cut -f 2 -d '=')

  ## Create main output directories
  if [[ "${REMOVE_EXISTING_OUTPUT}" == "y" ]]; then
    rm -rf "${MY_DIR}"
    mkdir -p "${MY_DIR}"
  else
    mkdir -p "${MY_DIR}"
  fi

  mkdir -p "${TEMP_DIR}/mothur_run"
  mkdir -p "${MY_DIR}/report"
  mkdir -p "${MY_DIR}/report/files"
  mkdir -p "${MY_DIR}/report/files/mothur"
  mkdir -p "${MY_DIR}/postprocessing"

  cd "${MY_DIR}"

  ## save first set of variables
  {
    # Run info
    declare -p RUN_ID MY_DIR TEMP_DIR MOTHUR_PREFIX
    # Sample/run info/config
    declare -p PROJECTS PROJECT_CTL SAMPLES POS_CONTROLS NEG_CONTROLS MINSUBSAMPLE RUN_MODE PRIMERS
    declare -p TARGET_OVERRIDE REM_LIN_OVERRIDE DATABASE_OVERRIDE CLUSTER_CUTOFF FLAG_ONLY_R1 MOTHUR_MODE CLUSTER_SPLIT
    # Version infos
    declare -p MOTHUR_VERSION
    # Path info (*NOT* to be load_vars'd from vars.txt, as paths may change between attempts)
    declare -p REF_DIR TAX_REF REF_ALN SCRIPTS MOTHUR
  } >"${MY_DIR}/postprocessing/vars.txt"

  cp "${PRIMER_RMD_FILE}" "${MY_DIR}/postprocessing/primers.Rmd"
  cp "${PRIMER_INFO_FILE}" "${MY_DIR}/postprocessing/primers.info"
  cp "${DB_INFO_FILE}" "${MY_DIR}/postprocessing/db_info.info"
  if [[ "${NO_CONTROLS}" != "y" ]]; then
    cp "${MOCK_REFDIR}/${MOCK_REF_ID}.info" "${MY_DIR}/postprocessing/mock_community.info"
    # Mock info
    declare -p MOCK_REF_ID >> "${MY_DIR}/postprocessing/vars.txt"
  else
    echo 'declare -- MOCK_REF_ID=""' >> "${MY_DIR}/postprocessing/vars.txt"
  fi

  if [[ "$MOTHUR_MODE" == "noalign" ]] && [[ "${BUILD_TREE}" == "y" ]]; then
    error "Using a primer with MOTHUR_MODE=noalign and BUILD_TREE is incompatible!"
    exit 1
  fi
  if [[ "$MOTHUR_MODE" == "align" ]] && [[ "${FLAG_ONLY_R1}" == "y" ]]; then
    error "Using a primer with MOTHUR_MODE=align and FLAG_ONLY_R1 is incompatible!"
    exit 1
  fi
  if [[ "$MOTHUR_MODE" == "noalign" ]] && [[ "${CLUSTER_SPLIT}" == "y" ]]; then
    error "Using a primer with MOTHUR_MODE=align and CLUSTER_SPLIT is incompatible!"
    exit 1
  fi
  declare -p BUILD_TREE >>"${MY_DIR}/postprocessing/vars.txt"

  if [[ "${BUILD_TREE}" == "y" ]]; then
    local FASTTREE_VERSION
    FASTTREE_VERSION=$(find_version FastTree | cut -f 1 -d ' ')
    declare -p FASTTREE_VERSION >>"${MY_DIR}/postprocessing/vars.txt"
  fi

  declare -p NO_CONTROLS >>"${MY_DIR}/postprocessing/vars.txt"
}

function otureporter_prepare_samples() {
  # Other stages:
  local RUN_MODE NO_CONTROLS PROJECT_CTL POS_CONTROLS NEG_CONTROLS PROJECTS SAMPLES
  # Outputs:
  local PROJECTS POS_CONTROLS NEG_CONTROLS ALL_SAMPLES SAMPLE_MAP_DISPLAY_NAME NUM_SAMPLES POSCTL NEGCTL

  local tag i all_samples sample_id

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" RUN_MODE NO_CONTROLS PROJECT_CTL POS_CONTROLS NEG_CONTROLS PROJECTS SAMPLES)
  eval "$vars_decl"

  if [[ $RUN_MODE == bs ]]; then
    update_status_progress 1 "Checking basespace"

    local CONTROL_COUNT ALL_SAMPLES

    # prefix_each output purposefully unquoted
    local bs_args=()
    if [[ "${#SAMPLES[@]}" -gt 0 ]]; then
      bs_args+=($(prefix_each " --sample " "${SAMPLES[@]}"))
    fi
    if [[ "${#POS_CONTROLS[@]}" -gt 0 ]]; then
      bs_args+=($(prefix_each " --pos " "${POS_CONTROLS[@]}"))
    fi
    if [[ "${#NEG_CONTROLS[@]}" -gt 0 ]]; then
      bs_args+=($(prefix_each " --neg " "${NEG_CONTROLS[@]}"))
    fi
    if [[ "${#PROJECTS[@]}" -gt 0 ]]; then
      bs_args+=($(prefix_each " --project " "${PROJECTS[@]}"))
    fi

    call_py_script -m scripts.basespace \
      "${bs_args[@]}" \
      --ctl_project "$PROJECT_CTL" \
      --dest "${TEMP_DIR}/mothur_run" \
      --info "${MY_DIR}/report/files/LIMSID_to_sampleID.tsv" \
      --script
  elif [[ $RUN_MODE == local ]]; then
    get_local_tag() {
      local sample_id=$1
      local path=${LOCAL_SAMPLE_PATHS[$sample_id]}
      local sample_dir=$(dirname "$path")
      local sample_name=${LOCAL_SAMPLE_NAMES[$sample_id]}
      local sample_filename=$(basename "$path")

      local ext1=${sample_filename/$sample_name/}
      local ext2=${ext1/_R1_001/_R2_001}
      local newext1=$(echo "${ext1%.gz}" | awk '{ print gensub(/_S[0-9]+(_L[0-9][0-9][0-9]_R1_001)/, "\\1", "g", $1);}')
      local newext2=${newext1/_R1_001/_R2_001}

      local orig_r1="$sample_dir/$sample_name$ext1"
      local orig_r2="$sample_dir/$sample_name$ext2"
      local new_r1="$sample_id$newext1"
      local new_r2="$sample_id$newext2"

      gunzip -c "$orig_r1" >"$TEMP_DIR/mothur_run/$new_r1"
      gunzip -c "$orig_r2" >"$TEMP_DIR/mothur_run/$new_r2"

      local entry_count=$(($(wc -l "$TEMP_DIR/mothur_run/$new_r1" | cut -d ' ' -f 1) / 4))
      echo -e "${sample_id}\t${path}\t$entry_count\t${sample_name}" >>"${MY_DIR}/report/files/LIMSID_to_sampleID.tsv"
    }

    update_status_progress 2 "Retrieving samples"

    local_load_samples_from_args "PROJECTS" "$PROJECT_CTL" "POS_CONTROLS" "NEG_CONTROLS"
    local_save_globals >>"${MY_DIR}/postprocessing/vars.txt"

    # PROJECTS is left unchanged
    all_samples=("${LOCAL_ALL_SAMPLES[@]}")
    if ((${#LOCAL_POS_CONTROL_SAMPLES[@]})); then
      all_samples+=("${LOCAL_POS_CONTROL_SAMPLES[@]}")

      LOADED_POS_CONTROLS=("${LOCAL_POS_CONTROL_SAMPLES[@]-}")
    fi
    if ((${#LOCAL_NEG_CONTROL_SAMPLES[@]})); then
      all_samples+=("${LOCAL_NEG_CONTROL_SAMPLES[@]}")

      LOADED_NEG_CONTROLS=("${LOCAL_NEG_CONTROL_SAMPLES[@]-}")
    fi

    for idx in "${!all_samples[@]}"; do
      sample_id=${all_samples[$idx]}

      get_local_tag "$sample_id"
    done

    ALL_SAMPLES=${#LOCAL_ALL_SAMPLES[@]}
    CONTROL_COUNT=$((${#LOCAL_POS_CONTROL_SAMPLES[@]} + ${#LOCAL_NEG_CONTROL_SAMPLES[@]}))
  fi

  declare -p PROJECTS LOADED_POS_CONTROLS LOADED_NEG_CONTROLS >>"${MY_DIR}/postprocessing/vars.txt"

  local -A SAMPLE_MAP_DISPLAY_NAME
  load_sample_map "SAMPLE_MAP" "${MY_DIR}/report/files/LIMSID_to_sampleID.tsv"

  if [[ "${NO_CONTROLS}" != "y" ]]; then
    local pos_controls_disp=("${LOADED_POS_CONTROLS[@]-}")
    local neg_controls_disp=("${LOADED_NEG_CONTROLS[@]-}")

    map_sample_display_names "SAMPLE_MAP_DISPLAY_NAME" "pos_controls_disp"
    map_sample_display_names "SAMPLE_MAP_DISPLAY_NAME" "neg_controls_disp"

    POSCTL=$(implode ", " "${pos_controls_disp[@]-}")
    NEGCTL=$(implode ", " "${neg_controls_disp[@]-}")
  else
    POSCTL="None used"
    NEGCTL="None used"
  fi

  declare -p NEGCTL POSCTL PROJECTS >>"${MY_DIR}/postprocessing/vars.txt"

  NUM_SAMPLES=$((ALL_SAMPLES - CONTROL_COUNT))
  declare -p ALL_SAMPLES NUM_SAMPLES SAMPLE_MAP_DISPLAY_NAME >>"${MY_DIR}/postprocessing/vars.txt"
  info "Loaded $ALL_SAMPLES samples and $CONTROL_COUNT controls."
}

##############
### MOTHUR ###
##############

function otureporter_prepare_mothur_scripts() {
  # Other stages:
  local PRIMERS REM_LIN_OVERRIDE TARGET_OVERRIDE DATABASE_OVERRIDE FLAG_ONLY_R1
  # Outputs:
  local PREGROUP= GROUP= NAME_GROUPS=

  local tag i R1 R2

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" PRIMERS REM_LIN_OVERRIDE TARGET_OVERRIDE DATABASE_OVERRIDE FLAG_ONLY_R1)
  eval "$vars_decl"

  local MOTHUR_MODE TAX_REF REF_ALN
  eval_vars_from_func MOTHUR_MODE TAX_REF REF_ALN -- \
    get_primer_info "$PRIMERS" "$REM_LIN_OVERRIDE" "$TARGET_OVERRIDE" "n" "" "$DATABASE_OVERRIDE"

  update_status_progress 3 "Preparing mothur scripts"
  cd "${TEMP_DIR}/mothur_run"

  if [[ $MOTHUR_MODE == align ]]; then
    info "Constructing '${MOTHUR_PREFIX}.files'"

    ## This substitutes make.file() as it has a bug in mothur 1.39.5
    # for i in *R1*fastq.gz; do
    # 	tag=$(echo ${i} | awk '{gsub(/_L001_R1_001\.fastq\.gz/, ""); print}')
    # 	R1=$(pwd)/${tag}_L001_R1_001.fastq.gz
    # 	R2=$(pwd)/${tag}_L001_R2_001.fastq.gz
    # 	echo -ne "${tag}\t$(readlink ${R1})\t$(readlink ${R2})\n"
    # done > ${MOTHUR_PREFIX}.files

    for i in "${TEMP_DIR}/mothur_run/"*R1*fastq; do
      tag=$(basename "$(echo "${i}" | awk '{gsub(/_L[0-9][0-9][0-9]_R1_001\.fastq/, ""); print}')")
      R1=${TEMP_DIR}/mothur_run/${tag}_L001_R1_001.fastq
      R2=${TEMP_DIR}/mothur_run/${tag}_L001_R2_001.fastq
      echo -ne "${tag}\t${R1}\t${R2}\n"
    done >${MOTHUR_PREFIX}.files
  elif [[ $MOTHUR_MODE == noalign ]]; then
    info "Preparing ITS fastq files"

    function preproc_its() {
      source "$OTUREPORTER_DIR/scripts/common.sh"

      R1=${TEMP_DIR}/mothur_run/${1}_L001_R1_001.fastq
      R2=${TEMP_DIR}/mothur_run/${1}_L001_R2_001.fastq

      # This function concatenates R1 and R2 and do mothur-based quality trimming
      # Makes sequence names unique across R1 and R2
      if [[ "$FLAG_ONLY_R1" == "y" ]]; then
        tr ' ' '_' <"$R1" >"${1}.fastq"
      else
        cat "$R1" "$R2" | tr ' ' '_' >"${1}.fastq"
      fi

      wrap_mothur "#fastq.info(fastq=${1}.fastq);
          trim.seqs(fasta=current, qfile=current, qwindowaverage=25, qwindowsize=50)"

      rm "$R1" "$R2" "${1}.scrap.fasta"
    }
    export -f preproc_its
    export TEMP_DIR OTUREPORTER_DIR FLAG_ONLY_R1

    # Instead of creating a *.files, the ITS pipeline needs a
    for i in "${TEMP_DIR}/mothur_run/"*R1*fastq; do
      basename "$(echo "${i}" | awk '{gsub(/_L[0-9][0-9][0-9]_R1_001\.fastq/, ""); print}')"
    done | parallel -j "${NUMPROC_GLOBAL}" preproc_its {}

    # un-export variables used by parallelized function
    typeset +x OTUREPORTER_DIR FLAG_ONLY_R1

    cat "$TEMP_DIR/mothur_run/"*.trim.fasta >"$TEMP_DIR/mothur_run/$MOTHUR_PREFIX.fasta"

    # shellcheck disable=SC2035
    PREGROUP=$(implode "-" *.trim.fasta)                                    ## dash-separated list of the fasta option in mothur
    GROUP=${PREGROUP%-}                                                     ## clean the final dash in the $PREGROUP var
    NAME_GROUPS=$(echo "$GROUP" | awk '{gsub(/\.trim\.fasta/, ""); print}') ##Get names for "groups" option
  else
    error "Unhandled MOTHUR_MODE=$MOTHUR_MODE"
    exit 1
  fi

  declare -p PREGROUP GROUP NAME_GROUPS >>"${MY_DIR}/postprocessing/vars.txt"
}

function otureporter_prepare_batch() {
  # Other stages:
  local MOCK_REF_ID NAME_GROUPS GROUP NO_CONTROLS LOADED_POS_CONTROLS PRIMERS REM_LIN_OVERRIDE TARGET_OVERRIDE DATABASE_OVERRIDE CLUSTER_CUTOFF MOTHUR_VERSION
  # Outputs:
  local NAME_UNIQUE_LIST

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" MOCK_REF_ID NAME_GROUPS GROUP NO_CONTROLS LOADED_POS_CONTROLS PRIMERS REM_LIN_OVERRIDE TARGET_OVERRIDE DATABASE_OVERRIDE CLUSTER_CUTOFF MOTHUR_VERSION CLUSTER_SPLIT)
  eval "$vars_decl"

  # Load specific primer info
  local TARGET REM_LIN REF_SEQ_START DIFFS MAXLEN MINLEN MOTHUR_MODE REF_SEQ_END REF_ALN TAX_REF
  eval_vars_from_func TARGET REM_LIN REF_SEQ_START DIFFS MAXLEN MINLEN MOTHUR_MODE REF_SEQ_END REF_ALN TAX_REF -- \
    get_primer_info "$PRIMERS" "$REM_LIN_OVERRIDE" "$TARGET_OVERRIDE" --quiet "" "$DATABASE_OVERRIDE"

  # Load specific mock info
  local MOCK_REF
  if [[ "${NO_CONTROLS}" != "y" ]]; then
    eval_vars_from_func MOCK_REF -- \
      get_mock_info "$MOCK_REF_ID"
  fi

  cd "${TEMP_DIR}/mothur_run"
  info "Starting mothur processing..."

  # Concatenate the controls to be removed after seq.error
  local mock_group="" ref_primer_suffix i

  info "Generating custom mothur batch files"
  cp "${MOTHUR}/${MOTHUR_MODE}/"01*.batch \
    "${MOTHUR}/${MOTHUR_MODE}/"03*.batch \
    "${MOTHUR}/${MOTHUR_MODE}/"04*.batch \
    "${MOTHUR}/${MOTHUR_MODE}/"05*.batch \
    "${TEMP_DIR}/mothur_run"

  debug "Replacing placeholders in mothur batch files"
  if [[ "${NO_CONTROLS}" != "y" ]]; then
    mock_group=$(echo "${LOADED_POS_CONTROLS[@]}" | tr " " "-")

    for i in "${LOADED_POS_CONTROLS[@]}"; do
      cp "${MOTHUR}/${MOTHUR_MODE}/02_get_error.batch" "${TEMP_DIR}/mothur_run/02_get_error.${i}.batch"
      sed -i "s|MOCK_GROUP|${i}|g" "${TEMP_DIR}/mothur_run/02_get_error.${i}.batch"
    done
  else
    # No controls to remove, so we'll skip it
    sed -i '/remove.groups/d' 03_get_shared_otus.batch
  fi

  ref_primer_suffix=$(second_to_last_extension "$REF_ALN")

  if version_greater_equal "1.40.0" "$MOTHUR_VERSION"; then
    NAME_UNIQUE_LIST=""
  else
    NAME_UNIQUE_LIST=".unique_list"
  fi
  declare -p NAME_UNIQUE_LIST >>"${MY_DIR}/postprocessing/vars.txt"

  # Replace placeholders in the batch files
  sed -i "s|data/raw|./|g" ./*.batch
  sed -i "s|PREFIX|${MOTHUR_PREFIX}|g" ./*.batch
  sed -i "s|MINLEN|${MINLEN}|g" ./*.batch
  sed -i "s|MAXLEN|${MAXLEN}|g" ./*.batch
  sed -i "s|DIFFS|${DIFFS}|g" ./*.batch
  sed -i "s|NAME_UNIQUE_LIST|${NAME_UNIQUE_LIST}|g" ./*.batch
  sed -i "s|NUMPROC_GLOBAL|${NUMPROC_GLOBAL}|g" ./*.batch
  sed -i "s|TAX_REF|${TAX_REF}|g" ./*.batch
  sed -i "s|REF_ALN|${REF_ALN}|g" ./*.batch
  sed -i "s|CLUSTER_CUTOFF|${CLUSTER_CUTOFF}|g" ./*.batch
  sed -i "s|PRIMERS|${ref_primer_suffix}|g" ./*.batch
  sed -i "s|MOCK_GROUP|${mock_group:-}|g" ./*.batch
  sed -i "s|MOCK_REF|${MOCK_REF:-}|g" ./*.batch
  sed -i "s|TARGET|${TARGET}|g" ./*.batch

  if [[ "${MOTHUR_MODE}" == "align" ]]; then
    sed -i "s|REF_START|${REF_SEQ_START}|g" ./*.batch
    sed -i "s|REF_END|${REF_SEQ_END}|g" ./*.batch
    sed -i "s|REM_LIN|${REM_LIN}|g" ./*.batch
    if [[ "${CLUSTER_SPLIT}" == "y" ]]; then
      sed -i "/cluster(/d" ./03_get_shared_otus.batch
      sed -i "/dist.seqs(/d" ./03_get_shared_otus.batch
      sed -i "s|TAXLEVEL|${TAXLEVEL}|g" ./03_get_shared_otus.batch
      sed -i "s|NUMPROC_SPLIT|${NUMPROC_SPLIT}|g" ./03_get_shared_otus.batch
    else
      sed -i "/cluster.split(/d" ./03_get_shared_otus.batch
    fi
  elif [[ "${MOTHUR_MODE}" == noalign ]]; then
    sed -i "s|NAME_GROUPS|${NAME_GROUPS}|g" ./*.batch
    sed -i "s|GROUP|${GROUP}|g" ./*.batch
  fi

  #noalign mode does not pre-cluster
  if [[ "${MOTHUR_MODE}" == "align" ]]; then
    PRECLUSTER='.precluster'
  elif [[ "${MOTHUR_MODE}" == noalign ]]; then
    PRECLUSTER=""
  fi
  declare -p PRECLUSTER >>"${MY_DIR}/postprocessing/vars.txt"
}

function otureporter_get_good_seqs() {
  local i f file

  cd "${TEMP_DIR}/mothur_run"
  update_status_progress 4 "Mothur: 01_get_good_seqs.batch"
  wrap_mothur 01_get_good_seqs.batch

  info "Cleaning up"
  debug "Remove the initial fasta+qual files (they occupy most of the space)"
  if [[ "${KEEP_INTERMEDIARY}" != "y" ]]; then
    rm -f ${MOTHUR_PREFIX}.{trim,scrap}.contigs.{qual,fasta,summary} ${MOTHUR_PREFIX}.trim.contigs.good.unique.{align,good.align,summary,good.summary} ${MOTHUR_PREFIX}.*.{map,rabund,dist}

    debug "Removing raw fastq and fastq.gz"

    while read -r file; do
      for f in "${file}_"*; do
        rm -f "${f}"
      done
    done < <(cut -f1 "${MOTHUR_PREFIX}.files")
  else
    for i in ${MOTHUR_PREFIX}.{trim,scrap}.contigs.{qual,fasta,summary} ${MOTHUR_PREFIX}.trim.contigs.good.unique.{align,good.align,summary,good.summary}; do
      if [[ -e "$i" ]]; then
        tar cvzf ${i}.tar.gz ${i}
        rm ${i}
      else
        warn "File doesn't exist: $i"
      fi

      debug "Compress raw fastq files"

      while read -r file; do
        for f in "${file}_"*; do
          gzip "${f}"
        done
      done < <(cut -f1 "${MOTHUR_PREFIX}.files")
    done
  fi
}

function otureporter_get_error() {
  # Other stages:
  local NO_CONTROLS LOADED_POS_CONTROLS PRIMERS REM_LIN_OVERRIDE TARGET_OVERRIDE DATABASE_OVERRIDE PRECLUSTER

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" NO_CONTROLS LOADED_POS_CONTROLS PRIMERS REM_LIN_OVERRIDE TARGET_OVERRIDE DATABASE_OVERRIDE MOTHUR_MODE PRECLUSTER)
  eval "$vars_decl"

  # Load specific primer info
  local REF_ALN
  eval_vars_from_func REF_ALN -- \
    get_primer_info "$PRIMERS" "$REM_LIN_OVERRIDE" "$TARGET_OVERRIDE" --quiet "" "$DATABASE_OVERRIDE"

  local i ref_primer_suffix
  cd "${TEMP_DIR}/mothur_run"

  if [[ "${NO_CONTROLS}" != "y" ]]; then
    ## Instead of the standard way of running the 02_get_error.batch, we run in
    ## independently for each positive control. So we get the error rate for each run.
    for i in "${LOADED_POS_CONTROLS[@]}"; do
      update_status_progress 5 "Mothur: 02_get_error.${i}.batch"
      mkdir -p seqerror
      mkdir -p "seqerror/${i}"
      wrap_mothur "02_get_error.${i}.batch"
      mv $(ls -t -w1 mothur.*.logfile | head -n 1) "seqerror/${i}"
      mv ./*.error.* "seqerror/${i}"
    done
  else
    info "Skipping 02_get_error as --no_controls was used!"

    ref_primer_suffix=$(second_to_last_extension "${REF_ALN}")

    # 02_get_error.batch will create these files with get.groups
    debug "--no_controls: Copying files to match what downstream expects"
    if [[ "${MOTHUR_MODE}" == "align" ]]; then
      debug "test1"
      cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick{,.pick}.fasta
      cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.denovo.vsearch.pick.pick{,.pick}.count_table
      cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.${ref_primer_suffix}.wang.pick{,.pick}.taxonomy
    elif [[ "${MOTHUR_MODE}" == "noalign" ]]; then
      debug "test"
      cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.fasta "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.pick.fasta
      cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good${PRECLUSTER}.denovo.vsearch.pick.pick.count_table "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.denovo.vsearch.pick.pick.pick.count_table
      cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.${ref_primer_suffix}.wang.pick.taxonomy "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.${ref_primer_suffix}.wang.pick.pick.taxonomy
    fi
      #statements
    # cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick{,.pick}.fasta
    # cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.denovo.vsearch.pick.pick{,.pick}.count_table
    # cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.${ref_primer_suffix}.wang.pick{,.pick}.taxonomy
  # else
  #   cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick{,.pick}.fasta
  #   cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.denovo.vsearch.pick.pick{,.pick}.count_table
  #   cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.${ref_primer_suffix}.wang.pick{,.pick}.taxonomy
  fi
}

function otureporter_get_shared_otus() {
  cd "${TEMP_DIR}/mothur_run"

  update_status_progress 6 "Mothur: 03_get_shared_otus.batch"
  wrap_mothur 03_get_shared_otus.batch
}

function otureporter_get_oturep() {
  # Other stages:
  local NO_CONTROLS PRIMERS REM_LIN_OVERRIDE TARGET_OVERRIDE DATABASE_OVERRIDE CLUSTER_CUTOFF NAME_UNIQUE_LIST PRECLUSTER
  # Outputs:
  local ALIGN_OPTI_MCC CLASSIFY_OTU_SUMMARY

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" NO_CONTROLS PRIMERS REM_LIN_OVERRIDE TARGET_OVERRIDE DATABASE_OVERRIDE CLUSTER_CUTOFF NAME_UNIQUE_LIST PRECLUSTER)
  eval "$vars_decl"

  # Load specific primer info
  local MOTHUR_MODE
  eval_vars_from_func MOTHUR_MODE -- \
    get_primer_info "$PRIMERS" "$REM_LIN_OVERRIDE" "$TARGET_OVERRIDE" --quiet "" "$DATABASE_OVERRIDE"

  cd "${TEMP_DIR}/mothur_run"

  # Whether there's a ".opti_mcc" section in the name, depends on the `cluster`
  # call in 03_get_shared_otus.batch.
  declare ALIGN_OPTI_MCC
  # summary output file from classify.otu in 03_get_shared_otus.batch.
  declare CLASSIFY_OTU_SUMMARY

  if [[ "$MOTHUR_MODE" == "align" ]]; then
    ALIGN_OPTI_MCC=".opti_mcc"
    CLASSIFY_OTU_SUMMARY="$MOTHUR_PREFIX.trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.cons.tax.summary"
  else
    ALIGN_OPTI_MCC=".opti"
    CLASSIFY_OTU_SUMMARY="$MOTHUR_PREFIX.good.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.${CLUSTER_CUTOFF}.cons.tax.summary"
  fi

  declare -p ALIGN_OPTI_MCC CLASSIFY_OTU_SUMMARY >>"${MY_DIR}/postprocessing/vars.txt"

  if [[ "${NO_CONTROLS}" == "y" ]]; then
    debug "--no_controls: Copying files to match what downstream expects"
    if [[ "${MOTHUR_MODE}" == "align" ]]; then
      debug "debug align"
      # if [[ "${NO_CONTROLS}" == "y" ]]; then
      #   cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick{,.pick}${ALIGN_OPTI_MCC}.list
      #   cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick{,.pick}${ALIGN_OPTI_MCC}.shared
      #   cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick{,.pick}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.cons.taxonomy
      #
      #   cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick{,.pick}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.cons.tax.summary
      # else
      if [[ "${CLUSTER_SPLIT}" == "y" ]]; then
        cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.list "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}.list
        cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.shared "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}.shared
        cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.${CLUSTER_CUTOFF}.cons.taxonomy "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.cons.taxonomy
        cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.${CLUSTER_CUTOFF}.cons.tax.summary "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.cons.tax.summary
      else
        cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick{,.pick}${ALIGN_OPTI_MCC}.list
        cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick{,.pick}${ALIGN_OPTI_MCC}.shared
        cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick{,.pick}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.cons.taxonomy
        cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick{,.pick}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.cons.tax.summary
      fi
    elif [[ "${MOTHUR_MODE}" == "noalign" ]]; then
      debug "debug noalign"
      #cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.pick.fasta "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.pick.fasta
      # cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.denovo.vsearch.pick.pick.pick.count_table "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.denovo.vsearch.pick.pick.pick.count_table
      # cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.list "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.list
      # cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.shared "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.shared
      # cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.${CLUSTER_CUTOFF}.cons.taxonomy "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.${CLUSTER_CUTOFF}.cons.taxonomy
      # cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.${CLUSTER_CUTOFF}.cons.tax.summary "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".good.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.${CLUSTER_CUTOFF}.cons.tax.summary
    fi
  elif [[ "${NO_CONTROLS}" != "y" ]] && [[ "${CLUSTER_SPLIT}" == "y" ]]; then
    cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.list "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}.list
    cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.shared "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}.shared
    cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.${CLUSTER_CUTOFF}.cons.taxonomy "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.cons.taxonomy
    cp "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}${NAME_UNIQUE_LIST}.${CLUSTER_CUTOFF}.cons.tax.summary "$TEMP_DIR/mothur_run/$MOTHUR_PREFIX".trim.contigs.good.unique.good.filter.unique${PRECLUSTER}.pick.pick.pick${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.cons.tax.summary
  fi
  # fi
  update_status_progress 7 "Mothur: 04_get_oturep.batch"
  wrap_mothur 04_get_oturep.batch
}

# Current files saved by mothur:
# shared=${MOTHUR_PREFIX}.opti_mcc.shared
# constaxonomy=${MOTHUR_PREFIX}.cons.taxonomy
# count=${MOTHUR_PREFIX}.count_table
# fasta=${MOTHUR_PREFIX}.fasta
# list=${MOTHUR_PREFIX}.opti_mcc.list
# taxonomy=${MOTHUR_PREFIX}.taxonomy

function otureporter_subsample_sequences() {
  # Outputs:
  local BAD_COUNT BAD_LIST GOOD_COUNT SUBSAMPLE PREBAD
  # Other stages:
  local ALIGN_OPTI_MCC MINSUBSAMPLE SAMPLE_MAP_DISPLAY_NAME PRECLUSTER

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" ALIGN_OPTI_MCC MINSUBSAMPLE SAMPLE_MAP_DISPLAY_NAME MOTHUR_MODE PRECLUSTER)
  eval "$vars_decl"

  cd "${TEMP_DIR}/mothur_run"
  local i

  if [[ $MOTHUR_MODE == "noalign" ]]; then
    local ALIGN_OPTI_MCC=""
  fi

  ## Gather different subsampling number depending if a minimum is required. Creates subsets of the count.summary files
  info "A minimum of subsampling level of $MINSUBSAMPLE sequences per sample has been requested.\nRemoving samples not below that value..."
  awk -v "i=${MINSUBSAMPLE}" '$2>=i{print}' ${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.count.summary > good_samples.tsv
  awk -v "i=${MINSUBSAMPLE}" '$2<i{print}' ${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.count.summary > samples_below_minsubsample.tsv

  SUBSAMPLE=$(cut -f2 good_samples.tsv | sort -n | head -n 1)
  GOOD_COUNT=$(awk 'END{print NR}' good_samples.tsv)
  BAD_COUNT=$(awk 'END{print NR}' samples_below_minsubsample.tsv)
  PREBAD=($(cut -f1 samples_below_minsubsample.tsv))
  # Map against the contents of SAMPLE_MAP_DISPLAY_NAME to find the "Display" names
  map_sample_display_names "SAMPLE_MAP_DISPLAY_NAME" "PREBAD"
  BAD_LIST="$(echo "${PREBAD[@]-}" | awk '{gsub(/ /, ", "); print}')"

  if [[ $GOOD_COUNT -eq 0 ]]; then
    info "Sample sequence counts:\n$(cat "${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.count.summary")"
    error "No samples had the required number of sequences!"
    exit 1
  fi

  if [[ -z $BAD_LIST ]]; then
    BAD_LIST=NA
    declare -p BAD_LIST >>"${MY_DIR}/postprocessing/vars.txt"
  fi

  #save vars
  declare -p SUBSAMPLE GOOD_COUNT BAD_COUNT PREBAD BAD_LIST >>"${MY_DIR}/postprocessing/vars.txt"

  info "Subsampling at ${SUBSAMPLE} sequences per sample."

  if [[ $BAD_COUNT -gt 0 ]]; then
    info "Removing the following samples:"
    cut -f1 samples_below_minsubsample.tsv
  else
    info "Every sample contains at least ${SUBSAMPLE} sequences. No need to discard any samples."
    #touch samples_below_minsubsample.tsv
  fi

  sed -i "s|SUBSAMPLE|${SUBSAMPLE}|g" 05_get_subsample.batch

  update_status_progress 8 "Mothur: 05_get_subsample.batch | Subsampling at ${SUBSAMPLE} sequences per sample."
  wrap_mothur 05_get_subsample.batch

  # Mothur versions following 9c603b6 (first in 1.43.0) no longer produce groups.summary
  #     9c603b6 - Modifies summary.single to produce rarefied OR summary results, not both
  if [[ ! -e "${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.groups.summary" ]]; then
    wrap_mothur "#summary.single(shared=${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.shared, calc=nseqs-sobs-coverage-invsimpson)"
  fi
}

function otureporter_prepare_report_data() {
  # Other stages:
  local ALIGN_OPTI_MCC PRECLUSTER

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" ALIGN_OPTI_MCC MOTHUR_MODE PRECLUSTER)
  eval "$vars_decl"

  local i
  cd "${TEMP_DIR}/mothur_run"

  if [[ $MOTHUR_MODE == "noalign" ]]; then
    local ALIGN_OPTI_MCC=""
  fi

  update_status_progress 9 "Preparing data for report"
  ## Get summary for non-subsampled data
  #grab the header
  head -n 1 ${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.shared >"${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.shared"
  #extract records from full-shared file
  for i in $(cut -f1 samples_below_minsubsample.tsv); do
    awk -v "i=${i}" '$2 ~ i' "${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.shared"
  done >>"${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.shared"
}

function otureporter_summarize_bad_samples() {
  # Other stage vars:
  local ALIGN_OPTI_MCC BAD_COUNT
  # Outputs:
  local BAD

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" ALIGN_OPTI_MCC BAD_COUNT MOTHUR_MODE)
  eval "$vars_decl"

  if [[ $MOTHUR_MODE == "noalign" ]]; then
    local ALIGN_OPTI_MCC=""
  fi

  cd "${TEMP_DIR}/mothur_run"
  local i line original_header

  if [[ $BAD_COUNT -ne 0 ]]; then
    info "Summarizing bad samples"

    BAD=yes
    declare -p BAD >>"${MY_DIR}/postprocessing/vars.txt"
    # groupmode=F as mothur emits an empty group column when there is only one group in "${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.shared"
    wrap_mothur "#summary.single(shared=${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.shared, calc=nseqs-sobs-coverage-invsimpson, groupmode=F)"

    # Construct "${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.groups.summary" from each of the group summaries
    rm -f "${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.groups.summary"
    for i in $(cut -d '	' -f2 "${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.shared" | tail -n +2); do
      debug "Collecting and mangling $i"

      if [[ ! -e "${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.groups.summary" ]]; then
        # Mangle the header from the first group
        original_header=$(head -n1 "${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.$i.summary")

        echo -e "$(echo "$original_header" | cut -f 1)\tgroup\t$(echo "$original_header" | cut -f 2-)" >"${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.groups.summary"
      fi

      # Mangle each line to include the group
      while IFS="" read -r line || [ -n "$line" ]; do
        echo -e "$(echo "$line" | cut -f 1)\t$i\t$(echo "$line" | cut -f "2-")" >>"${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.groups.summary"
      done < <(tail -n +2 "${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.$i.summary")
    done
  else
    BAD=no
    declare -p BAD >>"${MY_DIR}/postprocessing/vars.txt"
    touch "${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.groups.summary"
  fi
}

function otureporter_copy_report_files() {
  # Other stages:
  local ALIGN_OPTI_MCC CLASSIFY_OTU_SUMMARY CLUSTER_CUTOFF

  ########################
  ### Copying files... ###
  ########################
  cd "${MY_DIR}"

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" ALIGN_OPTI_MCC CLASSIFY_OTU_SUMMARY CLUSTER_CUTOFF MOTHUR_MODE)
  eval "$vars_decl"

  if [[ $MOTHUR_MODE == "noalign" ]]; then
    local ALIGN_OPTI_MCC=""
  fi

  info "Copying select mothur output files."
  ## need to collect the exact files to copy

  debug "Copying files related to the full dataset."
  mkdir -p "${MY_DIR}/report/files/mothur/full"
  cp "${TEMP_DIR}/mothur_run/${CLASSIFY_OTU_SUMMARY}" "${MY_DIR}/report/files/mothur/full"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}.cons.taxonomy" "${MY_DIR}/report/files/mothur/full"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.groups.rarefaction" "${MY_DIR}/report/files/mothur/full"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.groups.rarefaction" "${MY_DIR}/postprocessing/rarefaction.table"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.biom" "${MY_DIR}/report/files/mothur/full"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.shared" "${MY_DIR}/report/files/mothur/full"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}.count_table" "${MY_DIR}/report/files/mothur/full"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.list" "${MY_DIR}/report/files/mothur/full"

  # Subsampled
  debug "Copying files related to the full subsampled."
  mkdir -p "${MY_DIR}/report/files/mothur/subsample"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}.subsample.tax.summary" "${MY_DIR}/report/files/mothur/subsample"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.groups.summary" "${MY_DIR}/report/files/mothur/subsample"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.groups.summary" "${MY_DIR}/postprocessing/stats_full.tsv"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.groups.ave-std.summary" "${MY_DIR}/report/files/mothur/subsample"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.groups.ave-std.summary" "${MY_DIR}/postprocessing/stats_good_subsample.tsv"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.subsample.shared" "${MY_DIR}/report/files/mothur/subsample"
  cp "${TEMP_DIR}/mothur_run/good_samples.tsv" "${MY_DIR}/report/files/mothur/subsample"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.subsample.${CLUSTER_CUTOFF}.biom" "${MY_DIR}/report/files/mothur/subsample"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.subsample.${CLUSTER_CUTOFF}.count_table" "${MY_DIR}/report/files/mothur/subsample"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.subsample.list" "${MY_DIR}/report/files/mothur/subsample"

  # Bad
  debug "Copying files related to the bad subsamples."
  mkdir -p "${MY_DIR}/report/files/mothur/bad_subsample"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.groups.summary" "${MY_DIR}/report/files/mothur/bad_subsample" || echo "No bad samples"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.shared" "${MY_DIR}/report/files/mothur/bad_subsample" || echo "No bad samples"
  cp "${TEMP_DIR}/mothur_run/samples_below_minsubsample.tsv" "${MY_DIR}/report/files/mothur/bad_subsample" || echo "No bad samples"

  cp "${MY_DIR}/report/files/mothur/bad_subsample/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.bad.groups.summary" "${MY_DIR}/postprocessing/stats_bad.tsv"

  # Representative sequences
  debug "Copying representative sequences"
  mkdir -p "${MY_DIR}/report/files/mothur/rep_seqs"
  cp "${TEMP_DIR}/mothur_run/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.rep."{count_table,fasta} "${MY_DIR}/report/files/mothur/rep_seqs"
}

function otureporter_parse_error_rates() {
  # Globals
  local NO_CONTROLS
  # Other stages:
  local LOADED_POS_CONTROLS

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" NO_CONTROLS LOADED_POS_CONTROLS)
  eval "$vars_decl"

  info "Parse error rates to table that can be imported into the R report"
  cd "${TEMP_DIR}/mothur_run"

  local rate i
  if [[ "${NO_CONTROLS}" != "y" ]]; then
    for i in "${LOADED_POS_CONTROLS[@]}"; do
      rate=$(grep "Overall error rate" "seqerror/${i}/"mothur* | cut -f2)
      echo -en "${i}\t${rate}\n"
    done >"${MY_DIR}/report/files/error_rates.tsv"
  else
    echo -en "N/A\tN/A\n" >"${MY_DIR}/report/files/error_rates.tsv"
  fi

  info "Saving mothur logfiles"
  cd "${TEMP_DIR}/mothur_run"
  tar cvzf "${MY_DIR}/report/files/mothur/mothur_logfiles.tar.gz" mothur.*.logfile
}

function otureporter_generate_tree() {
  # Options:
  local BUILD_TREE
  # Other stages:
  local ALIGN_OPTI_MCC CLUSTER_CUTOFF
  # Outputs:
  local TREE_GENERATED

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" ALIGN_OPTI_MCC CLUSTER_CUTOFF BUILD_TREE)
  eval "$vars_decl"

  cd "${TEMP_DIR}/mothur_run"

  if [[ "${BUILD_TREE}" == "y" ]]; then
    update_status_progress 10 "Generating tree with FastTree"

    # Load variables from other stages
    local vars_decl
    vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" BUILD_TREE ALIGN_OPTI_MCC BUILD_TREE)
    eval "$vars_decl"

    #####################################
    ### Generating tree with FastTree ###
    #####################################
    mkdir -p "${MY_DIR}/report/files/FastTree"
    "${SCRIPTS}/repseq_renamer.py" "${MY_DIR}/report/files/mothur/rep_seqs/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.rep.fasta" >"${MY_DIR}/report/files/FastTree/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.rep.rename.fasta"
    FastTree -log "${MY_DIR}/report/files/FastTree/${MOTHUR_PREFIX}_FastTree.log" -fastest -nt -gtr -gamma "${MY_DIR}/report/files/FastTree/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.rep.rename.fasta" > "${MY_DIR}/report/files/FastTree/${MOTHUR_PREFIX}_FastTree.tree"
    if [[ -s "${MY_DIR}/report/files/FastTree/${MOTHUR_PREFIX}_FastTree.tree" ]]; then
      TREE_GENERATED=Yes
      cp ${MY_DIR}/report/files/FastTree/${MOTHUR_PREFIX}_FastTree.tree ${MY_DIR}/report/files/${MOTHUR_PREFIX}_FastTree.tree
    else
      TREE_GENERATED=No
    fi
  else
    TREE_GENERATED=NA
  fi

  declare -p TREE_GENERATED >>"${MY_DIR}/postprocessing/vars.txt"
}

############################
### Create Krona outputs ###
############################

function otureporter_create_krona_outputs() {
  # Other stages:
  local ALIGN_OPTI_MCC CLASSIFY_OTU_SUMMARY CLUSTER_CUTOFF

  local KRONA

  update_status_progress 11 "Creating Krona outputs"
  cd "${MY_DIR}/postprocessing"

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" ALIGN_OPTI_MCC CLASSIFY_OTU_SUMMARY CLUSTER_CUTOFF MOTHUR_MODE)
  eval "$vars_decl"

  if [[ $MOTHUR_MODE == "noalign" ]]; then
    local ALIGN_OPTI_MCC=""
  fi

  ## FOR FULL DATASET:
  # Richness
  debug "Krona - Full data set - Richness - Creating XML"
  "${SCRIPTS}/mothur_krona_XML.py" "${MY_DIR}/report/files/mothur/full/${CLASSIFY_OTU_SUMMARY}" >"${MY_DIR}/postprocessing/tax_summary_report.full.xml"
  debug "Krona - Full data set - Richness - Creating chart"
  ktImportXML "${MY_DIR}/postprocessing/tax_summary_report.full.xml" -o "${MY_DIR}/postprocessing/richness_full.krona.html"

  # Diversity
  debug "Krona - Full data set - Diversity - Creating biom to tsv"
  biom convert -i "${MY_DIR}/report/files/mothur/full/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.biom" -o "${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_mothur_biom.reformated.full.tsv" --to-tsv --table-type "OTU table" --output-metadata-id "Domain; Phylum; Class; Order; Family; Genus" --header-key taxonomy
  biom convert -i "${MY_DIR}/report/files/mothur/full/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.biom" -o "${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_mothur_biom.reformated.full.json.biom" --to-json --table-type "OTU table" --output-metadata-id "Domain; Phylum; Class; Order; Family; Genus" --header-key taxonomy

  debug "Krona - Full data set - Diversity - OTUsamples2krona"
  sed 's/ //g' "${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_mothur_biom.reformated.full.tsv" | tail -n +2 | cut -f1 --complement | sed 's/\.0\t/\t/g' >"${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_mothur_biom.reformated.full.fix.tsv"
  "${SCRIPTS}/OTUsamples2krona.sh" "${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_mothur_biom.reformated.full.fix.tsv"
  debug "Krona - Full data set - Diversity - Creating chart"
  KRONA=($(ls "${MY_DIR}/postprocessing/krona"/*.html))
  ktImportKrona "${KRONA[@]-}" -o "${MY_DIR}/postprocessing/diversity_full.krona.html"

  tar cvzf "${MY_DIR}/report/files/krona_single.full.tar.gz" krona/
  rm -r krona

  ## FOR SUBSAMPLED DATASET:
  ## Richness
  debug "Krona - Subsampled data set - Richness - Creating XML"
  "${SCRIPTS}/mothur_krona_XML.py" "${MY_DIR}/report/files/mothur/subsample/${MOTHUR_PREFIX}.subsample.tax.summary" >"${MY_DIR}/postprocessing/tax_summary_report.subsample.xml"
  debug "Krona - Subsampled data set - Richness - Creating chart"
  ktImportXML "${MY_DIR}/postprocessing/tax_summary_report.subsample.xml" -o "${MY_DIR}/postprocessing/richness_subsample.krona.html"

  ## Diversity
  debug "Krona - Subsampled data set - Diversity - Creating biom to tsv"
  biom convert -i "${MY_DIR}/report/files/mothur/subsample/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.subsample.${CLUSTER_CUTOFF}.biom" -o "${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_mothur.reformated.subsample.tsv" --to-tsv --table-type "OTU table" --output-metadata-id "Domain; Phylum; Class; Order; Family; Genus" --header-key taxonomy
  biom convert -i "${MY_DIR}/report/files/mothur/subsample/${MOTHUR_PREFIX}${ALIGN_OPTI_MCC}.${CLUSTER_CUTOFF}.subsample.${CLUSTER_CUTOFF}.biom" -o "${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_mothur_biom.reformated.subsample.json.biom" --to-json --table-type "OTU table" --output-metadata-id "Domain; Phylum; Class; Order; Family; Genus" --header-key taxonomy

  debug "Krona - Subsampled data set - Diversity - OTUsamples2krona"
  sed 's/ //g' "${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_mothur.reformated.subsample.tsv" | tail -n +2 | cut -f1 --complement | sed 's/\.0\t/\t/g' >"${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_mothur_biom.reformated.subsample.fix.tsv"
  "${SCRIPTS}/OTUsamples2krona.sh" "${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_mothur_biom.reformated.subsample.fix.tsv"
  debug "Krona - Subsampled data set - Diversity - Creating chart"
  KRONA=($(ls "${MY_DIR}/postprocessing/krona"/*.html))
  ktImportKrona "${KRONA[@]-}" -o "${MY_DIR}/postprocessing/diversity_subsample.krona.html"

  debug "Archiving the contents of krona/"
  tar cvzf "${MY_DIR}/report/files/krona_single.subsample.tar.gz" krona/
  rm -r krona

  ## Copy files of interest
  info "Copying files of interest into ${MY_DIR}/postprocessing/"
  cp "${MY_DIR}/postprocessing/"{richness_full.krona.html,richness_subsample.krona.html,diversity_subsample.krona.html,diversity_full.krona.html} "${MY_DIR}/report/files"
  cp "${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_"mothur_biom.reformated.{subsample,full}.json.biom "${MY_DIR}/report/files"

  cp "${MY_DIR}/postprocessing/${MOTHUR_PREFIX}_mothur_biom.reformated.subsample.json.biom" "${MY_DIR}/postprocessing/mothur.biom"
}

function otureporter_generate_report() {
  # Globals:
  local RUN_ID RUN_MODE TREE_GENERATED
  # Other stages:
  local BAD_COUNT BAD_LIST NUM_SAMPLES MINSUBSAMPLE NEGCTL POSCTL PROJECTS PRIMERS REM_LIN_OVERRIDE TARGET_OVERRIDE DATABASE_OVERRIDE
  # Accesses primer/db info:
  local MOCK_REF_ID ALL_SAMPLES SUBSAMPLE

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" RUN_ID PROJECTS RUN_MODE TREE_GENERATED BAD_COUNT BAD_LIST NUM_SAMPLES MINSUBSAMPLE NEGCTL POSCTL MOCK_REF_ID ALL_SAMPLES SUBSAMPLE PRIMERS REM_LIN_OVERRIDE TARGET_OVERRIDE DATABASE_OVERRIDE)
  eval "$vars_decl"

  # Load specific primer info
  local MOTHUR_MODE REM_LIN TARGET RV RV_SEQ FW FW_SEQ DB_NAME_VER DB_DESCRIPTION_TEXT REF_ALN TAX_REF
  eval_vars_from_func MOTHUR_MODE REM_LIN TARGET RV RV_SEQ FW FW_SEQ DB_NAME_VER DB_DESCRIPTION_TEXT REF_ALN TAX_REF -- \
    get_primer_info "$PRIMERS" "$REM_LIN_OVERRIDE" "$TARGET_OVERRIDE" --quiet "" "$DATABASE_OVERRIDE"

  # Load specific mock info
  local MOCK_REF_NAME MOCK_REF
  if [[ "${NO_CONTROLS}" != "y" ]]; then
    eval_vars_from_func MOCK_REF_NAME MOCK_REF -- \
      get_mock_info "$MOCK_REF_ID"
  fi

  update_status_progress 13 "Generating reports"
  cd "${MY_DIR}/report"

  if [[ -f "${MY_DIR}/postprocessing/analysis.summary" ]]; then
    warn "Removing existing file: ${MY_DIR}/postprocessing/analysis.summary"
    rm "${MY_DIR}/postprocessing/analysis.summary"
  fi

  local cluster_cutoff_complement
  cluster_cutoff_complement=$(float_subtract "1" "$CLUSTER_CUTOFF")

  {
    echo -e "Run ID\t${RUN_ID}"
    if [[ "$RUN_MODE" != "local" ]]; then
      # TODO: Consider if/how this should be filled when specifying samples using --sample
      echo -e "Project(s)\t$(implode ", " "${PROJECTS[@]-}")"
    fi
    echo -e "Target lineage(s)\t${TARGET//-/, }"
    if [[ "$MOTHUR_MODE" == "align" ]]; then
      echo -e "Excluded lineages\t${REM_LIN//-/, }"
    fi
    echo -e "Reference database\t${DB_NAME_VER}"
    echo -e "Reference alignment\t${REF_ALN##*/}"
    echo -e "Reference taxonomy\t${TAX_REF##*/}"
    echo -e "Mock community\t${MOCK_REF_NAME:-No mock provided}"
    echo -e "Mock reference file\t$( if [[ -f "${MOCK_REF:-}" ]]; then basename "${MOCK_REF}"; else echo "NA"; fi )"
    echo -e "OTU clustering cutoff\t${cluster_cutoff_complement}"
    echo -e "Minimum sequences\t${MINSUBSAMPLE} sequences/sample"
    echo -e "Subsampling level\t${SUBSAMPLE} sequences/sample"
    echo -e "Number of samples\t${NUM_SAMPLES} (${ALL_SAMPLES} incl. controls)"
    echo -e "Number of samples below threshold\t${BAD_COUNT}"
    echo -e "'Bad' sample(s) ID(s)\t${BAD_LIST}"
    echo -e "Forward primer\t${FW} (5'-${FW_SEQ}-3')"
    echo -e "Reverse primer\t${RV} (5'-${RV_SEQ}-3')"
    echo -e "Positive control(s)\t${POSCTL}"
    echo -e "Negative control(s)\t${NEGCTL}"
    echo -e "Tree generated\t${TREE_GENERATED}"
  } >>"${MY_DIR}/postprocessing/analysis.summary"

  local OTUREPORTER_VERSION=$(otureporter_version)
  declare -p OTUREPORTER_VERSION >>"${MY_DIR}/postprocessing/vars.txt"
}

function otureporter_clean_up() {
  # Other stages:
  local BUILD_TREE

  # Load variables from other stages
  local vars_decl
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" BUILD_TREE)
  eval "$vars_decl"

  update_status_progress 12 "Cleaning up"
  cd "${MY_DIR}/report/files/mothur"

  ## Some cleaning...
  ## Final housekeeping:
  local i
  for i in rep_seqs bad_subsample subsample full; do
    tar cvzf "mothur_${i}.tar.gz" "${i}" && rm -rf "${MY_DIR}/report/files/mothur/${i}"
  done

  if [[ "${BUILD_TREE}" == "y" ]]; then
    cd "${MY_DIR}/report/files"
    tar cvzf FastTree.tar.gz FastTree && rm -r "${MY_DIR}/report/files/FastTree"
  fi
}

function otureporter_knit_it() {
  # We won't load anything here, so as to mimic reknitting as close as possible

  local -i knit_rc=-1
  local failure_type

  {
    knit_it
    knit_rc=$?
  } || true

  if [[ "$knit_rc" -ne 0 ]]; then
    case "$knit_rc" in
    1) # PDF
      failure_type="PDF report"
      ;;
    2) # HTML
      failure_type="HTML report"
      ;;
    3) # PDF + HTML
      failure_type="PDF and HTML reports"
      ;;
    esac

    warn "Generation of the $failure_type failed!"
    info "To regenerate the reports, run:"
    info "$(dirname "$0")/otureporter.sh --reknit --output $(printf "%q" "$MY_DIR")"
  fi
}

function otureporter_perform_self_test() {
  # Whether we created the output directory
  local own_output=""

  info "Starting self test"

  if [[ -z "${MY_DIR:-}" ]]; then
    MY_DIR=$(mktemp -du)
    # older versions of Mothur do not handle periods in the folder structure well
    # esp. rename.file
    MY_DIR=${MY_DIR//./_}
    RUN_ID="$(basename "$MY_DIR")"
    own_output="y"
  fi
  if [[ -z ${TEMP_DIR:-} ]]; then
    TEMP_DIR=${MY_DIR}
  fi

  if [[ ! -e "$OTUREPORTER_DIR/tests/libs/bats/bin/bats" ]]; then
    warn "tests/libs/bats/bin/bats appears to be missing."
    warn "Did you run 'git submodule update --init'?"
    exit 1
  fi

  ensure_all_dependencies

  info "  Test output: $MY_DIR"
  info "  Temp output: $TEMP_DIR"

  if [[ ! -e "$REF_DIR/db/test_data" ]]; then
    info "Downloading test data.."
    download_db_download_by_names "test_data.tar.bz2"
  fi

  # Internally specify paramaters
  local DOWNLOAD_MISSING_DB="y"
  local PRIMERS="v4test"
  local RUN_MODE="local"
  local MOCK_REF_ID="zymo"
  if [[ "${NO_CONTROLS}" != "y" ]]; then
    local POS_CONTROLS=("$REF_DIR/db/test_data/Zymo-DNA-control_S52_L001_R1_001.fastq.gz")
    local NEG_CONTROLS=("$REF_DIR/db/test_data/No-Template-Control_S69_L001_R1_001.fastq.gz")
  else
    # self_test ends up using the 'controls' as normal samples
    local POS_CONTROLS=()
    local NEG_CONTROLS=()
  fi
  local PROJECTS=("$REF_DIR/db/test_data")
  if command -v FastTree >/dev/null; then
    local BUILD_TREE="y"
  fi

  otureporter_run_stages 0

  info "Quickly checking output looks sane.."
  MY_DIR="$MY_DIR" "$OTUREPORTER_DIR/tests/libs/bats/bin/bats" -p "$OTUREPORTER_DIR/tests/selftest.bats" || true

  local REPORT_PDF="$MY_DIR/report/${RUN_ID}_analysis_report.pdf"
  local REPORT_HTML="$MY_DIR/report/${RUN_ID}_analysis_report.html"

  if [[ ! -e "$REPORT_PDF" ]] || [[ ! -e "$REPORT_HTML" ]]; then
    if [[ ! -e "$REPORT_PDF" ]] && [[ ! -e "$REPORT_HTML" ]]; then
      warn "The reports failed to generate. See knitr output above for details."
    elif [[ ! -e "$REPORT_HTML" ]]; then
      warn "The HTML report failed to generate. See knitr output above for details."
      warn "One cause of this is issues with cairo, esp. older versions when running headless."
    elif [[ ! -e "$REPORT_PDF" ]]; then
      warn "The HTML report is present, but the PDF report is missing."
      warn "This is usually indicative that something is wrong with the latex installation."
    fi

    info "Reports can be (re)generated after the analysis has completed."
  else
    info "Reports were successfully generated."

    if [[ "$own_output" == "y" ]] && [[ "$KEEP_INTERMEDIARY" != "y" ]]; then
      info "Removing selftest output.. (You may specify --hoarder_mode to keep this)"
      rm -rf "$MY_DIR"
    fi
  fi

  if [[ -e "$MY_DIR" ]]; then
    info "Selftest output: $MY_DIR"
  fi
}

function complete_stage() {
  local -i stage_num=$1
  local parent_dir tar_rc

  echo "declare -- COMPLETED_STAGE=\"$stage_num\"" >>"${MY_DIR}/postprocessing/vars.txt"

  if [[ "${FLAG_SAVE_ALL_STAGES}" != "n" ]]; then
    parent_dir=$(dirname "$MY_DIR")
    tar_rc=$(GZIP_OPT=-1 GZIP=-1 check_exit tar --warning=no-file-changed -cvzf "$parent_dir/${RUN_ID}_stage_$stage_num.tgz" -C "$parent_dir" "$RUN_ID")

    # We go out of our way to ignore the effects of 'file changed as we read it',
    # which is due to the log file being updated concurrently.
    if [[ "$tar_rc" -ne 0 ]] && [[ "$tar_rc" -ne 1 ]]; then
      exit "$tar_rc"
    fi
  fi
}

function otureporter_resume() {
  # Load variables from stage 0 (otureporter_run_init)
  # REF_ALN REF_DIR TAX_REF
  vars_decl=$(load_vars "${MY_DIR}/postprocessing/vars.txt" COMPLETED_STAGE)
  eval "$vars_decl"
  starting_stage=${START_FROM_OVERRIDE:-$((COMPLETED_STAGE + 1))}

  # A new COMPLETED_STAGE is appended to vars.txt following the completion of each stage.
  # So we can simply use this to attempt to resume processing.
  # Notably the stage index is different than the stage progress listed in the logs,
  # the stage index is then index of the stage in the stages array in otureporter_run_stages()

  if [[ ! -d "$TEMP_DIR" ]]; then
    error "\$TEMP_DIR doesn't exist! Did you specify one with --tmp previously?\n\$TEMP_DIR=$TEMP_DIR"
    exit 1
  fi

  info "Resuming from stage $starting_stage"
}

function otureporter_run_stages() {
  local starting_stage=$1

  local clean_vars check_vars
  local stages=(
    otureporter_run_init
    otureporter_prepare_samples
    otureporter_prepare_mothur_scripts
    otureporter_prepare_batch
    otureporter_get_good_seqs
    otureporter_get_error
    otureporter_get_shared_otus
    otureporter_get_oturep
    otureporter_subsample_sequences
    otureporter_prepare_report_data
    otureporter_summarize_bad_samples
    otureporter_copy_report_files
    otureporter_parse_error_rates
    otureporter_generate_tree
    otureporter_create_krona_outputs
    otureporter_clean_up
    otureporter_generate_report
    otureporter_knit_it
  )

  if [[ "${FLAG_DIFF_STAGES}" != "n" ]]; then
    clean_vars=$(mktemp)
    check_vars=$(mktemp)
    function dump_vars() {
      local exclude_vars
      exclude_vars=$(implode "|" BASH_LINENO BASH_SOURCE FUNCNAME stage_num PWD OLDPWD stage_func)

      (
        set -o posix
        set | grep -Ev "^($exclude_vars)="
      )
    }
    dump_vars >"$clean_vars"

    dump clean_vars check_vars
  fi

  for ((stage_num = starting_stage; stage_num < ${#stages[@]}; stage_num++)); do
    stage_func=${stages[$stage_num]}

    # Launch each stage in a subshell so we can better notice accidental changes
    # to the environment.
    (
      local start_time finish_time run_time

      start_time=$(date +%s.%N)
      eval "$stage_func"
      finish_time=$(date +%s.%N)
      run_time=$(python3 -c "print('{:.2f}'.format($finish_time - $start_time))")

      debug "Stage $stage_num completed in $run_time seconds."

      if [[ "${FLAG_DIFF_STAGES}" != "n" ]]; then
        dump_vars >"$check_vars"
        diff "$clean_vars" "$check_vars" || true
      fi
    )

    complete_stage "$stage_num"

    if [[ "$stage_num" -eq 0 ]]; then
      logfile_move "$MY_DIR/$RUN_ID"
    fi

    if [[ -n "$RUN_STAGE_OVERRIDE" ]]; then
      # Only run a single stage
      break
    fi
  done
}

function otureporter_main() {
  local starting_stage

  logfile_init "otureporter"
  # OTUREPORTER_LAUNCH_ARGS contains a single space when there are no arguments
  if [[ "$OTUREPORTER_LAUNCH_ARGS" != " " ]]; then
    info "OTUreporter launched with:$OTUREPORTER_LAUNCH_ARGS"
  fi

  # Parse arguments with getopt into various global variables
  parse_arguments "$@"

  if [[ -n "$RUN_STAGE_OVERRIDE" ]] && { [[ -n "$START_FROM_OVERRIDE" ]] || [[ "$FLAG_RESUME" == "y" ]]; }; then
    error "--start_from and --run_stage/--resume options are mutually exclusive!"
    exit 1
  fi

  ## Reference files. No need to change unless there is a new Silva release
  SCRIPTS=${REF_DIR}/scripts     #Path to accessory scripts
  MOTHUR=${REF_DIR}/mothur_batch #path to mothur batch files
  MOCK_REFDIR=${REF_DIR}/mock

  # Setup the scroll region
  NUM_PROGRESS_STEPS=15

  if [[ "${FLAG_PERFORM_SELF_TEST}" == "y" ]]; then
    otureporter_perform_self_test
    exit 0
  fi

  # --output is the only required option with --reknit
  ensure_param "MY_DIR" "-o / --output"
  ensure_param "RUN_ID" "-o / --output"

  ## just recreate the report
  if [[ "${FLAG_REKNIT}" == "y" ]]; then
    if [[ ! -d "$MY_DIR/postprocessing" ]]; then
      error "This doesn't look like the output directory of a run!"
      error "$(printf "%q" "$MY_DIR/postprocessing") doesn't exist."
      exit 1
    fi

    cd "${MY_DIR}"
    otureporter_knit_it
    exit 0
  fi

  ensure_all_dependencies

  ##If no --tmp is specified in command line, use MY_DIR
  if [[ -z ${TEMP_DIR-} ]]; then
    TEMP_DIR=${MY_DIR}
  fi

  starting_stage=${START_FROM_OVERRIDE:-${RUN_STAGE_OVERRIDE:-0}}
  if [[ "${FLAG_RESUME}" == "y" ]]; then
    if [[ ! -d "$MY_DIR/postprocessing" ]]; then
      error "This doesn't look like the output directory of a run!"
      error "$(printf "%q" "$MY_DIR/postprocessing") doesn't exist."
      exit 1
    fi

    otureporter_resume

    # Usually moved after otureporter_run_init
    logfile_move "$MY_DIR/$RUN_ID"
  fi

  otureporter_run_stages "$starting_stage"
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  if [[ "$#" -eq 0 ]]; then
    # Avoid passing no arguments
    otureporter_main --help
  else
    otureporter_main "$@"
  fi
fi
