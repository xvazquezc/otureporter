#!/usr/bin/env bash

set -o errexit

TESTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
cd "$TESTS_DIR"

TESTS=(
    "$TESTS_DIR/test.bats"
    "$TESTS_DIR/test_local.bats"
)

if command -v bashcov >/dev/null; then
    bashcov --root "$TESTS_DIR/.." --skip-uncovered "$TESTS_DIR/libs/bats/bin/bats" "${TESTS[@]}"
else
    echo "bashcov not found. No coverage report will be created!"
    "$TESTS_DIR/libs/bats/bin/bats" "${TESTS[@]}"
fi
