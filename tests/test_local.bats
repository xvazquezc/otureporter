#!/usr/bin/env bats

load 'utils'

source "$OTUREPORTER_DIR/scripts/common.sh"
source "$OTUREPORTER_DIR/scripts/local.sh"

set -o errexit
set +o nounset
set +o pipefail

LOG_LEVEL=LEVEL_DEBUG

function setup_mock_samples() {
  mkdir "$BATS_TMPDIR/shared_in_folders"
  mkdir "$BATS_TMPDIR/shared_in_folders/samples_a"
  touch "$BATS_TMPDIR/shared_in_folders/samples_a/samples_a_"{1,2}_R1_001.fastq.gz
  mkdir "$BATS_TMPDIR/shared_in_folders/samples_b"
  touch "$BATS_TMPDIR/shared_in_folders/samples_b/samples_b_"{1,2}_R1_001.fastq.gz
  mkdir "$BATS_TMPDIR/shared_in_folders/controls"
  touch "$BATS_TMPDIR/shared_in_folders/controls/controls_"{pos,neg}_R1_001.fastq.gz

  mkdir "$BATS_TMPDIR/shared_same_folder"
  touch "$BATS_TMPDIR/shared_same_folder/samples_a_"{1,2}_R1_001.fastq.gz
  touch "$BATS_TMPDIR/shared_same_folder/samples_b_"{1,2}_R1_001.fastq.gz
  touch "$BATS_TMPDIR/shared_same_folder/controls_"{pos,neg}_R1_001.fastq.gz
}

function declare_local_vars() {
  declare -gA LOCAL_SAMPLE_NAMES=()        # A mapping from sample_id to its "id name"
  declare -gA LOCAL_SAMPLE_PATHS=()        # A mapping from sample_id to its path

  declare -ga LOCAL_ALL_SAMPLES=()         # List of sample_ids for all samples to be used (excluding controls)
  declare -ga LOCAL_POS_CONTROL_SAMPLES=() # List of sample_ids for all positive controls
  declare -ga LOCAL_NEG_CONTROL_SAMPLES=() # List of sample_ids for all positive controls
}

function load_samples_save() {
  local control_folder=${1:-}

  local_load_samples_from_args "PROJECTS" "$control_folder" "POS_CONTROLS" "NEG_CONTROLS"
  local_save_globals
}

function assert_found_samples_and_controls() {
  assert_success

  assert_array_element "LOCAL_ALL_SAMPLES" "samples_a_1_R1_001.fastq.gz_.*"
  assert_array_element "LOCAL_ALL_SAMPLES" "samples_a_2_R1_001.fastq.gz_.*"
  assert_array_element "LOCAL_ALL_SAMPLES" "samples_b_2_R1_001.fastq.gz_.*"
  assert_array_element "LOCAL_ALL_SAMPLES" "samples_b_1_R1_001.fastq.gz_.*"

  assert_array_element "LOCAL_POS_CONTROL_SAMPLES" "controls_pos_R1_001.fastq.gz_.*"
  assert_array_element "LOCAL_NEG_CONTROL_SAMPLES" "controls_neg_R1_001.fastq.gz_.*"
}

@test "[separate folders] test local finds controls with abs paths" {
  setup_mock_samples
  
  declare_local_vars

  local PROJECTS=("$BATS_TMPDIR/shared_in_folders")
  local POS_CONTROLS=("$BATS_TMPDIR/shared_in_folders/controls/controls_pos_R1_001.fastq.gz")
  local NEG_CONTROLS=("$BATS_TMPDIR/shared_in_folders/controls/controls_neg_R1_001.fastq.gz")

  run load_samples_save
  assert_found_samples_and_controls
}

@test "[separate folders] test local finds controls with relative paths" {
  setup_mock_samples
  
  declare_local_vars

  local PROJECTS=("$BATS_TMPDIR/shared_in_folders")
  local POS_CONTROLS=("controls/controls_pos_R1_001.fastq.gz")
  local NEG_CONTROLS=("controls/controls_neg_R1_001.fastq.gz")

  run load_samples_save
  assert_found_samples_and_controls
}

@test "[separate folders] test local finds controls with control project" {
  setup_mock_samples
  
  declare_local_vars

  local PROJECTS=("$BATS_TMPDIR/shared_in_folders")
  local PROJECT_CTL=$BATS_TMPDIR/shared_in_folders/controls
  local POS_CONTROLS=("controls_pos_R1_001.fastq.gz")
  local NEG_CONTROLS=("controls_neg_R1_001.fastq.gz")

  run load_samples_save "$PROJECT_CTL"
  assert_found_samples_and_controls
}

@test "[same folder] test local finds controls with abs paths" {
  setup_mock_samples
  
  declare_local_vars

  local PROJECTS=("$BATS_TMPDIR/shared_same_folder")
  local POS_CONTROLS=("$BATS_TMPDIR/shared_same_folder/controls_pos_R1_001.fastq.gz")
  local NEG_CONTROLS=("$BATS_TMPDIR/shared_same_folder/controls_neg_R1_001.fastq.gz")

  run load_samples_save
  assert_found_samples_and_controls
}

@test "[same folder] test local finds controls with relative paths" {
  setup_mock_samples
  
  declare_local_vars

  local PROJECTS=("$BATS_TMPDIR/shared_same_folder")
  local POS_CONTROLS=("controls_pos_R1_001.fastq.gz")
  local NEG_CONTROLS=("controls_neg_R1_001.fastq.gz")

  run load_samples_save
  assert_found_samples_and_controls
}
