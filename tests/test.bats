#!/usr/bin/env bats

load 'utils'

source "$OTUREPORTER_DIR/scripts/common.sh"

set -o errexit
set +o nounset
set +o pipefail

LOG_LEVEL=LEVEL_DEBUG

@test "test dump" {
  local array=(a b "c d e" f) bar=foo x y=
  
  run dump array bar x y
  assert_success
  assert_line "array[0]=a"
  assert_line "array[1]=b"
  assert_line "array[2]=c d e"
  assert_line "array[3]=f"
  assert_line "bar[0]=foo"
  assert_line "x is empty!"
  assert_line "y is empty!"
  assert_line "bar[0]=foo"
}

@test "test array_index_of" {
  local array=(a b "c d e" f)
  
  run array_index_of "array" "a"
  assert_success
  assert_output "0"

  run array_index_of "array" "c"
  assert_failure
  assert_output ""
  
  run array_index_of "array" "c d e"
  assert_success
  assert_output "2"

  run array_index_of "array" "f"
  assert_success
  assert_output "3"
}

@test "test array_contains" {
  local array=(a b "c d e" f)
  
  run array_contains "array" "a"
  assert_success

  run array_contains "array" "c"
  assert_failure
  
  run array_contains "array" "c d e"
  assert_success

  run array_contains "array" "f"
  assert_success
}

@test "test prefix_each" {
  # Empty prefix, single empty string as item
  run prefix_each "" ""
  assert_success
  assert_output ""

  # Empty prefix, no items
  run prefix_each ""
  assert_success
  assert_output ""

  # Prefix is mandatory
  run prefix_each
  assert_failure

  # Complex prefix, items containing spaces
  run prefix_each "-%#$)(" "item1" "i tem tw o" "item three"
  assert_success
  assert_output "-%#$)(item1-%#$)(i tem tw o-%#$)(item three"
}

@test "check_exit echos function exit codes" {
  function returns_0() { return 0; }
  function returns_1() { return 1; }
  function returns_2() { return 2; }

  [[ "$(check_exit returns_0)" -eq "0" ]]
  [[ "$(check_exit returns_1)" -eq "1" ]]
  [[ "$(check_exit returns_2)" -eq "2" ]]
  [[ "$(check_exit getopt --test)" -eq "4" ]]
}

@test "call_py_script returns zero on success" {
  run call_py_script -c "a = 1"
  assert_success
}

@test "call_py_script returns non-zero on exception" {
  run call_py_script -c "raise ValueError('test')"
  assert_failure
}

@test "eval_vars_from_func works with empty variables" {
  function foo() {
      echo "foo"
  }
  function driver() {
    local var1 var2
    eval_vars_from_func var1 var2 -- \
      foo
  }

  export -f foo driver
  run driver
  assert_success
}

# Simple check for info loaded from db info
@test "find_load_primer_db loads db info" {
  # Mock value from primer info
  local MOTHUR_MODE="align"

  run track_vars find_load_primer_db "v4" "v4" "silva"
  assert_success

  assert_var DB_NAME "SILVA"
  assert_var REGION "v4"
  assert_var REF_ALN ".*v4\.align"
  assert_var TAX_REF ".*v4\.tax"
}

# Simple check that db info can be loaded from a specific path
@test "find_load_primer_db can load from specific path" {
  # Mock value from primer info
  local MOTHUR_MODE="align"

  local TMP_DB="$BATS_TMPDIR/test.db.info"
  cp "$REF_DIR/db/v4.db.info" "$TMP_DB"
  echo "PRIMER_SEQUENCE_BOUNDS[v4]=\"9123123856:97235168234\"" >>"$TMP_DB"

  run track_vars find_load_primer_db "v4" "v4" "silva" "n" "$TMP_DB"
  assert_success

  assert_var DB_NAME "SILVA"
  assert_var REGION "v4"
  assert_var REF_ALN ".*v4\.align"
  assert_var TAX_REF ".*v4\.tax"
  assert_var REF_SEQ_START "9123123856"
  assert_var REF_SEQ_END "97235168234"
  assert_var DB_INFO_FILE "$TMP_DB"
}

# Simple check for info loaded from primer file
@test "get_primer_info loads primer info" {
  run track_vars get_primer_info "v4" "" "" "quiet"
  assert_success

  assert_var "FW_SEQ" "GTGYCAGCMGCCGCGGTAA"
}

# Check that primer info files can be loaded by path
@test "get_primer_info can load from specific path" {
  local TMP_PRIMER="$BATS_TMPDIR/test.primer.info"
  cp "$REF_DIR/primers/v4.info" "$TMP_PRIMER"
  echo "FW_SEQ=\"Bar\"" >>"$TMP_PRIMER"

  run track_vars get_primer_info "v4" "" "" "quiet" "$TMP_PRIMER"
  assert_success

  assert_var FW_SEQ "Bar"
  assert_var PRIMER_INFO_FILE "$TMP_PRIMER"
}

# Test both primer and db info can be specified by path
@test "get_primer_info can load both primer and db from specific path" {
  local TMP_PRIMER="$BATS_TMPDIR/test.primer.info"
  cp "$REF_DIR/primers/v4.info" "$TMP_PRIMER"
  echo "FW_SEQ=\"Bar\"" >>"$TMP_PRIMER"

  local TMP_DB="$BATS_TMPDIR/test.db.info"
  cp "$REF_DIR/db/v4.db.info" "$TMP_DB"
  echo "PRIMER_SEQUENCE_BOUNDS[v4]=\"9123123856:97235168234\"" >>"$TMP_DB"

  run track_vars get_primer_info "v4" "" "" "quiet" "$TMP_PRIMER" "$TMP_DB"
  assert_success

  assert_var FW_SEQ "Bar"
  assert_var PRIMER_INFO_FILE "$TMP_PRIMER"

  assert_var REF_SEQ_START "9123123856"
  assert_var REF_SEQ_END "97235168234"
  assert_var DB_INFO_FILE "$TMP_DB"
}

# Check the implode utility function works as expected
@test "implode behaves as expected" {
  # Empty array
  assert_equal \
    "" \
    "$(implode "-mark-")"

  # Single item array
  assert_equal \
    "item one" \
    "$(implode "-mark-" "item one")"

  # Two item array
  assert_equal \
    "item one-mark-it em t w o" \
    "$(implode "-mark-" "item one" "it em t w o")"

  # Multi character delimiter
  assert_equal \
    "foo bar-mark-what-mark-1-mark-12, 3-mark- 4  4 " \
    "$(implode "-mark-" "foo bar" "what" "1" "12, 3" " 4  4 ")"

  # Empty delimiter
  assert_equal \
    "foo barwhat112, 3 4  4 " \
    "$(implode "" "foo bar" "what" "1" "12, 3" " 4  4 ")"
}
