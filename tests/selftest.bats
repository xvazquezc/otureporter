#!/usr/bin/env bats

load 'utils'

# echo "$BATS_TEST_DIRNAME"
source "$OTUREPORTER_DIR/scripts/common.sh"

# errexit breaks tests
set -o errexit
set +o nounset
set +o pipefail

POST_PROC="$MY_DIR/postprocessing"

function setup() {
  [[ -n "${MY_DIR:-}" ]] || fail "MY_DIR not provided"
  [[ -e "${MY_DIR:-}" ]] || fail "Path in MY_DIR doesn't exist"
  [[ -e "${POST_PROC:-}" ]] || fail "MY_DIR missing post_processing"
}

function teardown() {
  # Do nothing, overriding teardown in utils.bash

  return 0
}

function is_using_controls() {
  local vars_decl NO_CONTROLS
  eval "$(load_vars "${MY_DIR}/postprocessing/vars.txt" NO_CONTROLS)"

  if [[ "$NO_CONTROLS" == "y" ]]; then
    return 1
  else
    return 0
  fi
}

@test "stats_full.tsv has expected line count" {
  local vars_decl MOTHUR_VERSION
  eval "$(load_vars "${MY_DIR}/postprocessing/vars.txt" MOTHUR_VERSION)"

  if version_greater_equal "1.43.0" "$MOTHUR_VERSION"; then
    if is_using_controls; then
      assert_line_count "$POST_PROC/stats_full.tsv" "5"
    else
      assert_line_count "$POST_PROC/stats_full.tsv" "6"
    fi
  else
    if is_using_controls; then
      assert_line_count "$POST_PROC/stats_full.tsv" "3"
    else
      assert_line_count "$POST_PROC/stats_full.tsv" "4"
    fi
  fi
}

@test "stats_full.tsv contains Sample1 and Sample2" {
  run cat "$POST_PROC/stats_full.tsv"
  assert_line --partial "Sample1"
  assert_line --partial "Sample2"
}

@test "stats_full.tsv Sample1 nseqs looks OK" {
  sample1_nseqs=$(line_field_number "$POST_PROC/stats_full.tsv" "Sample1" "3")
  [[ sample1_nseqs -gt 19400 ]]
  [[ sample1_nseqs -lt 19500 ]]
}

@test "stats_full.tsv Sample2 nseqs looks OK" {
  sample2_nseqs=$(line_field_number "$POST_PROC/stats_full.tsv" "Sample2" "3")
  [[ sample2_nseqs -gt 19400 ]]
  [[ sample2_nseqs -lt 21700 ]]
}

@test "stats_bad.tsv has 3 lines" {
  assert_line_count "$POST_PROC/stats_bad.tsv" "3"
}

@test "stats_bad.tsv contains NTC and Sample3" {
  run cat "$POST_PROC/stats_bad.tsv"
  assert_line --partial "No_Template_Control"
  assert_line --partial "Sample3"
}

@test "stats_good_subsample.tsv has expected line count" {
  if is_using_controls; then
    assert_line_count "$POST_PROC/stats_good_subsample.tsv" "5"
  else
    assert_line_count "$POST_PROC/stats_good_subsample.tsv" "7"
  fi
}

@test "stats_good_subsample.tsv contains Sample1 and Sample2" {
  run cat "$POST_PROC/stats_good_subsample.tsv"
  assert_line --partial "Sample1"
  assert_line --partial "Sample2"
}

@test "analysis.summary looks alright" {
  [[ -e "$POST_PROC/analysis.summary" ]]
  
  run cat "$POST_PROC/analysis.summary"
  if is_using_controls; then
    assert_line --regexp "Number of samples\s+3 \(5 incl. controls\)"
  else
    # self_test ends up using the 'controls' as normal samples
    assert_line --regexp "Number of samples\s+5 \(5 incl. controls\)"
  fi
  assert_line --regexp "Number of samples below threshold\s+2"
  assert_line --regexp "'Bad' sample\(s\) ID\(s\)\s+(.*)No-Template-Control"
  assert_line --regexp "'Bad' sample\(s\) ID\(s\)\s+(.*)Sample3"
}

@test "Tree was generated" {
  if ! command -v FastTree 2>/dev/null; then
    skip "FastTree command not present."
  fi

  run tar -tf "$MY_DIR/report/files/FastTree.tar.gz"
  assert_line --partial otureporter_FastTree.log
  assert_line --partial otureporter_FastTree.tree
}
