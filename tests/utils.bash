declare TEST_TMP_DIR BATS_TEST_SKIPPED= e=

load 'libs/bats-support/load'
load 'libs/bats-assert/load'

if [[ -z "${OTUREPORTER_DIR:-}" ]]; then
  OTUREPORTER_DIR="${BATS_TEST_DIRNAME}/.."
fi
REF_DIR=${REF_DIR:-$OTUREPORTER_DIR}

function setup() {
  set -o errexit
  set -o nounset
  set -o pipefail

  BATS_TMPDIR=$(mktemp -d --tmpdir="$BATS_TMPDIR" "bats_test.$BATS_TEST_NUMBER.XXXXXXXXXX")
  BATS_OUR_OWN_TMPDIR="$BATS_TMPDIR"
}

function teardown() {
  if [[ -n "${BATS_OUR_OWN_TMPDIR:-}" ]]; then
    rm -rf "$BATS_OUR_OWN_TMPDIR"
  fi
  
  set -o errexit
  set +o nounset
  set +o pipefail
}

function line_count() {
  wc -l "$1" | cut -d ' ' -f 1
}
function assert_line_count() {
  assert_equal "$(line_count "$1")" "$2"
}
function line_field_number() {
  grep "$2" "$1" | cut -f "$3" | cut -f 1 -d '.'
}
function track_vars() {
  local changed_vars=$(
    diff --changed-group-format='%>' --unchanged-group-format='' \
      <(declare -p) \
      <(eval_declare "$@")
  )
  eval_select_vars "__RETURN_CODE" <<<"$changed_vars"

  echo "$changed_vars"

  return ${__RETURN_CODE:-1}
}

function assert_var() {
  local name=$1
  local expected=${2:-}

  if [[ -n "$expected" ]]; then
    assert_output --regexp "declare -. $name=\"?$expected\"?"
  else
    assert_output --regexp "declare -. $name\b"
  fi
}

function assert_array_element() {
  local name=$1
  local expected=${2:-}

  if [[ -n "$expected" ]]; then
    assert_output --regexp "declare -. $name=\(.*\[[[:digit:]]\]=\"$expected\".*\)"
  else
    assert_output --regexp "declare -. $name\b"
  fi
}