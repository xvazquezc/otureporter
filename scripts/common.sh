#!/usr/bin/env bash

SCRIPTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
# shellcheck source=./common.sh
source "$SCRIPTS_DIR/colors.sh"
# shellcheck source=./mothur_color.sh
source "$SCRIPTS_DIR/mothur_color.sh"
# shellcheck source=./download_db.sh
source "$SCRIPTS_DIR/download_db.sh"

set -o errexit
set -o nounset
set -o pipefail

CURRENT_VER="$BASH_VERSION"
REQUIRED_VER="4.0.0"
if [[ "$(printf '%s\n' "$REQUIRED_VER" "$CURRENT_VER" | sort -V | head -n1)" != "$REQUIRED_VER" ]]; then
  echo "[ERROR] Bash version $REQUIRED_VER or greater is required!" >&2
  BASH_PATH=$(readlink -f "/proc/$$/exe")
  echo "You are running $CURRENT_VER from $BASH_PATH" >&2
  exit 1
fi

# Default prefix for Mothur files
export MOTHUR_PREFIX="otureporter"

export LEVEL_ERROR=0
export LEVEL_WARN=1
export LEVEL_INFO=2
export LEVEL_DEBUG=3
export LEVEL_TRACE=4

# The current logging level (default: INFO)
export LOG_LEVEL=$LEVEL_INFO

# Path to the reference folder containing accessory scripts, databases and
# templates (default: same as $(cmd) location).
declare REF_DIR="$OTUREPORTER_DIR"

# Path to the reference folder containing mock databases.
declare MOCK_REFDIR=${REF_DIR}/mock

# ID name of the mock reference to load
declare MOCK_REF_ID

# Whether to download an appropriate database if one couldn't be found in the
# database directory.
declare DOWNLOAD_MISSING_DB="n"

export DEPENDENCIES=(python3 mothur tar biom vsearch parallel ktImportKrona awk)
export OPTIONAL_DEPENDENCIES=(FastTree jq xelatex xdvipdfmx R pandoc)

declare COLOR_OUTPUT="y"

# Path to the current logfile, used when the logfile is moved while running.
declare CURRENT_LOGFILE=""

# Disable color output when TERM is undefined/dumb (PBS)
# or when tput is unavailable.
if [[ "${TERM:-dumb}" == "dumb" ]] || ! command -v tput >/dev/null || [ ! -t 1 ] || [ ! -t 2 ]; then
  COLOR_OUTPUT="n"

  color_disable
fi

# Internal: If column is available, invokes it on stdin, otherwise echos.
function column_format() {
  if command -v column >/dev/null; then
    column -t -s ";"
  else
    debug "Utility 'column' not found!"
    cat
  fi
}

# Internal: Safely returns the exit code of the given command.
#
# Temporarily disables errexit (if necessary) before exciting "$@".
#
# $@ - The command (and it's parameters) to execute
#
# Examples
#
#   if [[ $(check_exit getopt --test) != 4 ]]; then echo "Failed"; else echo "OK"; fi
#
# Returns the exit code of the given command via stdout.
function check_exit() {
  "$@" 1>&2 && echo 0 || echo $?
}

function check_getopt() {
  if [[ $(check_exit getopt --test) != 4 ]]; then
    echo "I’m sorry, 'getopt --test' failed in this environment."
    exit 1
  fi
}

function cmd() {
  basename "$0"
}

# Internal: Save a set of variables into an associative array.
#
# $1     - Name of the *existing* associative array to store the variables in.
# ${@:2} - (varargs) Variable names to save.
function save_vars_to_assoc() {
  local dest_name=$1
  local vars=("${@:2}")

  local var
  for var in "${vars[@]}"; do
    eval "${dest_name}[\$var]=\"${!var}\""
  done
}

# Internal: Load a set of variables from an associative array.
#
# $1     - Name of the associative array to retrieve the variables from.
# ${@:2} - (varargs) Variable names to load.
function load_vars_from_assoc() {
  local array_name=$1
  local vars=("${@:2}")

  local var
  for var in "${vars[@]}"; do
    eval "$var=\${${array_name}[\$var]}"
  done
}

# Inteneral: Knit the final reports
#
# Notably, unlike most other high-level functions, this function will litter
# variables in the global scope.  TODO
#
# Returns
#  0 - Success
#  1 - PDF report failed to generate
#  2 - HTML report failed to generate
#  3 - Both PDF and HTML report failed to generate
function knit_it() {
  # Store key variables we want to keep
  declare -A original_values=()
  save_vars_to_assoc "original_values" "MY_DIR" "REF_DIR"

  # Forcibly use the one in vars.txt, and notice if there was none.
  unset "MOCK_REF_ID"

  cd "${MY_DIR}"
  source "${MY_DIR}/postprocessing/vars.txt"

  # Restore key variables saved above
  unset "MY_DIR" "REF_DIR"
  load_vars_from_assoc "original_values" "MY_DIR" "REF_DIR"
  unset original_values

  if [[ -f "${MY_DIR}/postprocessing/mock_community.info" ]]; then
    get_mock_info "${MY_DIR}/postprocessing/mock_community.info"
  else
    warn "No mock_community.info found in postprocessing directory!"

    if [[ -n "${MOCK_REF_ID-}" ]]; then
      get_mock_info "$MOCK_REF_ID"
    else
      warn "And no MOCK_REF_ID was found in vars, YMMV. Good luck!"
    fi
  fi

  if [[ -f "${MY_DIR}/postprocessing/primers.info" ]] && [[ -n "${PRIMERS:-}" ]]; then
    if [[ -f "${MY_DIR}/postprocessing/db_info.info" ]] && [[ -n "${PRIMERS:-}" ]]; then
      get_primer_info "${PRIMERS}" "$REM_LIN_OVERRIDE" "$TARGET_OVERRIDE" --quiet "${MY_DIR}/postprocessing/primers.info" "${MY_DIR}/postprocessing/db_info.info"
    else
      warn "No db_info.info found in postprocessing directory!"
      get_primer_info "${PRIMERS}" "$REM_LIN_OVERRIDE" "$TARGET_OVERRIDE" --quiet "${MY_DIR}/postprocessing/primers.info"
    fi
  else
    warn "No primers.info found in postprocessing directory!"

    if [[ -n "${PRIMERS:-}" ]]; then
      get_primer_info "$PRIMERS" "$REM_LIN_OVERRIDE" "$TARGET_OVERRIDE" --quiet
    else
      warn "And no PRIMERS was found in vars, YMMV. Good luck!"
    fi
  fi

  local primer_info_rmd=${MY_DIR}/postprocessing/primers.Rmd
  if [[ ! -f "$primer_info_rmd" ]]; then
    warn "No primers.Rmd found in postprocessing directory!"

    primer_info_rmd=${REF_DIR}/primers/${PRIMERS}.Rmd

    if [[ ! -f "$primer_info_rmd" ]]; then
      warn "And the fallback value of '$primer_info_rmd' doesn't appear to exist, YMMV. Good luck!"
    fi
  fi

  local template=${REF_DIR}/template_files #Path to templates, e.g. report template

  local cluster_cutoff_complement
  cluster_cutoff_complement=$(float_subtract "1" "$CLUSTER_CUTOFF")

  local step=13 i
  local -i global_exit_code=0
  for i in pdf html; do
    ((step += 1))
    update_status_progress "$step" "Generating $i report"

    if [[ -z "${REM_LIN-}" ]]; then
      local REM_LIN=""
    fi

    local bool_no_controls
    if [[ "${NO_CONTROLS}" == "y" ]]; then
      bool_no_controls="TRUE"
    else
      bool_no_controls="FALSE"
    fi

    local RMD_LIB_QC_FILE RMD_CLEAN_UP_FILE
    RMD_LIB_QC_FILE=$(smart_resolve_path "$RMD_LIB_QC" "$MY_DIR/postprocessing" "${REF_DIR}/primers")
    RMD_CLEAN_UP_FILE=$(smart_resolve_path "$RMD_CLEAN_UP" "$MY_DIR/postprocessing" "${REF_DIR}/primers")

    if ! R -e "thetitle = 'Analysis: ${RUN_ID}'; \
      run_id = '${RUN_ID}'; \
      primers = '${PRIMERS}'; \
      target = '${TARGET//-/, }'; \
      subsample = '${SUBSAMPLE}'; \
      db_description_text = '${DB_DESCRIPTION_TEXT}'; \
      miseq_kit = '${MISEQ_KIT}'; \
      miseq_format = '${MISEQ_FORM}'; \
      read_len = '${READ_LEN}'; \
      frag = '${FRAG}'; \
      frag_sd = '${FRAG_SD}'; \
      maxlen = '${MAXLEN}'; \
      minlen = '${MINLEN}'; \
      remlin = '${REM_LIN//-/, }'; \
      subsample = '${SUBSAMPLE}'; \
      no_controls = ${bool_no_controls}; \
      min_subsample = '${MINSUBSAMPLE}'; \
      num_samples = '${ALL_SAMPLES}'; \
      mock_ref = '${MOCK_REF:-No mock provided}'; \
      mothur_mode = '${MOTHUR_MODE}'; \
      cluster_cutoff = ${cluster_cutoff_complement}; \
      mock_ref_name = '${MOCK_REF_NAME:-**Unknown**}'; \
      seq_error_text = '${MOCK_REF_DESC:-**NO SEQ ERROR TEXT**}'; \
      fw_primer = '${FW}'; fw_seq = '${FW_SEQ}'; \
      rv_primer = '${RV}'; rv_seq = '${RV_SEQ}'; \
      reftax = '${TAX_REF}'; refaln = '${REF_ALN}'; \
      mothur_version = '${MOTHUR_VERSION:-**Unknown**}'; \
      sample_source = '${RUN_MODE}'; \
      bad = '${BAD}'; \
      tree = '${TREE_GENERATED}'; \
      fasttree_version = '${FASTTREE_VERSION:-**Unknown**}'; \
      otureporter_version = '${OTUREPORTER_VERSION:-Undefined}'; \
      rmarkdown::render('${template}/template.Rmd', output_dir='${MY_DIR}/report/', knit_root_dir='${MY_DIR}', output_format='${i}_document', params=list(primer_info='${primer_info_rmd}', clean_up='$RMD_CLEAN_UP_FILE', lib_qc='$RMD_LIB_QC_FILE'))"; then
      case "$i" in
      pdf)
        global_exit_code+=1
        ;;
      html)
        global_exit_code+=2
        ;;
      esac
    fi

    mv "${MY_DIR}/report/template.${i}" "${MY_DIR}/report/${RUN_ID}_analysis_report.${i}"
  done

  return "$global_exit_code"
}

# Internal: Escape the arguments passed to be shell safe.
function quote() {
  printf "%q" "$@"
}

# Internal: Attempt to resolve the given file by itself, or in one of the folders.
function smart_resolve_path() {
  local filename=$1

  # If the filename exists as a file path, use that directly
  if [[ -e "$filename" ]]; then
    echo "$filename"
    return 0
  fi

  shift 1

  for part in "$@"; do
    if [[ -n "$part" ]] && [[ -e "$part/$filename" ]]; then
      echo "$part/$filename"
      return 0
    fi
  done

  warn "Failed to find $(quote "$filename") either by itself, or under one of:"
  warn "    $(implode ", " "$@")"
  echo "${*: -1}/$filename"
}

# Internal: Extract the penultimate file extension
#
# Will extract the second to last field delimited by periods.
# Stripping any leading folder parts.
#
# $1 - The filename, possibly a full file path.
function second_to_last_extension() {
  local path=$1
  local filename=$(basename "$path")
  local without_last=${filename%.*}

  echo "${without_last##*.}"
}

# Internal: Check the given variable is initialized.
#
# Ensures the variable named in $1 is initialized and not empty,
# outputting $2 as the suggested option(s) to define it and
# $3 (if present) as an extra error message.
#
# $1 - The name of the variable to check.
# $2 - The cli options to suggest to define this variable.
# $3 - (Optional) Extra error message to display.
#
# Examples
#
#   ensure_param "RUN_ID" "-o / --output"
#
#   ensure_param "PROJECT_CTL" "-c / --control" "A control project ID is required for BaseSpace-based runs."
function ensure_param() {
  if [[ -z ${!1-} ]]; then
    error "The '$1' ($2) for $(cmd) has not been provided."
    if [[ -n ${3-} ]]; then
      error "$3"
    fi
    error "Check your input command or run \"--help\" for more information."
    exit 1
  fi
}

# Internal: Check's for the presence of program dependencies.
#
# Also outputs dependency location and version when debug logging is enabled.
function ensure_all_dependencies() {
  local include_optional=${1:-}

  info "Checking dependencies"

  local missing_count=0 i
  for i in "${DEPENDENCIES[@]}"; do
    if hash "$i" >/dev/null 2>&1; then
      local dep_path=$(command -v "$i" 2>/dev/null)
      debug "Found: $i at $dep_path"

      # Dump version numbers where we can
      show_utility_version "$i" "       "
    else
      error "Failed to find required dependency: $i"
      ((missing_count += 1))
    fi
  done

  local failed=0
  if ! echo f | awk '{ print gensub(/f/, "", "g", "");}'; then
    error "'awk' gensub failed, are you missing gawk?"
    ((failed += 1))
  fi

  if [[ -n "$include_optional" ]]; then
    local opt_missing_count=0 i
    for i in "${OPTIONAL_DEPENDENCIES[@]}"; do
      if hash "$i" >/dev/null 2>&1; then
        local dep_path=$(command -v "$i" 2>/dev/null)
        debug "Found: $i at $dep_path"

        # Dump version numbers where we can
        show_utility_version "$i" "       "
      else
        warn "Failed to find optional dependency: $i"
        ((opt_missing_count += 1))
      fi
    done

    if [[ "$opt_missing_count" -gt 0 ]]; then
      warn "$opt_missing_count optional dependencies are missing or absent from \$PATH.\n\nPATH=$PATH\n" >&2
      ((failed += 1))
    fi
  fi

  if [[ "$missing_count" -gt 0 ]]; then
    error "$missing_count dependencies are missing or absent from \$PATH.\nPlease check the error(s) above and install the missing requirements.\n\nPATH=$PATH\n\nExiting..." >&2
    ((failed += 1))
  fi

  if [[ "$failed" -gt 0 ]]; then
    exit 1
  else
    info "All required dependencies installed. Continuing..."
  fi
}

# Internal: Attempt to determine the version of the given utility.
#
# $1 - Name of the utility
function find_version() {
  local name=$1

  case "$name" in
  "mothur")
    mothur --version | sed 's/\x1B\[3J\x1B\[H\x1B\[2J//' | tr -d '\33'
    ;;
  "vsearch")
    vsearch --version 2>&1 | head -n1
    ;;
  "ktImportKrona")
    TERM= ktImportKrona 2>/dev/null | head -n2 | tail -n1 | sed -e 's/^[/\\_ ]*\(.*\) [/\\_ ]*$/\1/g'
    ;;
  "FastTree")
    FastTree -expert 2>&1 | head -n1 | cut -d ' ' -f 5- | tr -d ':'
    ;;
  *)
    "$name" --version | head -n1
    ;;
  esac
}

# Internal: Attempt to output version number of the given utility as debug
#           messages.
#
# $1 - Name of the utility
# $2 - (Optional) Text to prefix the version text with
function show_utility_version() {
  local name=$1
  local prefix=${2:-}

  local ver_line
  while read -r ver_line; do
    if [[ -z "$ver_line" ]]; then
      continue
    fi

    debug "$prefix$ver_line"
  done < <(find_version "$name")
}

# Internal: Loads a map between sample_id and display name from the given file.
#
# Will store map in ${dest_name}_DISPLAY_NAME, to allow later additions to
# load the remaining data.
#
# $1 - The name prefix to use for the output
# $2 - The file path to LIMSID_to_sampleID.tsv
function load_sample_map() {
  local dest_name=$1
  local file_name=$2

  local sample_id spec_name count display_name
  while read -r sample_id spec_name count display_name; do
    display_name=${display_name//[$'\t\r\n']/}
    eval "${dest_name}_DISPLAY_NAME["\$sample_id"]=\$display_name"
  done <"$file_name"
}

# Internal: Replace occurrences in the target array with values from the given
#           associative array.
#
# Entries in the target array will be replaced with the value associated to
# the original value in the associative array, iff it's present.
#
# $1 - Name of the associative array to use
# $2 - Name of the target array to map values to/from
function map_sample_display_names() {
  local sample_display_name_map=$1
  local target_array=$2

  local length item idx
  eval "length=\${#${target_array}[@]}"
  for ((idx = 0; idx < length; idx++)); do
    eval "item=\${${target_array}[\$idx]}"

    if assoc_array_contains "${sample_display_name_map}" "$item"; then
      local item_display_name=$(assoc_array_get "${sample_display_name_map}" "$item")
      eval "${target_array}[\$idx]=\$item_display_name"
    fi
  done
}

# Internal: Handle execution of and output from Mothur.
#
# Executes Mothur with the given parameters ("$@"). Additionally,
#  * Strips escape characters, as Mothur explicitly calls 'clear' on startup
#  * Skips the first 30 lines of output as just clutters stdout
#  * Colorizes certain fragments of Mothur output
#  * Logs the commands used to invoke Mothur
#
# $@ - The parameters to execute Mothur with.
#
# Examples
#
#   wrap_mothur 01_get_good_seqs.batch
function wrap_mothur() {
  # TODO Consider checking Mothur output for 'ERROR' while it's running

  info "Running 'mothur $*'"
  mothur_color "$@" || {
    error "Something went wrong. Exiting..." >&2
    exit 1
  }
}

function common_log() {
  local msg_level=$1
  local args=("${@:2}")

  local col_error="$BIRed"
  local col_warn="$BIYel"
  local col_info="$BIPur"
  local col_debug="$BICya"
  local col_unknown="$BIBlu"

  local col_var="col_$msg_level"
  local color="${!col_var-:$col_unknown}"
  local color_reset="$RCol"
  local color_arrow="$Gre"

  if [[ "${COLOR_OUTPUT:-y}" != y ]]; then
    color=""
    color_reset=""
    color_arrow=""
  fi

  echo -e "${color_arrow}$(date -u +"%Y-%m-%d %H:%M:%S UTC") ${color}$(printf "[%5s]" "${msg_level^^}")${color_reset} ${args[*]}${color_reset}" 1>&2
}

function debug() {
  [[ $LOG_LEVEL -ge $LEVEL_DEBUG ]] && common_log "debug" "$@" || true
}

function info() {
  [[ $LOG_LEVEL -ge $LEVEL_INFO ]] && common_log "info" "$@" || true
}

function warn() {
  [[ $LOG_LEVEL -ge $LEVEL_WARN ]] && common_log "warn" "$@" || true
}

function error() {
  [[ $LOG_LEVEL -ge $LEVEL_ERROR ]] && common_log "error" "$@" || true
}

# Internal: Updates the status text.
#
# Uses the global "$NUM_PROGRESS_STEPS" # as the total number of steps
#
# $1 - The current step number to show in the status text.
# $2 - The text to display in the status text.
#
# Examples
#
#   update_status_progress 3 "Calculating things"
function update_status_progress() {
  local current_step=$1

  # temporarily disable tracing while we move the cursor around
  local current_flags=$-
  set +x

  local bold progress_color normal cols status_text
  if [[ "${COLOR_OUTPUT:-y}" == "y" ]]; then
    bold=$FmtBold
    progress_color="${RCol}${Whi}${On_Blu}"
    normal="${RCol}${Cya}${On_Bla}"
    cols=$(tput cols)
  else
    bold=""
    progress_color=""
    normal=""
    cols=120
  fi

  status_text=$(printf "${progress_color}${bold}[ %2d / %2d ]${normal} %.$(($cols - 12))s" "$current_step" "$NUM_PROGRESS_STEPS" "$2")
  info "$status_text"

  set -$current_flags
}

# Internal: Debug function to output contents of one or more variables by name.
#
# $@ - Variables names to print the content of
function dump() {
  local __dump_var __dump_i __dump_varArray __dump_array

  for __dump_var in "$@"; do
    __dump_varArray="${__dump_var}[@]"
    if [[ -z ${!__dump_var-} ]]; then
      printf "%s is empty!\n" "$__dump_var"
      continue
    fi

    __dump_array=("${!__dump_varArray}")
    for __dump_i in "${!__dump_array[@]}"; do
      printf "%s[%i]=%s\n" "$__dump_var" "$__dump_i" "${__dump_array[$__dump_i]}" 1>&2
    done
  done
}

# Internal: Retrieve the value associated to key in the given associative array.
#
# $1 - Name of the associative array
# $2 - The key to access
function assoc_array_get() {
  local var_name="$1"
  local key=$2

  local var_as_array="$1[@]"

  if [[ -z "${!var_as_array-}" ]]; then
    # Array specified by var_name is empty/unset
    return 1
  fi

  eval "echo \${${var_name}[\$key]-}"
}

# Internal: Check if a given key is in the given associative array.
#
# $1 - Name of the associative array
# $2 - The key to check
function assoc_array_contains() {
  local var_name="$1"
  local key=$2

  local var_as_array="$1[@]"

  if [[ -z "${!var_as_array-}" ]]; then
    # Array specified by var_name is empty/unset
    return 1
  fi

  local value=$(assoc_array_get "$var_name" "$key")
  if [[ -n "${value-}" ]]; then
    return 0
  fi
  return 1
}

# Internal: List primers in the primers reference folder.
function list_primers() {
  local i

  echo -en "${FmtBold}"
  {
    echo -e "Name; Taxon group; Region; Primer pair; (Non-)Targets${RCol}"
    echo -e "----; -----------; ------; -----------; -------------"
    for i in "$REF_DIR/primers/"*.info; do
      declare Info
      source "$i"

      if [[ -n "${Info-}" ]]; then
        echo -e "${Info}"
        #Reset info variable after grabbing it from the reference files
        unset Info
      else
        debug "Primer '$i' has no 'Info' variable!"
      fi
    done
  } | column_format
}

# Internal: List entries in the mock reference folder
function list_mocks() {
  echo -en "${FmtBold}"
  (
    local i MOCK_REF MOCK_REF_NAME MOCK_REF_DESC

    echo -e "ID;Reference file;Name;Description${RCol}"
    echo -e "--;--------------;----;-----------"

    for i in "$MOCK_REFDIR/"*.info; do
      source "$i"

      if ! ensure_all_variables "Mock template $i is missing: %s" MOCK_REF MOCK_REF_NAME MOCK_REF_DESC; then
        continue
      fi

      local info_name=$(basename "${i::-5}")

      echo -e "$info_name;$MOCK_REF;$MOCK_REF_NAME;$MOCK_REF_DESC"

      unset MOCK_REF MOCK_REF_NAME MOCK_REF_DESC
    done
  ) | column_format
}

declare MOCK_REF MOCK_REF_NAME MOCK_REF_DESC
# Internal: Load the mock template with the given ID name.
#
# Alternatively, if $1 is a file, load that directly.
#
# $1 - ID name of the mock template to load.
function get_mock_info() {
  local mock=$1

  local mock_info_file
  if [[ -f "$mock" ]]; then
    mock_info_file=$mock
  else
    mock_info_file=${MOCK_REFDIR}/${mock}.info
  fi

  if [[ -f "$mock_info_file" ]]; then
    source "$mock_info_file"

    ensure_all_variables "Mock template $mock is missing: %s" MOCK_REF MOCK_REF_NAME MOCK_REF_DESC
  else
    error "Unknown mock file: $mock. Please run \"$(cmd) --list_mock\" for supported options." >&2
    exit 1
  fi

  # Allow the mock reference file to be next to the info file, in the event
  # that $mock is a path to a file.
  local mock_ref_local="$(readlink -qm "${mock_info_file}")/${MOCK_REF}"
  if [[ -f "${mock_ref_local}" ]]; then
    MOCK_REF=${mock_ref_local}
  else
    MOCK_REF=$(readlink -qm "${MOCK_REFDIR}/${MOCK_REF}")
  fi

  info "${MOCK_REF_NAME} mock selected:
  \tMock file: ${MOCK_REF}
  \tMock description: ${MOCK_REF_DESC}"
}

# Internal: Ensure each provided variable is initialised.
#
# $1     - Format string to use when a variable is not initialised.
# ${@:2} - Variable names to check
function ensure_all_variables() {
  local message_template=$1
  local vars=("${@:2}")

  local var

  for var in "${vars[@]}"; do
    if [[ -z ${!var-} ]]; then
      error "$(printf "$message_template" "$var")"
      exit 1
    fi
  done
}

declare PRIMER_INFO_FILE PRIMER_RMD_FILE TARGET FW FW_SEQ RV RV_SEQ READ_LEN MISEQ_KIT MISEQ_FORM FRAG DB_REGION DB_NAME FRAG_SD MINLEN MAXLEN DIFFS MOTHUR_MODE REM_LIN RMD_LIB_QC RMD_CLEAN_UP
# Internal: Load primer information for the given primer.
#
# Will also load a database as appropriate for this primer.
#
# $1 - Name or file path of the primer to load.
# $2 - Override for $REM_LIN - for MOTHUR_MODe=="align"
# $3 - Override for $TARGET - for MOTHUR_MODE=="no_align"
# $4 - (Optional) Whether to silence output of this function.
# $5 - (Optional) Path to a primer file to use instead of one within the
#      primers directory.
# $6 - (Optional) Path to a db info file to use instead of looking one up.
function get_primer_info() {
  local primer=$1
  local rem_lin=${2:-}
  local target_override=${3:-}
  local quiet=${4:-n}
  local primer_file=${5:-}
  local specific_db_file=${6:-} # TODO

  # If $primer is a file, load that as the info file. Otherwise, look for
  # $primer in the primers folder of the reference directory.
  local myprimers
  if [[ -f "$primer_file" ]]; then
    myprimers=$primer_file
  else
    myprimers=${REF_DIR}/primers/${primer}.info
  fi

  if [[ -f "$myprimers" ]]; then
    source "$myprimers"
  else
    error "Unknown primer set: $primer. Please run \"$(cmd) --list\" for supported options." >&2
    exit 1
  fi

  if [[ -f "${myprimers%.info}.Rmd" ]]; then
    PRIMER_RMD_FILE="${myprimers%.info}.Rmd"
  else
    PRIMER_RMD_FILE=${REF_DIR}/primers/${primer}.Rmd
  fi

  PRIMER_INFO_FILE="$myprimers"

  # Attempt to find and load a database for this primer
  if ! find_load_primer_db "$primer" "$DB_REGION" "$DB_NAME" "$quiet" "$specific_db_file"; then
    if [[ -n "$specific_db_file" ]]; then
      error "User speicified db info file not found: $specific_db_file"
      exit 1
    fi

    if [[ "${DOWNLOAD_MISSING_DB}" == "y" ]]; then
      if [[ "$quiet" == "n" ]]; then
        info "Will attempt to download a $DB_NAME database for the $DB_REGION region."
      fi

      # Try to download a database for this primer from the repository
      if ! download_db_for_primer "$primer" "$DB_REGION" "$DB_NAME"; then
        error "Failed to find a database for primer $primer"
        exit 1
      fi

      # Retry loading the database now that we've downloaded one
      if ! find_load_primer_db "$primer" "$DB_REGION" "$DB_NAME" "$quiet"; then
        # This shouldn't happen
        error "Failed to find a database for primer $primer"
        exit 1
      fi
    else
      error "Failed to find a database for primer $primer"
      exit 1
    fi
  fi

  ensure_all_variables "Primer $primer is missing %s" TARGET FW FW_SEQ RV RV_SEQ READ_LEN MISEQ_KIT MISEQ_FORM FRAG MOTHUR_MODE RMD_LIB_QC RMD_CLEAN_UP

  if [[ -z "${MINLEN:-}" ]]; then
    MINLEN=100
  fi
  ## Generate values for fragment sd (needed by FLASH), as well as the min and max fragment length (for mothur, 10% of the fragment length)
  FRAG_SD=$((FRAG / 10))
  if [[ -z "${MAXLEN:-}" ]]; then
    MAXLEN=$((FRAG + FRAG_SD))
  fi
  DIFFS=$((FRAG / 100))

  if [[ "$MOTHUR_MODE" == "align" ]]; then
    ## Generate list of lineages to be removed based on the target
    if [[ -n "$rem_lin" ]]; then
      info "Automatic setting of '\$REM_LIN' parameter overridden."
      REM_LIN=$rem_lin
    else
      case $TARGET in
      Bacteria)
        REM_LIN="Archaea-Chloroplast-Eukaryota-Mitochondria-unknown"
        ;;
      Archaea)
        REM_LIN="Bacteria-Chloroplast-Eukaryota-Mitochondria-unknown"
        ;;
      Universal)
        REM_LIN="Chloroplast-Eukaryota-Mitochondria-unknown"
        ;;
      Eukaryota)
        REM_LIN="Archaea-Bacteria-Chloroplast-Mitochondria-unknown"
        ;;
      *)
        error "TARGET=${TARGET} is not a valid parameter" >&2
        exit 1
        ;;
      esac
    fi
  elif [[ "$MOTHUR_MODE" == "align" ]]; then
    unset REM_LIN

    if [[ -n "$target_override" ]]; then
      TARGET=$target_override
    fi
  fi

  if [[ "$quiet" == "n" ]]; then
    info "${primer} primer set selected:
  \tTARGET: ${TARGET}
  \tForward primer: ${FW}; (5'-${FW_SEQ}-3')
  \tReverse primer: ${RV}; (5'-${RV_SEQ}-3')
  \tRead length: ${READ_LEN}; MiSeq ${MISEQ_KIT}, ${MISEQ_FORM}
  \tEstimated fragment size=${FRAG}"
  fi
}

# Internal: Check if the named array contains the given item
#
# $1 - Name of the array to check
# $2 - Needles ot search for in the array
function array_contains() {
  local var_name="$1[@]"
  local needle=$2

  if [[ -z "${!var_name-}" ]]; then
    # Array specified by var_name is empty/unset
    return 1
  fi

  local element
  for element in "${!var_name-}"; do
    if [[ "$element" == "$needle" ]]; then
      return 0
    fi
  done
  return 1
}

declare DB_INFO_FILE REF_SEQ_START REF_SEQ_END REF_ALN TAX_REF DB_NAME DB_VER DB_NAME_VER REGION
# Internal: Find and load a database for the given primer.
#
# $1 - Target primer to find a database for
# $2 - The target region of the database
# $3 - The name of the database family
# $4 - (Optional) Whether to silence output of this function.
# $5 - (Optional) Path to a db info file to use instead of looking for one
#      within the directory.
function find_load_primer_db() {
  local target_primer=$1
  local db_region=$2
  local db_name=$3
  local quiet=${4:-n}
  local db_info_file=${5:-}

  local db_info db_primers iter_db_name db_ver
  local db_dir="$REF_DIR/db"

  function attempt_load() {
    local db_info=$1
    local vocal_errors=${2:-n}

    local db_info_direcory
    db_info_direcory=$(dirname "$db_info")

    # Check the database name
    iter_db_name=$(
      declare -A PRIMER_SEQUENCE_BOUNDS=()
      source "$db_info"
      printf "%s\n" "$DB_NAME"
    )
    if [[ "${iter_db_name,,}" != "${db_name,,}" ]]; then
      if [[ "$quiet" == "n" ]]; then
        debug "Skipping $db_info as $iter_db_name != $db_name"
      fi
      return 1
    fi

    declare -A PRIMER_SEQUENCE_BOUNDS=()
    if [[ "$MOTHUR_MODE" == "align" ]]; then
      # Check the database has sequence bounds for our selected primer

      readarray -t db_primers <<<"$(
        declare -A PRIMER_SEQUENCE_BOUNDS=()
        source "$db_info"
        IFS="|" printf "%s\n" "${!PRIMER_SEQUENCE_BOUNDS[@]}"
      )"

      if ! array_contains db_primers "$target_primer"; then
        if [[ "$quiet" == "n" ]]; then
          local message="Database $db_info of family $db_name didn't contain PRIMER_SEQUENCE_BOUNDS for primer $target_primer"
          if [[ "$vocal_errors" == "y" ]]; then
            error "$message"
          else
            debug "$message"
          fi
        fi
        return 2
      fi

      source "$db_info"

      # Extract the sequence bounds for $target_primer
      IFS=":" read -r REF_SEQ_START REF_SEQ_END <<<"${PRIMER_SEQUENCE_BOUNDS[$target_primer]}"
      ensure_all_variables "Database $db_info missing %s" REF_SEQ_START REF_SEQ_END
    else
      source "$db_info"
    fi

    DB_INFO_FILE="$db_info"

    # If the reference and alignment files exist next to the info file, use those.
    if [[ -f "$db_info_direcory/$TAX_REF" ]] && [[ -f "$db_info_direcory/$REF_ALN" ]]; then
      TAX_REF="$db_info_direcory/$TAX_REF"
      REF_ALN="$db_info_direcory/$REF_ALN"
    else
      if { [[ -f "$db_info_direcory/$TAX_REF" ]] || [[ -f "$db_info_direcory/$REF_ALN" ]]; } &&
        { [[ ! -f "$db_info_direcory/$TAX_REF" ]] || [[ ! -f "$db_info_direcory/$REF_ALN" ]]; }; then
        warn "$TAX_REF and $REF_ALN were not both found in $db_info_direcory!"
      fi

      TAX_REF="$db_dir/$TAX_REF"
      REF_ALN="$db_dir/$REF_ALN"
    fi

    ensure_all_variables "Database $db_info missing %s" TAX_REF REF_ALN DB_NAME_VER DB_DESCRIPTION_TEXT

    if [[ "$quiet" == "n" ]]; then
      info "Using database: $db_info for primer $target_primer"
    fi
    return 0
  }

  if [[ -n "$db_info_file" ]] && [[ -f "$db_info_file" ]]; then
    if attempt_load "$db_info_file" "y"; then
      return 0
    else
      return 3
    fi
  fi

  # Iterate over all the database info files in decreasing order of version
  while read -r db_ver db_info; do
    if [[ "$db_info" == "" ]]; then
      continue
    fi

    if [[ "$quiet" == "n" ]]; then
      debug "Checking database: $db_ver $db_info"
    fi

    if attempt_load "$db_info"; then
      return 0
    fi
  done \
    <<<"$(
      for db_info in "$db_dir/"*.info; do
        if [[ ! -f "$db_info" ]]; then
          # Skip non-files and the case when there are no info files
          continue
        fi

        declare -A PRIMER_SEQUENCE_BOUNDS=()
        source "$db_info"
        printf "%q %q\n" "$DB_VER" "$db_info"
      done | sort -nr
    )"

  return 4
}

# Internal: Join array contents with the given delimiter.
#
# Delimiter can be a multi-character string
#
# $1     - The delimiter to use.
# ${@:2} - The elements to join with the delimiter.
function implode() {
  local delimiter=$1

  if [[ $# -lt 2 ]]; then
    return 0
  fi
  echo -n "${2-}"
  if [[ $# -lt 3 ]]; then
    return 0
  fi

  # Easier handling of single element arrays
  shift 2
  printf "%s" "${@/#/$delimiter}"
}

# Internal: Prefix each item in the array with the given string.
#
# $1     - The prefix to use.
# ${@:2} - The items to attach the prefix to.
function prefix_each() {
  local prefix=$1
  if [[ "${2:-}" ]]; then
    echo -n "$prefix"
  else
    return 0
  fi
  echo -n "${2-}"
  if [[ $# -lt 3 ]]; then
    return 0
  fi

  # Easier handling of single element arrays
  shift 2
  printf "%s" "${@/#/$prefix}"
}

# Internal: Execute python3 with the given arguments, and parse it's output.
#
# Expects program output lines to conform to "$type $a $b $rest.."
# Where type indicates the kind of output it is, and $a, $b, and $rest
# are arguments/supporting information.
function call_py_script() {
  local initialized type a b rest

  initialized=0
  while read -r type a b rest; do
    if [[ "$type" == "init" ]]; then
      initialized=1
    elif [[ "$initialized" -eq 0 ]]; then
      error "$type $a $b $rest"
      continue
    elif [[ "$type" == "log" ]]; then
      common_log "$a" "$b $rest"
    elif [[ "$type" == "array_init" ]]; then
      eval "${a}=()"
    elif [[ "$type" == "array_add" ]]; then
      eval "${a}+=("\$rest")"
    elif [[ "$type" == "assoc_init" ]]; then
      eval "${a}=()"
    elif [[ "$type" == "assoc_set" ]]; then
      eval "${a}[\$b]=\$rest"
    elif [[ "$type" == "set" ]]; then
      eval "${a}=\$rest"
    elif [[ "$type" == "return" ]]; then
      debug "Script returned $a"
      return "$a"
    else
      error "What is type=$type a=$a b=$b rest=$rest"
    fi
  done < <(
    # Suppress errexit as we want to explicitly capture the return code
    set +e

    cd "$SCRIPTS_DIR/.."
    debug "Running python3 $@"
    python3 "$@"
    ret_val=$?
    echo -e "init\nreturn $ret_val"

    return 0
  )
}

# Internal: Extract vars by name from the given vars file
#
# Examples
#
#   eval "$(load_vars "/path/to/vars" PREBAD)"
function load_vars() {
  local vars_file=$1
  local var_names=("${@:2}")

  local vars_selector line var declarations
  local -a names=()

  # Load the declarations from the vars file.
  vars_selector=$(implode "|" "${var_names[@]}")
  declarations=$(grep -E "declare -[aA-] ($vars_selector)\b" "$vars_file")

  # Extract just the variable names from the declarations.
  while read -r line; do
    names+=("$(echo "$line" | cut -d ' ' -f 3 | cut -d = -f 1)")
  done <<<"$declarations"

  # Ensure each variable in var_names is present at least once.
  for var in "${var_names[@]}"; do
    if ! array_contains names "$var"; then
      error "$vars_file didn't contain variable $var!"
      exit 1
    fi
  done

  echo "$declarations"
}

# Internal: Find the index of the given element in the named array.
#
# $1 - Name of the array to search.
# $2 - Target element to find the index of.
function array_index_of() {
  local array_name="$1[@]"
  local needle=$2

  local -i index=0
  local element
  for element in "${!array_name-}"; do
    if [[ "$element" == "$needle" ]]; then
      echo "$index"
      return 0
    fi

    ((index += 1))
  done

  return 1
}

# Internal: Eval the arguments in a subshell, then dump all variables.
#
# Note: stdout of the arguments is redirected to stderr!
#
# $@ - Function/arguments to eval
function eval_declare() {
  (
    # NB. eval will eat quotes, so we have to pass them verbatim, this limits
    # us to only using actual functions/commands, instead of any expression.
    eval "$1" \"\$\{@:2\}\" 1>&2
    __RETURN_CODE=$?
    declare -p | grep -v " PS4=\""
    return $__RETURN_CODE
  )

  return $?
}

# Internal: Selectively set variables from stdin which match the arguments.
#
# Expects input of the same form as $(delcare -p)'s output.
#
# DOESN'T explicitly declare variables as global or local, and as such, will
# default to that of the outer scope.
#
# $@ - List of variable names to look for and set from stdin.
function eval_select_vars() {
  local vars_selector declarations

  vars_selector=$(implode "|" "${@}")
  # Strip declare prefixes, and ignore empty declarations
  declarations=$(grep -E "declare -[aA-] ($vars_selector)\b" | cut -d ' ' -f 3- | grep '=')

  debug "declarations=$declarations"

  eval "$declarations"
}

# Internal: Execute a function and extract only some of it's variable modifications.
#
# Note: stdout of the arguments is redirected to stderr!
#
# $@ - List of variables to keep, "--", function name and arguments to execute.
#
# Examples
#
#   var2="untouched"
#   eval_vars_from_func var1 var3 -- some_function_that_sets_some_vars
#   echo "var1=$var1 var3=$var3"  # OK
#   echo "var2=$var2 var4=$var4"  # "var2=untouched var4=" (var4 is unset)
function eval_vars_from_func() {
  local args=("$@")
  local vars_selector declarations
  local -i index rc

  # Split the input on '--'
  index=$(array_index_of args '--')
  local -a vars=("${args[@]:0:$index}")
  local -a func_args=("${args[@]:$index+1}")

  # Run the user function, taking a copy the variable state afterwards
  declarations=$(eval_declare "${func_args[@]}")
  # Keep the return code of the user function
  rc=$?

  # Selectively eval declarations which match the vars we've been asked for
  eval_select_vars "${vars[@]}" <<<"$declarations"

  # Return the return code of the user function
  return $rc
}

# Internal: Initialize logging of script output to file.
#
# Strips ANSI color escape sequences before appending stdout and stderr to the
# active log file.
#
# $1 - Path prefix for the log file, the current unix timestamp will be
#      appended to this, e.g. "prefix.1500000000.log"
function logfile_init() {
  local name_prefix=$1

  if [[ -n "$CURRENT_LOGFILE" ]]; then
    error "Logging is already initialized to '$CURRENT_LOGFILE'"
    return 1
  fi

  CURRENT_LOGFILE=$(readlink -qm "$name_prefix").$(date +%s).log

  # Ensure the logfile always exists, in case we move it before anything is logged
  touch "$CURRENT_LOGFILE"

  # Copy stdout and stderr to fd3 and fd4 respectively
  exec 3>&1 4>&2

  # Replace stdout and stderr such that strips ANSI color codes from output before logging to file
  exec 1> >(tee /dev/fd/3 | sed -u "s/\x1B\[\([0-9]\{1,2\}\(;[0-9]\{1,2\}\)\?\)\?[mGK]//g" >>"$CURRENT_LOGFILE")
  exec 2> >(tee /dev/fd/4 | sed -u "s/\x1B\[\([0-9]\{1,2\}\(;[0-9]\{1,2\}\)\?\)\?[mGK]//g" >>"$CURRENT_LOGFILE")
}

# Internal: Move the active log file to another location.
#
# Note: This results in a new unix timestamp in the log filename.
#
# $1 - Path prefix for the new log file, the current unix timestamp will be
#      appended to this, e.g. "prefix.1500000000.log"
function logfile_move() {
  local name_prefix=$1

  local new_destination old_logfile=$CURRENT_LOGFILE
  new_destination=$(readlink -qm "$name_prefix").$(date +%s).log

  debug "Moving current logfile to $new_destination"
  CURRENT_LOGFILE=$new_destination

  # Re-replace stdout and stderr as in logfile_init
  exec 1> >(tee /dev/fd/3 | sed -u "s/\x1B\[\([0-9]\{1,2\}\(;[0-9]\{1,2\}\)\?\)\?[mGK]//g" >>"$CURRENT_LOGFILE")
  exec 2> >(tee /dev/fd/4 | sed -u "s/\x1B\[\([0-9]\{1,2\}\(;[0-9]\{1,2\}\)\?\)\?[mGK]//g" >>"$CURRENT_LOGFILE")

  # mv "$old_logfile" "$new_destination"
  cat "$old_logfile" >>"$new_destination"
  rm "$old_logfile"
}

# Internal: Output the version of OTUreporter
#
# Extracts version information from the VERSION file in the installation
# directory.
function otureporter_version() {
  local VERSION_FILE="$OTUREPORTER_DIR/VERSION"
  local COMMIT_HASH=""

  if command -v "git" &>/dev/null && [[ -d "$OTUREPORTER_DIR/.git" ]]; then
    COMMIT_HASH=$(cd "$OTUREPORTER_DIR" && git rev-parse --short HEAD)

    debug "COMMIT_HASH: $COMMIT_HASH"
  fi

  if [[ ! -f "$VERSION_FILE" ]]; then
    warn "VERSION file not found?"
    if [[ -n "$COMMIT_HASH" ]]; then
      echo "Unknown ($COMMIT_HASH)"
    else
      echo "Unknown"
    fi
    return 0
  fi

  local VERSION_TAG="" VERSION_COMMIT="" ARCHIVE_COMMIT_TAG=""
  {
    read -r VERSION_TAG || true
    read -r VERSION_COMMIT || true
    read -r ARCHIVE_COMMIT_TAG || true
  } < <(cat "$VERSION_FILE")

  debug "VERSION_TAG: $VERSION_TAG"
  debug "VERSION_COMMIT: $VERSION_COMMIT"
  debug "ARCHIVE_COMMIT_TAG: ${ARCHIVE_COMMIT_TAG:-None}"

  if [[ -n "$ARCHIVE_COMMIT_TAG" ]]; then
    echo "$ARCHIVE_COMMIT_TAG"
  else
    printf "%s (%s)\n" "${VERSION_TAG:-Unknown}" "${COMMIT_HASH:-${VERSION_COMMIT:-Unknown}}"
  fi
}

# Internal: Subtract two floating point numbers.
#
# Python's decimal module is used to obtain more accurate results.
function float_subtract() {
  local a=$1
  local b=$2

  local regex_number='^[[:digit:]]+(\.[[:digit:]]+)?$'

  if [[ ! "$a" =~ $regex_number ]]; then
    error "That doesn't look like a number! a=$a"
    exit 1
  elif [[ ! "$b" =~ $regex_number ]]; then
    error "That doesn't look like a number! b=$b"
    exit 1
  fi

  # Normalize to remove extra precision
  python3 -c "from decimal import Decimal as D; print((D('$a') - D('$b')).normalize())"
}

# Internal: Check if second version string is greater or equal to the first.
function version_greater_equal() {
  local wanted=$1
  local version=$2

  if [[ "$(printf '%s\n' "$wanted" "$version" | sort -V | head -n1)" != "$wanted" ]]; then
    return 1
  else
    return 0
  fi
}
