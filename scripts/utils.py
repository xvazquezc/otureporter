import functools
import logging
import shlex
import subprocess
import sys
from inspect import isgenerator
from typing import Dict, Any, Union, List


def cache_it(key_fmt: str = '') -> callable:
    def decorator(func: callable) -> callable:
        cache = {}

        @functools.wraps(func)
        def cached_func(*args, **kwargs):
            key = key_fmt.format(*args, **kwargs)

            if key not in cache:
                # print('Cache miss: {} {}'.format(func.__name__, key))
                cache[key] = func(*args, **kwargs)

                if isgenerator(cache[key]):
                    cache[key] = tuple(cache[key])
            else:
                # print('Cache hit: {}'.format(key))
                pass

            return cache[key]

        return cached_func

    return decorator


def count_lines(path: str) -> int:
    with open(path, 'rb') as f:
        return sum(1 for _ in f)


def dump(data: Dict[str, Any]):
    import json
    p = subprocess.Popen(["jq", "-S"], stdin=subprocess.PIPE, stdout=sys.stdout)
    p.communicate(json.dumps(data).encode("utf-8"))
    p.wait()


def bash_quote(s: str, wrap: bool = True):
    quoted = shlex.quote(s)
    if wrap and not quoted.startswith("'"):
        return "'{}'".format(quoted)

    if not wrap and quoted.startswith("'"):
        return quoted[1:-1]

    return quoted


def script_resp(type: str, a: str = '_', b: str = '_', rest: str = ''):
    if '\n' in rest:
        raise ValueError("Illegal linefeed found in rest={!r}", format(rest))

    # sys.stdout.flush()

    print('{} {} {} {}'.format(type, a, b, rest))


def bash_declare(name: str, x: Union[List[str], Dict[str, Union[List[str], str]], str]):
    if type(x) is list:
        print('declare -a {}=({})'.format(
            name,
            ' '.join('[{}]={}'.format(idx, bash_quote(item)) for idx, item in enumerate(x))
        ))
    elif type(x) is dict:
        print('declare -A {}=({})'.format(
            name,
            ' '.join("[{}]='{}'".format(
                bash_quote(key),
                bash_quote(item, wrap=False) if type(item) is str else
                " ".join(bash_quote(y, wrap=False) for y in item) if type(item) is list and (
                        not len(item) or type(item[0]) is str) else
                None
            ) for key, item in x.items())
        ))
    else:
        print('declare -- {}={}'.format(name, bash_quote(str(x))))


def script_set(name: str, x: Union[List[str], Dict[str, Union[List[str], str]], str]):
    if type(x) is list:
        script_resp("array_init", name)
        for item in x:
            script_resp("array_add", name, rest=item)
    elif type(x) is dict:
        script_resp("assoc_init", name)
        for key, item in x.items():
            assert ' ' not in key
            if type(item) is list:
                assert ' ' not in '|'.join(item)
                items = ' '.join(item)
            elif type(item) is str:
                items = item
            else:
                raise ValueError('Unexpected item type {}'.format(type(item)))

            script_resp("assoc_set", name, key, rest=items)
    else:
        script_resp('set', name, rest=bash_quote(str(x), wrap=False))


def init_logging(level=logging.INFO):
    logging.addLevelName(logging.ERROR, 'error')
    logging.addLevelName(logging.WARN, 'warn')
    logging.addLevelName(logging.INFO, 'info')
    logging.addLevelName(logging.DEBUG, 'debug')

    logging.basicConfig(
        stream=sys.stdout,
        level=level,
        format='log %(levelname)s %(name)s %(funcName)s:%(lineno)d %(message)s',
    )
