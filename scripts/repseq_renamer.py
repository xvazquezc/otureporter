#!/usr/bin/env python3

import sys
import os

__author__ = 'Asa Perez-Bercoff'
__copyright__ = 'Asa Perez-Bercoff, The EdwardsLab, BABS, The University of New South Wales'
__credits__ = ['Asa Perez-Bercoff', 'Xabier Vazquez-Campos']
__license__ = 'GPL'
__version__ = '2.0'
__maintainer__ = 'Xabier Vazquez-Campos'
__email__ = 'x.vazquezcampos@unsw.edu.au'
__status__ = 'Development'

def parseFasta(argv):
    """
    Renaming the FASTA headers.
    The headers are very long, and as a unique identifier the OTU is enough.

    This is an example of what a header looks like in the FASTA input file:
    >M01153_365_000000000-BP443_1_1101_14609_1623	Otu00001|622223|KOV4703A1-KOV4703A10-KOV4703A11-KOV4703A12-KOV4703A13-KOV4703A14-KOV4703A15-KOV4703A16-KOV4703A17-KOV4703A18-KOV4703A19-KOV4703A2-KOV4703A20-KOV4703A21-KOV4703A22-KOV4703A23-KOV4703A24-KOV4703A25-KOV4703A26-KOV4703A27-KOV4703A28-KOV4703A29-KOV4703A3-KOV4703A30-KOV4703A31-KOV4703A32-KOV4703A33-KOV4703A34-KOV4703A35-KOV4703A36-KOV4703A37-KOV4703A38-KOV4703A39-KOV4703A4-KOV4703A40-KOV4703A41-KOV4703A42-KOV4703A43-KOV4703A44-KOV4703A45-KOV4703A46-KOV4703A47-KOV4703A48-KOV4703A49-KOV4703A5-KOV4703A50-KOV4703A51-KOV4703A52-KOV4703A53-KOV4703A54-KOV4703A55-KOV4703A56-KOV4703A57-KOV4703A58-KOV4703A59-KOV4703A6-KOV4703A60-KOV4703A61-KOV4703A62-KOV4703A63-KOV4703A64-KOV4703A65-KOV4703A66-KOV4703A67-KOV4703A68-KOV4703A69-KOV4703A7-KOV4703A70-KOV4703A71-KOV4703A72-KOV4703A8-KOV4703A9-KOV4703_NTC

    which should be renamed to:
    >Otu00001
    """
    filename = argv[0]
    try:
        # 'with open' will open the file, and close the file automatically once it's done
        with open(filename) as infile:
            for line in infile:
                # Remove any end-of-line characters
                line = line.rstrip('\r\n')
                if line.startswith('>'):
                    columns = line.split()
                    new_header = columns[1].split('|')[0]
                    print('>%s' % new_header)
                else:
                    print(line)
    except IOError:
        print('Cannot open %s' % os.path.abspath(filename))


def main(argv):
    parseFasta(argv)

if __name__=="__main__":
    try:
        if len(sys.argv) !=2:
            print('''
            Takes 1 argument (an input FASTA file)
            $ python rename_fasta_headers.py sequences.fa
            Exiting...
            ''')
            sys.exit()
        else:
            main(sys.argv[1:])
    except SystemExit:
        sys.exit()
