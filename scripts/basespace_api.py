import os
import urllib.parse
from configparser import ConfigParser
from typing import Dict, Any, Iterator, Optional

import requests

from scripts.utils import cache_it


class Errors(object):
    MISSING_DEFAULT = "MISSING_DEFAULT"


class BaseSpaceDatasetFile(object):
    href_content: str
    id: int
    name: str
    size: int

    def __init__(self, dataset_item: Dict[str, Any]):
        self.href_content = dataset_item.get("HrefContent")
        self.id = dataset_item.get("IdAsLong")
        self.name = dataset_item.get("Name")
        self.size = dataset_item.get("Size")


class BaseSpaceDataset(object):
    def __init__(self, dataset: Dict[str, Any]):
        self.id: str = dataset.get("Id")
        self.name: str = dataset.get("Name")

        project: Dict[str, Any] = dataset.get("Project")
        self.project_id: int = int(project.get("Id"))
        self.project_name: str = project.get("Name")


class BaseSpaceVipreProject(object):
    id: int
    name: str

    def __init__(self, vipre_project: Dict[str, Any]):
        self.id = int(vipre_project.get("Id"))
        self.name = vipre_project.get("Name")


class BaseSpaceApi(object):
    """
    Wrapper around the BaseSpace API, masquerading as the CLI.
    """

    USER_AGENT = "BaseSpaceGOSDK/0.6.0 BaseSpaceCLI/0.10.8"

    def __init__(self, access_token: str = None, endpoint: str = None):
        if access_token is None:
            basespace_cfg = os.path.expanduser("~/.basespace/default.cfg")
            if os.path.exists(basespace_cfg):
                with open(basespace_cfg, "r") as f:
                    conf = ConfigParser()
                    conf.read_string("[root]\n" + f.read())
                    access_token = conf.get("root", "accessToken", fallback=None)
                    endpoint = conf.get("root", "apiServer", fallback=None)

            if access_token is None:
                raise ValueError("BaseSpace not authenticated! (Run 'bs auth') - {}".format(
                    basespace_cfg
                ))
            elif endpoint is None:
                raise ValueError(
                    "BaseSpace config has no endpoint, what's going on? (Try re-authenticating with 'bs auth') - {}".format(
                        basespace_cfg
                    ))
        elif endpoint is None:
            raise ValueError("BaseSpace endpoint is required when specifying an access_token explicitly")

        self.access_token = access_token
        self.endpoint = endpoint
        self._headers = {
            "X-Access-Token": self.access_token,
            "User-Agent": self.USER_AGENT,
        }
        self.timeout = 10
        self.session = requests.Session()

    def get_dataset_files(self, dataset_id: str) -> Iterator[BaseSpaceDatasetFile]:
        """
        Obtain a list of all files in the given dataset.

        :param dataset_id: Id of the dataset
        :return: Generator yielding BaseSpaceDatasetFiles for each file in the
                 dataset
        """

        for item in self._get("/v2/datasets/{}/files".format(dataset_id))["Items"]:
            yield BaseSpaceDatasetFile(item)

    def get_datasets_by_project_id(self, project_id: int) -> Iterator[BaseSpaceDataset]:
        """
        Obtain a list of all datasets in the given project.

        :param project_id: Id of the project
        :return: Generator yielding BaseSpaceDataset for each dataset in the
                 project
        """

        for dataset in self._get("/v2/projects/{}/datasets?Limit=1024&Offset=0".format(project_id))["Items"]:
            yield BaseSpaceDataset(dataset)

    def get_datasets_by_project_name(self, project_name: str) -> Iterator[BaseSpaceDataset]:
        """
        Obtain a list of all datasets in the given project.

        :param project_name: Name of the project
        :return: Generator yielding BaseSpaceDataset for each dataset in the
                 project
        """

        project_id: int = self.get_project_by_name(project_name).id

        yield from self.get_datasets_by_project_id(project_id)

    def get_all_projects(self) -> Iterator[BaseSpaceVipreProject]:
        """
        Obtain a list of all projects the user has access to.

        :return: Generator yielding BaseSpaceVipreProject for each project
        """

        yield from self._get_projects()

    def get_project_by_name(self, project_name: str) -> Optional[BaseSpaceVipreProject]:
        """
        Get a project by it's name

        :return: BaseSpaceVipreProject matching the given project_name, if one
                 exists.
        """

        return next(self._get_projects(project_name), None)

    def _get(self, path: str, query: Optional[Dict[str, str]] = None, ) -> Dict[str, Any]:
        url = "{}{}".format(self.endpoint, path)

        if query:
            url += "?" + urllib.parse.urlencode(query)

        response = self.session.get(url, headers=self._headers, timeout=self.timeout)
        return self._handle_response(response)

    def _handle_response(self, response: requests.Response) -> Dict[str, Any]:
        if response.status_code != 200:
            if response.headers.get("Content-Type", "").startswith("application/json"):
                message = response.json().get("ErrorMessage", None)
                if message is not None:
                    raise ValueError("An error occurred: {}".format(message))

            raise ValueError("Unexpected return status: {}".format(response.status_code))

        return response.json()

    def _get_projects(
            self,
            name: str = None,
            limit: int = 200,
            offset: int = 0
    ) -> Iterator[BaseSpaceVipreProject]:
        """
        Obtain a list of all projects matching the provided criteria.

        :param name:  Optional name to filter projects by
        :param limit: Maximum number of projects to return
        :param offset: Offset to start from (pagination)
        :return: Generator yielding BaseSpaceVipreProject for each project
        """

        query = {
            "Limit": str(limit),
            "Offset": str(offset),
        }

        if name is not None:
            query["Name"] = name

        response = self._get("/v1pre3/users/current/projects", query)
        for item in response["Response"]["Items"]:
            yield BaseSpaceVipreProject(item)


class CachingBaseSpaceApi(BaseSpaceApi):
    """
    Adds a caching wrapper around certain endpoints in the BaseSpaceApi
    """

    def __init__(self, access_token: str = None):
        super().__init__(access_token)

    @cache_it('{1}')
    def get_dataset_files(self, dataset_id: str):
        return super().get_dataset_files(dataset_id)

    @cache_it('{1}')
    def get_datasets_by_project_id(self, project_id: int):
        return super().get_datasets_by_project_id(project_id)

    def find_project_dataset_by_name(self, project_id: int, sample_name: str) -> Optional[BaseSpaceDataset]:
        """
        Find a sample by the given name inside of the project.

        This method could be in BaseSpaceApi instead, but repeated calls would
        be incredibly inefficient without any caching.

        :param project_id: Id of the project to search
        :param sample_name: Name of the sample to look for
        :return: BaseSpaceDataset for the given sample if it was found
        """
        for dataset in self.get_datasets_by_project_id(project_id):
            if dataset.name == sample_name:
                return dataset

        # Fallback to checking for matches with a "_L001" suffix
        for dataset in self.get_datasets_by_project_id(project_id):
            if dataset.name == sample_name + "_L001":
                return dataset

        return None

    @cache_it
    def get_all_projects(self):
        return super().get_all_projects()

    @cache_it('{1}')
    def get_project_by_name(self, project_name: str):
        return super().get_project_by_name(project_name)
