#!/usr/bin/env bash

# Author:       Xabier Vázquez-Campos
# Email:        xvazquezc@gmail.com
# Date:         Check the repo for latest commits: https://bitbucket.org/xvazquezc/otureporter
# License:      GNU General Public License
# Usage:        download_db.sh -a | -d DATABASE
# Description:  Automatic download and unpack of OTUreporter ready-to use databases.

set -o errexit
set -o nounset
set -o pipefail

# Location of the database directory
DOWNLOAD_DB_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)/../db"

# Bitbucket's API2.0 URL of the repo's download section
DOWNLOAD_DB_URL="https://api.bitbucket.org/2.0/repositories/xvazquezc/otureporter/downloads"

# Interanl: Retrieve and output the contents at the given URL on stdout.
#
# Will look for curl or wget on $PATH to attempt to retrieve the URL.
function http_get() {
  local url=$1

  if command -v curl &>/dev/null; then
    curl -L -s -G "$url"
  elif command -v wget &>/dev/null; then
    wget -qO- -- "$url"
  else
    echo "Both curl and wget are no available on \$PATH! Bye."
    exit 1
  fi
}

# Internal: Retrieve cleaned JSON listing files available for download.
#
# Access the downloads section of Bitbucket via API2.0 and do pre-cleaning
# of the JSON file.
#
# $1 (Optional) - If present, the data will only be cached, and not written
#                 to stdout.
bbdowns() {
  local cache=${1:-}

  if [[ -z "${_download_db_bbdowns:-}" ]]; then
    _download_db_bbdowns=$(http_get ${DOWNLOAD_DB_URL} | jq '. | .values[]')
  fi

  if [[ -z "$cache" ]]; then
    echo "$_download_db_bbdowns"
  fi
  return 0
}

downNextract() {
  local name=$1
  local href

  dl_info=$(bbdowns | jq -r --arg db "$i" 'select(.name==$db)')
  href=$(echo "$dl_info" | jq -r '.links.self.href')

  if [[ ! -d "$DOWNLOAD_DB_DIR" ]]; then
    debug "Creating database directory: $DOWNLOAD_DB_DIR"
    mkdir "$DOWNLOAD_DB_DIR"
  fi

  ## Download the db files and extract the contents directly into the db folder
  echo -e "Downloading ${1}..."
  http_get "$href" | tar xjv -C "${DOWNLOAD_DB_DIR}" || { echo -e "${href} not found..." >&2; }
  echo ""
}

usage() {
  echo "\
  $(basename "$0") -a | -d DATABASE"
  echo "\
  ;
  OPTIONS
  -d, --database; Download specified databases.
  -a, --all; Download all available pre-formatted databases.
  -l, --list; Print a list of the available databases.
  -h, --help; Show this help information and exits.
  " | column -t -s ";"
}

# Internal: List databases available for download.
function download_db_repo_list() {
  local -a databases

  echo "Listing available databases for download..."
  echo ""

  bbdowns cache # cache the data

  IFS='' mapfile -t databases < <(bbdowns | jq -r '.name')

  (
    echo "Filename;Name;Version;Primer"
    echo "--------;----;-------;------"
    for i in "${databases[@]}"; do
      IFS='.' read -r name version primer _ <<<"$i"
      echo "$i;$name;$version;$primer"
    done
  ) | (column -t -s ";" 2>/dev/null || cat)

  echo ""
  echo "Note: pre-packed database files follow the naming convention DATABASE.vVERSION.PRIMER_SET"
}

# Internal: Attempt to download a database for the given target region and family.
#
# $1 - Target primer to find a database for
# $2 - The target region of the database
# $3 - The name of the database family
function find_db_for_primer() {
  local target_primer=$1
  local db_region=$2
  local db_name=$3

  local -a databases
  local name version primer _

  bbdowns cache # cache the data

  IFS='' mapfile -t databases < <(bbdowns | jq -r '.name')

  for i in "${databases[@]}"; do
    IFS='.' read -r name version primer _ <<<"$i"

    if [[ "${name,,}" == "${db_name,,}" ]] &&
      {
        [[ "${primer,,}" == "${db_region,,}" ]] ||
          [[ "${primer,,}" == "${target_primer,,}" ]]
      }; then
      echo "$i"

      return 0
    fi
  done

  return 1
}

# Internal: Attempt to download a database for the given target region and family.
#
# $1 - Target primer to find a database for
# $2 - The target region of the database
# $3 - The name of the database family
function download_db_for_primer() {
  local target_primer=$1
  local db_region=$2
  local db_name=$3

  local -a databases
  local found_database

  bbdowns cache # cache the data

  found_database=$(find_db_for_primer "$target_primer" "$db_region" "$db_name")

  if [[ -z "$found_database" ]]; then
    error "Failed to find a $db_name database for the primer '$target_primer' in the $db_region region!"
    return 1
  fi

  download_db_download_by_names "$found_database"
  return 0
}

# Internal: Download all databases available in the repository.
function download_db_download_all() {
  local i
  local -a databases

  bbdowns cache # cache the data

  IFS='' mapfile -t databases < <(bbdowns | jq -r '.name')

  for i in "${databases[@]}"; do
    downNextract "$i"
  done
}

# Internal: Download the named databases.
#
# $@ - Names of the databases to download.
function download_db_download_by_names() {
  local names=("$@")

  bbdowns cache # cache the data

  local i
  for i in "${names[@]}"; do
    dl_info=$(bbdowns | jq -r --arg db "$i" 'select(.name==$db)')

    if [[ -z "$dl_info" ]]; then
      echo "No such database: $i"
      exit 1
    fi
  done

  for i in "${names[@]}"; do
    downNextract "$i"
  done
}

function download_db_main() {
  # Launch in a subshell so we don't have to deal with errexit
  local getopt_exit=$(
    getopt --test >/dev/null
    echo $?
  )
  if [[ $getopt_exit -ne 4 ]]; then
    echo "I’m sorry, $(getopt --test) failed in this environment."
    exit 1
  fi

  OPTIONS=d:hla
  LONGOPTIONS=database:,help,list,all

  # -temporarily store output to be able to check for errors
  # -e.g. use “--options” parameter by name to activate quoting/enhanced mode
  # -pass arguments only via   -- "$@"   to separate them correctly
  PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")

  if [[ $? -ne 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
  fi
  # read getopt’s output this way to handle the quoting right:
  eval set -- "$PARSED"

  # handle non-option arguments
  if [[ $# -eq 1 ]]; then
    usage
    exit 4
  fi

  local db_list=()
  local all=

  while true; do
    case "$1" in
    -a | --all)
      all=true
      shift
      ;;
    -d | --database)
      if [[ -f $2 ]]; then
        db_list=($(cat $2))
      elif [[ $2 =~ "," ]]; then
        db_list=($(echo $2 | tr "," " "))
      else
        db_list=("$2")
      fi
      shift 2
      ;;
    -l | --list)
      ## list the manually-uploaded files in the Downloads section (not the repo itself)
      download_db_repo_list
      exit
      ;;
    -h | --help)
      usage
      exit
      ;;
    --)
      shift
      break
      ;;
    *)
      if [ -z "$1" ]; then
        break
      else
        echo "'$1' is not a valid option"
        usage
        exit 3
      fi
      ;;
    esac
  done

  if [[ $# -ne 0 ]]; then
    echo -- "$@" invalid option
    exit 4
  fi

  if [[ -n "${db_list:-}" ]] && [[ -n $all ]]; then
    echo "The provided options don't make a lot of sense..."
    echo "Do you want to download all the databases or just some of them?"
    exit 1
  fi

  if [[ -n $all ]]; then
    download_db_download_all
  elif [[ -n "${db_list:-}" ]]; then
    download_db_download_by_names "${db_list[@]}"
  fi
}

# If we're being executed as a script, pass arguments to download_db_main
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  download_db_main "$@"
fi
