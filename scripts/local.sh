#!/usr/bin/env bash

declare -A LOCAL_SAMPLE_NAMES=()        # A mapping from sample_id to its "id name"
declare -A LOCAL_SAMPLE_PATHS=()        # A mapping from sample_id to its path

declare -a LOCAL_ALL_SAMPLES=()         # List of sample_ids for all samples to be used (excluding controls)
declare -a LOCAL_POS_CONTROL_SAMPLES=() # List of sample_ids for all positive controls
declare -a LOCAL_NEG_CONTROL_SAMPLES=() # List of sample_ids for all positive controls

# Internal: Dump BaseSpace globals with declare -p
function local_save_globals() {
  declare -p LOCAL_SAMPLE_NAMES
  declare -p LOCAL_SAMPLE_PATHS
  declare -p LOCAL_ALL_SAMPLES
  declare -p LOCAL_POS_CONTROL_SAMPLES
  declare -p LOCAL_NEG_CONTROL_SAMPLES
}

# Internal: Attempt to create a unique id for the given sample.
#
# No guarantee is made other than it's *unlikely* for two samples to have
# the same id.
#
# Without specific regard for maintaining mapping between executions
# or machines.
#
# $1 - The local file path to the sample
# $2 - The mothur safe name of the same
function unique_sample_id() {
  hash=$( echo $1 | sha256sum | cut -f 1 -d ' ')
  echo "$2_${hash:0:32}"
}

# Internal: Loads the given sample into the given array.
#
# $1 - The full path to the sample
# $2 - (Optional) The name of the array to store the id of this sample in.
# $3 - (Optional) Name of the variable to store the id of this sample in.
function local_load_sample() {
  local path=$1
  local dest_array_name=${2-}
  local dest_name=${3-}

  local sample_filename=$(basename "$path")
  local sample_name=$(echo "$sample_filename" | awk '{gsub(/(_S[0-9]+)?_L[0-9][0-9][0-9]_R1_001\.fastq(\.gz)?$/, ""); print}')
  local mothur_safe_name=$(echo "${sample_name}" | tr ':\-/' '_')
  local sample_id_unique=$(unique_sample_id "${path/.fastq.gz/.fastq}" "$mothur_safe_name")

  LOCAL_SAMPLE_NAMES[$sample_id_unique]=$sample_name
  LOCAL_SAMPLE_PATHS[$sample_id_unique]=$path

  if [[ -n "${dest_array_name-}" ]]; then
    eval "$dest_array_name+=(\"\$sample_id_unique\")"
  fi
  if [[ -n "${dest_name-}" ]]; then
    eval "$dest_name=\$sample_id_unique"
  fi
}

# Internal: Load samples from the given folder
#
# Will skip samples already loaded as positive or negative controls.
function local_load_folder_samples() {
  local dir=$1
  local sample_id=

  while read -r path; do
    debug "Sample: $path"

    if [[ $path == *.fastq.gz && -f "${path/.fastq.gz/.fastq}" ]]; then
      debug "Skipping compressed sample $path in favour of it's deocmpressed counterpart"
      continue
    fi

    local_load_sample "$path" "" "sample_id"
    local sample_name=${LOCAL_SAMPLE_NAMES[$sample_id]}
    debug "Discovered sample $sample_name"

    if array_contains LOCAL_POS_CONTROL_SAMPLES "$sample_id"; then
      debug "Skipping '$sample_name' which is also a positive control"
    elif array_contains LOCAL_NEG_CONTROL_SAMPLES "$sample_id"; then
      debug "Skipping '$sample_name' which is also a negative control"
    else
      info "Loaded sample $sample_name as $sample_id."
      LOCAL_ALL_SAMPLES+=("$sample_id")
    fi
  done < <(find "$1" -type f | grep _R1_001\\.fastq\\.gz)
}

# Internal: Loads and checks basic information about the referenced samples.
#
# $1 - Name of the array holding a list of folders to use samples from.
# $2 - Name of the array holding a list of positive control samples, of the
#      same format as $2
# $3 - Name of the array holding a list of negative control samples, of the
#      same format as $2
function local_load_samples_from_args() {
  local sample_folders=$1
  local control_folder=$2
  local positive_controls=$3
  local negative_controls=$4

  local sample_array="${sample_folders}[@]"
  local sample_array_length
  eval 'sample_array_length=${#'"${sample_folders}"'[@]}'
  local control_type control search_path

  local control_search_paths=("")
  if [[ -n "$control_folder" ]]; then
    control_search_paths+=("$control_folder")
  fi
  if [[ "$sample_array_length" -gt 0 ]]; then
    control_search_paths+=("${!sample_array}")
  fi

  # Load specifed controls from {POS,NEG}_CONTROLS into {pos,neg}_CONTROL_SAMPLES
  for control_type in "POSITIVE" "NEGATIVE"; do
    if [[ "$control_type" == "POSITIVE" ]]; then
      control_array="${positive_controls}[@]"
      control_samples="LOCAL_POS_CONTROL_SAMPLES"
    else
      control_array="${negative_controls}[@]"
      control_samples="LOCAL_NEG_CONTROL_SAMPLES"
    fi

    if [[ -z "${!control_array-}" ]]; then
      warn "No $control_type controls specified!"
      continue
    fi

    # Load specified controls
    for control in "${!control_array}"; do
      fastq_files=()

      local stderr_file=$(mktemp)

      # Track so we can warn the user if we find the controls multiple times
      local -i num_folders_hit=0 last_hit_count=0
      for search_path in "${control_search_paths[@]}"; do
        while read -r path; do
          fastq_files+=("$path")
        done < <(
          if [[ -n "$search_path" ]]; then
            find "$search_path/$control" -type f 2>"$stderr_file" | grep _R1_001\\.fastq\\.gz
          else
            find "$control" -type f 2>"$stderr_file" | grep _R1_001\\.fastq\\.gz
          fi
        )

        if [[ ${#fastq_files[@]} -ne "$last_hit_count" ]]; then
          num_folders_hit+=1
          last_hit_count=${#fastq_files[@]}
        fi
      done

      local stderr="$(cat "$stderr_file")"
      rm "$stderr_file"

      if [[ "$num_folders_hit" -gt 1 ]]; then
        warn "Found control '$control' multiple times!"
        dump fastq_files
      fi

      len=${#fastq_files[@]}

      case "$len" in
      0)
        error "No controls found under $control"
        echo "$stderr"
        exit 1
        ;;
      1) ;;
      *)
        warn "Found $len controls under $control"
        ;;
      esac

      for control_sample in "${fastq_files[@]}"; do
        local_load_sample "$control_sample" "$control_samples" "control_id"
        info "Loaded $control_type control ${LOCAL_SAMPLE_NAMES[$control_id]} as $control_id."
      done
    done
  done

  if [[ "$sample_array_length" -gt 0 ]]; then
    for dir in "${!sample_array}"; do
      local_load_folder_samples "$dir"
    done
  fi

  if [[ $LOG_LEVEL -ge $LEVEL_DEBUG ]]; then
    dump LOCAL_ALL_SAMPLES LOCAL_POS_CONTROL_SAMPLES LOCAL_NEG_CONTROL_SAMPLES
  fi
}
