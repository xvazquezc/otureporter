import argparse
import csv
import logging
import os
import re
import sys
from typing import Optional, List, Iterable, NamedTuple

from scripts import utils
from scripts.basespace_api import Errors, BaseSpaceDatasetFile, BaseSpaceDataset, BaseSpaceVipreProject, \
    CachingBaseSpaceApi
from scripts.basespace_downloader import BaseSpaceDownloader
from scripts.utils import bash_declare, script_set, script_resp

PAT_FASTQ_FILENAME = re.compile(r'(.*?)(?:_S\d+)?_(L\d+_R(\d)_001(\.fastq(\.gz)?))')
SampleSpec = NamedTuple("SampleSpec", (("project", str), ("sample", str)))


def parse_spec(specs: List[str], default: Optional[str]) -> List[SampleSpec]:
    """
    Transform a list of strings of the format "sample" or "project/sample" to a
    list of tuples separating out the project and sample,   falling back to the
    provided default project if none was specified.

    :param specs: List of string like "sample" or "project/sample"
    :param default: Project to use when omitted in a specification
    :return: List of tuples containing project and sample of each specification
    """
    result: List[SampleSpec] = []

    for spec in specs:
        split_spec: List[str] = spec.split("/", 1)
        if len(split_spec) == 1:
            if default is None:
                raise ValueError(Errors.MISSING_DEFAULT)

            result.append(SampleSpec(default, split_spec[0]))
        else:
            result.append(SampleSpec(split_spec[0], split_spec[1]))

    return result


class ResolvedSampleSpec(object):
    """
    Holds information pertaining to a single sample, providing some simple
    utilities to work with filepaths.
    """

    def __init__(self, project: BaseSpaceVipreProject, dataset: BaseSpaceDataset, files: List[BaseSpaceDatasetFile]):
        self.project: BaseSpaceVipreProject = project
        self.dataset: BaseSpaceDataset = dataset
        self.files: List[BaseSpaceDatasetFile] = files
        self.filename_prefix = '{}_{}'.format(self.dataset.name, self.dataset.id)
        self.original_prefix = PAT_FASTQ_FILENAME.match(files[0].name).group(1)

    def get_local_filename(self, file: BaseSpaceDatasetFile):
        """
        Return the name to use locally for the given dataset file

        :param file: The dataset file
        :return: Local name for the provided dataset file
        """

        match_groups = PAT_FASTQ_FILENAME.match(file.name)
        suffix = match_groups.group(2)
        if suffix.endswith(".gz"):
            suffix = suffix[:-3]

        return '{}_{}'.format(self.filename_prefix, suffix)

    def get_local_filepaths(self, destination: Optional[str] = None) -> List[str]:
        """
        Return local filenames or filepaths (if destination is supplied) to all
        dataset files in this sample

        :param destination: Optional directory to prepend to each filename
        :return: List of filenames/filepaths for each file.
        """
        return [
            os.path.join(destination, self.get_local_filename(file))
            if destination is not None else self.get_local_filename(file)
            for file in self.files
        ]


class BaseSpaceResolver(object):
    logger = logging.getLogger("BaseSpaceResolver")
    logger.level = logging.DEBUG

    def __init__(self, projects: List[str], default_control_project: Optional[str]):
        self.api = CachingBaseSpaceApi()

        self.projects: List[str] = projects
        self.default_control_project: Optional[str] = default_control_project

        self.as_script: bool = False
        self.samples: List[ResolvedSampleSpec] = []
        self.negative_controls: List[ResolvedSampleSpec] = []
        self.positive_controls: List[ResolvedSampleSpec] = []

    @property
    def all_samples(self) -> List[ResolvedSampleSpec]:
        return self.samples + self.positive_controls + self.negative_controls

    @property
    def default_project(self) -> Optional[str]:
        return self.projects[0] if len(self.projects) != 0 else None

    def resolve_sample_specs(self, data: List[SampleSpec]) -> List[ResolvedSampleSpec]:
        """
        Resolve the given sample specs, obtaining information about the files
        in each sample, including:
            * Name
            * Size
            * URL

        :param data: List of SampleSpecs to resolve
        :return: List of ResolvedSampleSpecs
        """

        result: List[ResolvedSampleSpec] = []

        for project_name, sample_name in data:
            project = self.api.get_project_by_name(project_name)
            sample = self.api.find_project_dataset_by_name(project.id, sample_name)
            files = self.api.get_dataset_files(sample.id)

            result.append(ResolvedSampleSpec(project, sample, files))

        return result

    def parse(
            self,
            samples: List[str],
            positive_controls: List[str],
            negative_controls: List[str]
    ):
        """
        Parse and resolve samples and controls from user input.

        If no samples are provided, all non-control samples from the projects
        list will be used instead.

        :param samples: List of samples to parse
        :param positive_controls: List of negative controls to parse
        :param negative_controls: List of positive controls to parse
        """

        # Parse the input into SampleSpecs
        parsed_samples = parse_spec(samples, self.default_project)
        parsed_positive_controls = parse_spec(positive_controls, self.default_control_project or self.default_project)
        parsed_negative_controls = parse_spec(negative_controls, self.default_control_project or self.default_project)

        self.logger.debug(parsed_samples)
        self.logger.debug(parsed_positive_controls)
        self.logger.debug(parsed_negative_controls)

        if len(parsed_samples) == 0 and len(self.projects) > 0:
            # No specific samples, so we'll load all samples from the provided projects
            for project in self.projects:
                for dataset in self.api.get_datasets_by_project_name(project):
                    parsed_samples.append(SampleSpec(dataset.project_name, dataset.name))

        if len(parsed_samples) == 0:
            raise ValueError("No samples to process?")

        # Resolve file information for every sample (including filenames, size and URL)
        self.samples = self.resolve_sample_specs(parsed_samples)
        self.positive_controls = self.resolve_sample_specs(parsed_positive_controls)
        self.negative_controls = self.resolve_sample_specs(parsed_negative_controls)

        # Remove controls from the samples list
        # (e.g. if we loaded all samples in a project that also contains controls)
        controls_set = {
            x.dataset.id for x in self.positive_controls + self.negative_controls
        }
        self.samples = list(filter(lambda x: x.dataset.id not in controls_set, self.samples))

        def dump_infos(type: str, samples: Iterable[ResolvedSampleSpec]):
            self.logger.info("Discovered {}s:".format(type))
            for sample in samples:
                self.logger.info('    {}/{} - Files: {}'.format(
                    sample.project.name, sample.dataset.name,
                    ', '.join(y.name for y in sample.files))
                )

        # Log the resolved samples we found
        dump_infos("sample", self.samples)
        dump_infos("positive control", self.positive_controls)
        dump_infos("negative control", self.negative_controls)

    def download(self, destination: str):
        """
        Ensure the files for every resolved sample are present in destination,
        downloading them if they're missing.

        :param destination: Location to download samples to
        """

        downloader = BaseSpaceDownloader(self.api, destination)
        for sample in self.all_samples:
            for file in sample.files:
                downloader.add_task(file, sample.get_local_filename(file))

        downloader.close_join()

        self.logger.info("All samples downloaded")

    def save_limsid_map(self, limsid_map_path: str, sample_destination: str):
        """
        Construct LIMSID_to_sampleID containing information about the resolved
        samples. Including:
            * Name
            * Source
            * Sequence count
            * Display name (to be used in the report)

        :param limsid_map_path: Full path to save the TSV file at
        :param sample_destination: Location where samples were downloaded to
        """

        with open(limsid_map_path, 'w') as f:
            writer = csv.DictWriter(f, delimiter='\t', fieldnames=[
                "sample_prefix",
                "sample_source",
                "seq_count",
                "display_name",
            ])

            for sample in self.all_samples:
                first_file = sample.get_local_filepaths(sample_destination)[0]
                seq_count = utils.count_lines(first_file) // 4

                writer.writerow({
                    "sample_prefix": sample.filename_prefix,
                    "sample_source": sample.original_prefix,
                    "seq_count": seq_count,
                    "display_name": sample.dataset.name,
                })

    def dump_vars(self):
        """
        Output information about the resolved samples in a structured manner.

        When running with --script, the output is intended to be parsed line by
        line.

        When not running with --script, the output is of a similar format to
        `declare -p variable`.
        """

        if self.as_script:
            output_fn = script_set
        else:
            output_fn = bash_declare

        output_fn('CONTROL_COUNT', str(
            len(self.negative_controls) + len(self.positive_controls)
        ))
        output_fn('ALL_SAMPLES', str(len(self.all_samples)))
        output_fn('LOADED_POS_CONTROLS', [
            x.filename_prefix for x in self.positive_controls
        ])
        output_fn('LOADED_NEG_CONTROLS', [
            x.filename_prefix for x in self.negative_controls
        ])


def main():
    logger = logging.getLogger("BaseSpace")

    parser = argparse.ArgumentParser(description='Download and prepare samples from BaseSpace.')
    parser.add_argument('--ctl_project', dest='default_ctl_project', action='store',
                        help='Default control project to use when omitted from individual controls')
    parser.add_argument('--project', dest='projects', action='append', default=[],
                        help='Default project to use when omitted from individual samples')
    parser.add_argument('-s', '--sample', dest='samples', action='append', default=[],
                        help='Samples to obtain')
    parser.add_argument('-p', '--pos', dest='pos_controls', action='append', default=[],
                        help='Positive controls to obtain')
    parser.add_argument('-n', '--neg', dest='neg_controls', action='append', default=[],
                        help='Negative controls to obtain')
    parser.add_argument('--dest', dest='output_folder', action='store', required=True,
                        help='Folder to download the samples to')
    parser.add_argument('--info', dest='output_limsid', action='store', required=True,
                        help='File to output LIMSID info table')
    parser.add_argument('--script', dest='as_script', action='store_true',
                        help='Adjust output format for use with external scripts')

    args = parser.parse_args()

    if args.as_script:
        script_resp('init')
        utils.init_logging()

    if not os.path.exists(args.output_folder):
        logger.error("Output folder {!r} doesn't exist!".format(args.output_folder))
        return sys.exit(1)

    if not os.path.exists(os.path.dirname(args.output_limsid)):
        logger.error("Parent folder for {!r} doesn't exist!".format(args.output_limsid))
        return sys.exit(1)

    bsr = BaseSpaceResolver(args.projects, args.default_ctl_project)
    bsr.as_script = args.as_script
    bsr.parse(
        args.samples,
        args.pos_controls,
        args.neg_controls,
    )
    bsr.download(args.output_folder)
    bsr.save_limsid_map(args.output_limsid, args.output_folder)
    bsr.dump_vars()

    logger.info("Bye!")
    sys.exit(0)


if __name__ == "__main__":
    main()
