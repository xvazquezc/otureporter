#!/usr/bin/env bash

SCRIPTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
# shellcheck source=./xcol.sh
source "$SCRIPTS_DIR/xcol.sh"

# Public: Handle execution of and output from Mothur.
#
# Executes Mothur with the given parameters ("$@"). Additionally,
#  * Strips escape characters, as Mothur explicitly calls 'clear' on startup
#  * Skips the first 30 lines of output as just clutters stdout
#  * Colorizes certain fragments of Mothur output
#
# N.B. Currently buffers partial lines.
#
# $@ - The parameters to execute Mothur with.
#
# Examples
#
#   mothur_col 01_get_good_seqs.batch
function mothur_color() {
  # TODO Consider checking Mothur output for 'ERROR' while it's running

  # The perl one liner exits (i.e. SIGPIPE) when it sees any '[ERROR]' in
  # the output, explicitly ignoring the following errors types:
  #   * [ERROR]: unable to spawn the number of processes you requested, reducing number to 1
  #   * Detected 1 [ERROR] messages, please review.
  #      - Purely to follow on from ignoring the first error
  #      - Specifically, it will only ignore 1-9 errors, Mothur will abort
  #        after 10 errors anyway.

  # Temporarily disable pipefail so we can inspect Mothur's exit code, as it's
  # acceptable for Mothur to exit with 1, indicating there were one or more
  # errors. However, "unable to spawn the number of processes" is an 'error'
  # that is acceptable for us.
  local current_pipefail=$(shopt -po pipefail)
  set +o pipefail

  unbuffered mothur "$@" |
    unbuffered tr -d '\33' |
    LANG=C unbuffered perl -ne '$|=1; print; /(?<!Detected \d )\[ERROR\](?!: unable to spawn the number of processes| messages, please review)/i && exit 1' |
    unbuffered tail -n +29 |
    xcol_mothur
  local exit_codes=("${PIPESTATUS[@]}")

  # Restore original value of pipefail option
  eval "$current_pipefail"

  if [[ "${exit_codes[0]}" -ne 0 ]] && [[ "${exit_codes[0]}" -ne 1 ]]; then
    error "Mothur exited with ${exit_codes[0]}!"
    return "${exit_codes[0]}"
  fi

  # Check the remaining exit codes for any non-zero exists.
  local idx
  for ((idx = ${#exit_codes[@]} - 1; idx >= 1; idx--)); do
    if [[ "${exit_codes[$idx]}" -ne 0 ]]; then
      return "${exit_codes[$idx]}"
    fi
  done
}

# Internal: Colourises fragments of Mothur output.
#
# Colourises fragments of Mothur output using xcol, expects input from Mothur
# via stdin.
#
# Checks COLOR_OUTPUT is either blank or "y" before outputting color.
#
# Examples
#
#   mothur "my_commands.batch" | xcol_mothur
function xcol_mothur() {
  if [[ "${COLOR_OUTPUT:-y}" != y ]]; then
    cat
    return
  fi

  # TODO colorize the output with sed ourselves for more control

  local pat_name="[[:alnum:]_/\.-]+"
  local file_exts="\w{3,12}\b"
  local sp="[[:space:]]" dig="[[:digit:]]"

  xcol \
    "^mothur${sp}>"                         `# full prompt` \
    "${sp}>"                                `# just the \>` \
    "${sp}[a-z]{2,}\.[a-z]{2,}"             `# command names` \
    "(\(|${sp})\w+=[^,[:space:]\)]+"        `# paramaters ` \
    "^\w+=[^,]+"                            `# current files/params saved by mothur ` \
    "--"                                    `# junk to skip color` \
    "Output${sp}File${sp}Names:"            `# list of output file names` \
    "$pat_name/$pat_name\.($file_exts)\b"   `# file names` \
    "--"                                    `# junk to skip color` \
    "--"                                    `# junk to skip color` \
    ">>>>>.*?<<<<<"                         `# file being processed` \
    "--"                                    `# junk to skip color` \
    "--"                                    `# junk to skip color` \
    "--"                                    `# junk to skip color` \
    "--"                                    `# junk to skip color` \
    "--"                                    `# junk to skip color` \
    "It${sp}took.$dig.*?to.*?$dig.*$"       `# Took x time to do bla` \
    "${sp}\(files${sp}$dig+${sp}of${sp}$dig+\)${sp}*<<<<<"  `# trailer of files being processed`
}

declare _STDBUF
# Internal: If possible, executes the given command without buffering.
#
# The given command "$@" will be executed with stdbuf to disable buffering,
# if stdbuf is not available, "$@" will be executed as is.
#
# $@ - The command (and it's parameters) to execute without buffering
#
# Examples
#
#   unbuffered mothur "my_commands.batch" | unbuffered tr -d 'M'
function unbuffered() {
  if [ -z ${_STDBUF+x} ]; then
    _STDBUF=$(ignore_error command -v stdbuf 2>/dev/null)
  fi

  if [[ -n "${_STDBUF}" ]]; then
    "${_STDBUF}" -e 0 -o 0 "$@"
  else
    "$@"
  fi
}

# Internal: Safely execute the given command.
#
# Temporarily disables errexit (if necessary) before executing "$@".
#
# $@ - The command (and it's parameters) to execute
#
# Examples
#
#   BAR=$(ignore_error ls /path/to/nonexistant/file)
function ignore_error() {
  local current_flags=$-
  set +e # temporarily disable errexit
  "$@"
  set -$current_flags # restore the original flags
}

# If we're being executed as a script, pass arguments to mothur_call
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  set -o errexit
  set -o nounset
  set -o pipefail

  mothur_color "$@"
fi
