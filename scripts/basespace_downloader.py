import gzip
import logging
import os
import queue
import shutil
import time
from threading import Thread
from typing import Tuple, Optional, Dict

from scripts.basespace_api import BaseSpaceApi, BaseSpaceDatasetFile


class BaseSpaceDownloader(object):
    """
    Threaded download queue for BaseSpace datasets
    """

    logger = logging.getLogger("BaseSpaceDownloader")
    logger.level = logging.DEBUG
    max_attempts = 3

    def __init__(self, api: BaseSpaceApi, output_dir: str, n_threads: int = 16, decompress: bool = True):
        self.api: BaseSpaceApi = api
        self.output_dir: str = output_dir
        self.decompress = decompress
        self._queue: queue.Queue[Optional[Tuple[BaseSpaceDatasetFile, str]]] = queue.Queue()
        self.running = True
        self.stopping = False
        self._threads = []
        self.errors: Dict[str, Exception] = {}
        self.timeout = 25

        # Spawn n_threads worker threads, all in-process as jobs should be
        # IO-bound
        for i in range(n_threads):
            t = Thread(target=self._worker, name='DownloaderThread-{}'.format(i))
            t.daemon = True
            t.start()
            self._threads.append(t)

    def _worker(self):
        """
        Worker loop which processes jobs from the queue.

        `None` jobs are used to signal end of queue following a shutdown.
        """

        while self.running:
            try:
                job = self._queue.get(True, 1)
            except queue.Empty:
                if self.stopping:
                    break

                continue

            if job is None:
                if self.stopping:
                    break
                else:
                    raise ValueError("Received a None job, but we aren't stopping?")

            # TODO (David): Handle any failure
            self._download(job[0], job[1])

    def add_task(self, file: BaseSpaceDatasetFile, filename: str):
        """
        Queue the given file to be downloaded to be saved with the given
        filename in the output directory.

        :param file: Dataset file to download
        :param filename: Filename to save the file under
        """

        if not self.running:
            raise ValueError("Downloader isn't running?")
        if self.stopping:
            raise ValueError("Downloader is stopping, can't add new tasks!")

        self._queue.put((file, filename))

    def _download(self, file: BaseSpaceDatasetFile, filename: str):
        """
        Ensure the given dataset file is present in the output directory,
        downloading it if the file is missing.

        If the file is compressed (gz) and decompress is True, the file will be
        inflated before being written to disk.

        Files have a '.part' suffix while they are being downloaded.

        :param file: The dataset file to ensure is downloaded
        :param filename: Filename the file should be saved with
        """

        is_compressed = file.name.endswith(".gz") and self.decompress

        dest_file = os.path.join(self.output_dir, filename)
        part_dest_file = "{}.part".format(dest_file)

        if os.path.exists(dest_file):
            self.logger.info("Skipping: {}".format(dest_file))
            return

        self.logger.info("Downloading: {}".format(dest_file))
        time_started = time.time()
        success = False

        for attempt in range(1, self.max_attempts):
            try:
                with self.api.session.get(
                        file.href_content,
                        headers=self.api._headers,
                        timeout=self.timeout,
                        stream=True
                ) as r:
                    with open(part_dest_file, 'wb') as f:
                        gf = gzip.GzipFile(mode='rb', fileobj=r.raw) if is_compressed else r.raw
                        shutil.copyfileobj(gf, f)

                success = True

                # Remove any errors logged previously for this file
                if file.href_content in self.errors:
                    del self.errors[file.href_content]

                break
            except Exception as e:
                self.errors[file.href_content] = e
                self.logger.warning("Error occurred while downloaded {!r}: {}".format(file.href_content, str(e)))

                import traceback
                traceback.print_exc()

        if not success:
            self.logger.error("All attempts to download {!r} failed!".format(file.href_content))
            raise self.errors[file.href_content]

        os.rename(part_dest_file, dest_file)

        time_taken = time.time() - time_started
        size_mib = file.size / 1048576
        speed_mibs = size_mib / time_taken
        self.logger.info("Finished: {} ({:.2f} MiB in {:.2f}s @ {:.2f} MiB/s)".format(
            dest_file, size_mib, time_taken, speed_mibs)
        )

    def close_join(self):
        """
        Start shutting down the downloader and wait for all tasks to finish.
        """

        # Add sentinel tasks indicating to workers to quit
        for _ in self._threads:
            self._queue.put(None)

        self.stopping = True

        for t in self._threads:
            t.join()

        self.running = False

        if len(self.errors) > 0:
            self.logger.error("{} files failed to download!".format(len(self.errors)))
            raise ValueError("{} files failed to download!".format(len(self.errors)))
