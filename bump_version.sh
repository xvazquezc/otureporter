#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

function action_exec() {
  # echo "Would execute: $*"
  "$@"
}

SCRIPT_DIR="$(cd "$(dirname "$(readlink -e "${BASH_SOURCE[0]}")")" >/dev/null 2>&1 && pwd)"
cd "$SCRIPT_DIR"

# Show the current version information, if present
if [[ -e "$SCRIPT_DIR/VERSION" ]]; then
  echo "Current VERSION file:"
  cat "$SCRIPT_DIR/VERSION"
fi

# Prompt for the new version number
NEW_VERSION_NUMBER=""
while [[ -z "$NEW_VERSION_NUMBER" ]]; do
  read -rp "New version number: " NEW_VERSION_NUMBER
done

# Prompt for the type of release
while true; do
  read -rp "Relase type - [N]ormal (default), [B]eta, [A]lpha: " RELEASE_TYPE
  case "${RELEASE_TYPE,,}" in
  n | normal | "")
    RELEASE_TYPE=""
    ;;
  b | beta)
    RELEASE_TYPE="-beta"
    ;;
  a | alpha)
    RELEASE_TYPE="-alpha"
    ;;
  *)
    continue
    ;;
  esac
  break
done

TAG_VERSION="v${NEW_VERSION_NUMBER}${RELEASE_TYPE}"

# Check the tag doens't already exist
if git rev-parse "refs/tags/$TAG_VERSION" 2>/dev/null; then
  printf "\n[!] Tag %q already exists!\n" "$TAG_VERSION"

  # Offer to overwrite it
  while true; do
    read -rp "Overwrite existing tag? [Y]es/[N]o: " CONTINUE_CHOICE
    case "${CONTINUE_CHOICE,,}" in
    y | yes)
      action_exec git tag --delete "$TAG_VERSION"
      break
      ;;
    n | no)
      exit 1
      ;;
    *) ;;
    esac
  done
fi

echo -e "\nRecent commits:"
git --no-pager log --pretty=format:'  %h: %s' -n 3 HEAD
echo -e "\n"

while true; do
  read -rp "[C]reate a new commit (default), or [A]mend the previous commit: " BUMP_TYPE
  case "${BUMP_TYPE,,}" in
  c | create | "")
    # Reference the now current commit in VERSION
    COMMIT_HASH=$(git rev-parse --short HEAD)
    echo -e "$TAG_VERSION\n$COMMIT_HASH\n" >"$SCRIPT_DIR/VERSION"
    action_exec git add "$SCRIPT_DIR/VERSION"

    action_exec git commit -e -m "Bump version to $TAG_VERSION"
    ;;
  a | amend)
    # Reference the commit before last in VERSION - as HEAD will be clobbered by the amend
    COMMIT_HASH=$(git rev-parse --short HEAD~1)
    echo -e "$TAG_VERSION\n$COMMIT_HASH\n" >"$SCRIPT_DIR/VERSION"
    action_exec git add "$SCRIPT_DIR/VERSION"

    action_exec git commit --amend
    ;;
  *)
    continue
    ;;
  esac
  break
done

# Tag the new version
action_exec git tag -a "$TAG_VERSION" -m "version $TAG_VERSION"

# Offer to push the changes
echo
while true; do
  read -rp "Push changes? [Y]/n: " PUSH_TAGS
  case "${PUSH_TAGS,,}" in
  y | yes | "")
    action_exec git push --follow-tags
    ;;
  n | no) ;;
  *)
    continue
    ;;
  esac
  break
done

# Create an archive containing the repository, including submodules
echo
BUMPED_COMMIT_HASH=$(git rev-parse --short HEAD)
VERSION_STRING=$(git describe --tags --always HEAD)
ARCHIVE_TMP=$(mktemp -d)

DEST_DIR=$SCRIPT_DIR/dist
ZIP_ARCHIVE=$DEST_DIR/$VERSION_STRING.zip
if [[ ! -d "$DEST_DIR" ]]; then
  mkdir "$DEST_DIR"
fi

# Delete existing archive if present
rm -f "$ZIP_ARCHIVE" 2>/dev/null

(
  git clone . "$ARCHIVE_TMP"
  cd "$ARCHIVE_TMP"

  git checkout -q "$BUMPED_COMMIT_HASH"
  echo "Updating submodules..."
  git submodule update --init -q

  echo -e "$TAG_VERSION\n$COMMIT_HASH\n$VERSION_STRING\n" >"$ARCHIVE_TMP/VERSION"

  rm -rf "$ARCHIVE_TMP/.git"

  GLOBIGNORE=".:.." zip -Xrq "$ZIP_ARCHIVE" -- *
)
rm -rf "$ARCHIVE_TMP"

echo -e "\nArchive created: $ZIP_ARCHIVE"
