![](template_files/images/logo.png)

[![License](https://img.shields.io/badge/license-GPL3-green)](https://bitbucket.org/xvazquezc/otureporter/src/master/LICENSE) [![Issue counter](https://img.shields.io/bitbucket/issues/xvazquezc/otureporter.svg)](https://bitbucket.org/xvazquezc/otureporter/issues?status=new&status=open) [![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://bitbucket.org/xvazquezc/otureporter/commits/)
---
[![Maintenance](https://img.shields.io/badge/Made%20with-BASH%20v4-4eaa25.svg)](https://www.gnu.org/software/bash/) [![Maintenance](https://img.shields.io/badge/Made%20with-RMarkdown-red.svg)](https://rmarkdown.rstudio.com/) [![Maintenance](https://img.shields.io/badge/Made%20with-Python%20v3-FFD43B.svg)](https://www.python.org/)

* * *
[Visit the Wiki](https://bitbucket.org/xvazquezc/otureporter/wiki/) for detailed up to date documentation.
* * *

## Motivation

In the last years, the number of samples submitted for amplicon sequencing have increased notably. OTUreporter was initially developed as a veeery long PBS/Torque script for internal use by the [Ramaciotti Centre for Genomics](https://www.ramaciotti.unsw.edu.au/) (RAMAC) aiming to:

* provide an easy to use tool for the analysts at RAMAC so they could run analyses on request with the minimal interaction and spending minimal time and reduce the workload on the collaborating bioinformatics team;
* avoid using commercial "black boxes" such the "16S Metagenomics" from BaseSpace/Illumina (not metagenomics, by the way...) and use software that it is actually used in research context for this matters (mothur);
* provide a way to generate *exact* reports automatically, with the minimal/no input from the analyst side;
* avoid conflicts with customer sample IDs; and,
* access to the sequencing data in BaseSpace directly.

As the pipeline on its original form was becoming *not so user-friendly* and complicated to manage it was reformatted as an executable BASH program and restructured to provide a more modular setup where all the primer sets data are external to the main program, allowing an easier way to add or modified primer sets and other customisations.

## Quick Setup

Clone repo:
```bash
git clone https://xvazquezc@bitbucket.org/xvazquezc/otureporter.git
cd otureporter
```

Create Conda environment:

```bash
conda-env create -f conda.yaml
conda activate otureporter
```

Dependency check (just to be sure):
```bash
./otureporter.sh -v --checkdeps
```
 > A warning will appear if some of the required LaTeX packages required to generate the report in pdf format are not available.

Self-test:
```bash
git submodule update --init
./otureporter.sh -v --selftest
```

Good to go!!

For details on how to run OTUreporter, please check the help (`--help`) or the [Basic usage](https://bitbucket.org/xvazquezc/otureporter/wiki/Basic_usage) section of the [wiki](https://bitbucket.org/xvazquezc/otureporter/wiki)

## Citation

Until a peer-reviewed paper is published, you can either cite this repository or the following conference abstract/paper (copy available through the DOI link):

> Vázquez-Campos, X., Chilton, A., Koval, J., Tree, J. J., & Wilkins, M. R. *OTUreporter: a modular automated pipeline for the analysis and report of amplicon data*. Sydney Bioinformatics Research Symposium 2018. 15 Jun 2018. Sydney, NSW, Australia. DOI: [10.6084/m9.figshare.6521171](https://doi.org/10.6084/m9.figshare.6521171).


## Acknowledgements

Thanks to [Åsa Pérez-Bercoff](https://scholar.google.com.au/citations?user=wOdHa3YAAAAJ&hl=en) for the `repseq_renamer.py` script.

Thanks to [Jai J. Tree](https://www.researchgate.net/profile/Jai_Tree) for the "donation" of the testing dataset.

Thanks to [Ignatius Pang](https://github.com/IgnatiusPang) for his help with some of the R code.

Thanks to [Gene Hart-Smith](https://scholar.google.com.au/citations?user=UkK04sV9LdcC&hl=en) for the logo design.
