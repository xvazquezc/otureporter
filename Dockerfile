FROM ubuntu:18.04
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

# Add the R apt repository
RUN apt-get update -q && \
    DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends apt-transport-https software-properties-common dirmngr gnupg && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9 && \
    add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/' && \
    DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends r-base && \
    apt-get remove -qy software-properties-common apt-transport-https && \
    apt-get autoremove -qy && apt-get clean && rm -rf /install /var/lib/apt/lists/*

# Install R and build R packages
ADD /template_files/setup.R /tmp
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends build-essential libcurl4-openssl-dev libssl-dev libz3-dev libxml2-dev gfortran libblas-dev liblapack-dev && \
    MAKEFLAGS='-j 4' Rscript --vanilla /tmp/setup.R && \
    # Large unnecessary folder \
    rm -rf /usr/local/lib/R/site-library/BH/include \
        /tmp/setup.R && \
    # Cleanup uneeded packages \
    apt-get remove -qy perl apt-transport-https build-essential libcurl4-openssl-dev libssl-dev libz3-dev libxml2-dev gfortran libblas-dev liblapack-dev gcc-7 cpp-7 libc-dev libc6-dev libgcc-7-dev libstdc++-7-dev linux-libc-dev && \
    apt-get autoremove -qy && apt-get clean && rm -rf /install /var/lib/apt/lists/*

# Misc. OTUreporter dependencies
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends python3-lxml vsearch biom-format-tools python3-setuptools pandoc wget perl parallel bsdmainutils gawk jq python3 pandoc-citeproc gnupg git && \
    apt-get autoremove -qy && apt-get clean && rm -rf /install /var/lib/apt/lists/*

# Install texlive
ENV PATH="/usr/local/texlive/2019/bin/x86_64-linux:${PATH}"
RUN mkdir /install_tl && cd /install_tl && \
    wget ftp://tug.org/historic/systems/texlive/2019/install-tl-unx.tar.gz && \
    tar --strip-components=1 -xvf install-tl-unx.tar.gz && \
    printf '\
selected_scheme scheme-infraonly\n\
TEXDIR /usr/local/texlive/2019\n\
TEXMFCONFIG ~/.texlive2019/texmf-config\n\
TEXMFHOME ~/texmf\n\
TEXMFLOCAL /usr/local/texlive/texmf-local\n\
TEXMFSYSCONFIG /usr/local/texlive/2019/texmf-config\n\
TEXMFSYSVAR /usr/local/texlive/2019/texmf-var\n\
TEXMFVAR ~/.texlive2019/texmf-var\n\
binary_x86_64-linux 1\n\
instopt_adjustpath 0\n\
instopt_adjustrepo 1\n\
instopt_letter 0\n\
instopt_portable 0\n\
instopt_write18_restricted 1\n\
tlpdbopt_autobackup 1\n\
tlpdbopt_backupdir tlpkg/backups\n\
tlpdbopt_create_formats 1\n\
tlpdbopt_desktop_integration 0\n\
tlpdbopt_file_assocs 0\n\
tlpdbopt_generate_updmap 0\n\
tlpdbopt_install_docfiles 0\n\
tlpdbopt_install_srcfiles 0\n\
tlpdbopt_post_code 1\n\
tlpdbopt_sys_bin /usr/local/bin\n\
tlpdbopt_sys_info /usr/local/share/info\n\
tlpdbopt_sys_man /usr/local/share/man\n\
tlpdbopt_w32_multi_user 0\n\
' > texlive.profile && \
    ./install-tl --profile=texlive.profile && \
    tlmgr install xetex lm amsmath mathspec etoolbox fontspec xkeyval geometry oberdiek hyperref url zapfding fancyvrb framed titling booktabs tools multirow wrapfig float colortbl tabu varwidth threeparttable threeparttablex environ trimspaces ulem makecell xcolor fancyhdr lipsum graphics latex-graphics-dev && \
    rm -rf /install_tl

# Install KronaTools
RUN cd /opt && \
    wget 'https://github.com/marbl/Krona/releases/download/v2.7.1/KronaTools-2.7.1.tar' && \
    echo "e8c25b47ef7f9e8bd51bfda0d8e7c7099feb9f61dc7f3f055e6830573b2d8e829939d09927235940469129827cda81d9849de5339de7ac843249c71c62265ca2 KronaTools-2.7.1.tar" | sha512sum -c - && \
    tar xf KronaTools-2.7.1.tar && \
    rm KronaTools-2.7.1.tar && \
    cd KronaTools-2.7.1 && \
    perl install.pl

# Install bs utility
RUN mkdir /install && cd /install && \
   wget 'https://bintray.com/basespace/BaseSpaceCLI-EarlyAccess-BIN/download_file?file_path=latest%2F0.10.8%2Famd64-linux%2Fbs' -O /usr/local/bin/bs && \
   chmod +x /usr/local/bin/bs && \
   echo "18da4092a3c7452b22fa59769529d8f8f13d9166b3d7c842d69e618c841c97af669d51c54f1071be6f3e59d8c699df2fb690cd571743b40f39c70360ec058b66 /usr/local/bin/bs" | sha512sum -c - && \
   wget https://basespace.bintray.com/BaseSpaceCLI-DEB/bscp_0.6.1-337_amd64.deb && \
   echo "03f364a73b70ac7ac92e2b1440f9a6044bbe8584ea3123c52f2be0f6592a7005f902becf808f4a7310b9f1064e39cd7743f211573c32ce34fce04fff2773ac88 bscp_0.6.1-337_amd64.deb" | sha512sum -c - && \
   apt-get update && \
   apt-get install -qy --no-install-recommends bash-completion python-requests python-setuptools python-yaml libssl1.1 && \
   dpkg -i bscp_0.6.1-337_amd64.deb && \
   apt-get autoremove -qy && apt-get clean && rm -rf /install /var/lib/apt/lists/*

# Download Mothur
RUN mkdir /install && cd /install && \
    wget https://github.com/mothur/mothur/releases/download/v.1.42.3/Mothur.linux_64_noReadline.zip && \
    unzip Mothur.linux_64_noReadline.zip && \
    (cd mothur && mv mothur blast vsearch uchime /usr/bin) && \
    rm -rf /install

# Download FastTree
RUN wget http://www.microbesonline.org/fasttree/FastTree -O /usr/bin/FastTree && \
    bash -c 'sha256sum -c <<< "6fe112c32a2e0b5a2cc6baeeb5f564300f5f650b0dfe96b54a06f0c2ef7a97a5  /usr/bin/FastTree"' && \
    chmod +x /usr/bin/FastTree

# Cleanup some uneeded folders
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# RUN DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends ncdu

ADD . /otureporter

SHELL [ "/bin/bash", "-ic" ]
WORKDIR /otureporter
ENTRYPOINT [ "/otureporter/otureporter.sh" ]
