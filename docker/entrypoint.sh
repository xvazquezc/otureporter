#!/usr/bin/env bash
. /opt/conda/etc/profile.d/conda.sh
conda activate otureporter

cd /code
./otureporter.sh "$@"
