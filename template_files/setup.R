############
### N.B. Also add library installation to template_files/setup.R
############

if (utils::compareVersion(as.character(getRversion()), "3.5") == -1) {
    source("https://bioconductor.org/biocLite.R")
}

# Loading and/or installing packages
#
# Code from http://www.vikparuchuri.com/blog/loading-andor-installing-packages/
# Check whether a package of interest is installed
is_installed <- function(mypkg) is.element(mypkg, installed.packages()[, 1])
# Install it if it isn't already installed
# Run a for-loop of all the package names listed below in the function call
# with the list of packages: load_or_install(c("pkg1", "pkg2",..., "pkgn"))
load_or_install <- function(package_names) {
    for (package_name in package_names) {
        if (!is_installed(package_name)) {
            # Install tidyverse and kableExtra R packages from CRAN
            install.packages(package_name, repos = "http://lib.stat.cmu.edu/R/CRAN")
        }
        library(package_name, character.only = TRUE, quietly = TRUE, verbose = FALSE)
    }
}
# library(tools) has the file_path_sans_ext(filename) function

load_or_install(c("pacman", "BiocManager"))

p_load_gh(c("tidyverse/ggplot2"))

p_load(
    "knitr",
    "kableExtra",
    "dplyr", "tidyr",
    "rmarkdown",
    "phyloseq",
    "plotly"
)
